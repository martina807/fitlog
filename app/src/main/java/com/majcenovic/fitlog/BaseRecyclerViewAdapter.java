package com.majcenovic.fitlog;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.List;

/**
 * Created by martina on 07/08/2017.
 */

public class BaseRecyclerViewAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private int lastPosition = -1;
    protected Context context;
    protected List itemList;

    public BaseRecyclerViewAdapter(Context context, List itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(T holder, int position) {

    }

    protected void setAnimation(View viewToAnimate, int position) {
        if(viewToAnimate == null) return;
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.bottom_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public interface OnItemClickListener<V> {
        void onItemClick(V item);
    }
}