package com.majcenovic.fitlog.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.util.Log;

import com.google.gson.Gson;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.MessageEvent;
import com.majcenovic.fitlog.data.models.PushNotificationModel;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.main.MainActivity;
import com.majcenovic.fitlog.ui.main.messages.chat.ChatActivity;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.majcenovic.fitlog.util.ImageUtil;
import com.majcenovic.fitlog.util.NotificationTypeUtil;
import com.majcenovic.fitlog.util.PushUtil;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

/**
 * Created by martina on 08/08/2017.
 */

public class OneSignalPushService extends NotificationExtenderService {

    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {

        Log.d("Push", "onNotificationProcessing");

        User user = SharedPrefs.readUserData(getApplicationContext());
        boolean isLoggedIn = user != null;

        if(!isLoggedIn) return true;

        boolean shouldBackstack = !receivedResult.isAppInFocus;

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = null;
        TaskStackBuilder stackBuilder = null;
        Intent pending = null;
        Intent intent = null;

        final JSONObject data = receivedResult.payload.additionalData;
        Gson gson = new Gson();
        final PushNotificationModel notif = gson.fromJson(data.toString(), PushNotificationModel.class);

        String content = receivedResult.payload.body;
        String title = receivedResult.payload.title;
        String largeIconUrl = receivedResult.payload.largeIcon;
        String openLocation = null;
        Long openLocationId = null;

        if (notif != null) {
            openLocation = notif.getOpenLocation();
            openLocationId = notif.getOpenLocationId();
        }

        if (content == null) return true;

        int notificationId = 0;

        if (openLocation != null && openLocationId != null) {
            notificationId = openLocation.hashCode() + openLocationId.hashCode();
        } else if (openLocation != null) {
            notificationId = openLocation.hashCode();
        } else {
            notificationId = content.hashCode();
        }


        if (openLocation != null) {

            stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(MainActivity.class);

            switch (openLocation) {
                case NotificationTypeUtil.CHAT:
                    pending = new Intent(this, ChatActivity.class)
                            .putExtra(ExtraUtil.OBJECT, new User(openLocationId, title, largeIconUrl));
                    stackBuilder.addNextIntentWithParentStack(pending);
                    contentIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    EventBus.getDefault().postSticky(new MessageEvent(openLocationId));
                    break;
                default:
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            }
        } else {
            intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        Bitmap largePicture = null;

        if (largeIconUrl != null) {
            Bitmap bmp = ImageUtil.getBitmapFromURL(largeIconUrl);
            if (bmp != null) largePicture = ImageUtil.getCircleBitmap(bmp);
            else
                largePicture = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        } else {
            largePicture = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        }


        if (PushUtil.isInChat(getApplicationContext(), openLocationId + "") && openLocation != null && openLocation.equals(NotificationTypeUtil.CHAT)) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(300);
            return true;
        } else {
            if (title == null) title = getString(R.string.app_name);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setLargeIcon(largePicture)
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .setBigContentTitle(title)
                            .bigText(Html.fromHtml(content)))
                    .setAutoCancel(true)
                    .setContentText(Html.fromHtml(content))
                    .setContentIntent(contentIntent);
        }

        Log.d("MYPUSH notif ID: ", openLocation + ":" + openLocationId + " - " + notificationId);

        mNotificationManager.notify(notificationId, mBuilder.build());

        // Return true to stop the notification from displaying.
        return true;
    }
}
