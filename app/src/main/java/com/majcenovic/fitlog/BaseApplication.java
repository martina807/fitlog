package com.majcenovic.fitlog;

import android.app.Application;
import android.os.StrictMode;

import com.onesignal.OneSignal;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by martina on 26/06/2017.
 */

public class BaseApplication extends Application {

    public static BaseApplication appContext;

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = this;

        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build());

        OneSignal
                .startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();

        JodaTimeAndroid.init(this);

    }
}
