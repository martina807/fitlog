package com.majcenovic.fitlog.di;

import android.content.Context;

import com.majcenovic.fitlog.data.activities.ActivityInteractorImpl;
import com.majcenovic.fitlog.data.activities.RunningActivityInteractorImpl;
import com.majcenovic.fitlog.data.comments.CommentsInteractorImpl;
import com.majcenovic.fitlog.data.galleries.GalleryInteractorImpl;
import com.majcenovic.fitlog.data.goals.GoalsInteractorImpl;
import com.majcenovic.fitlog.data.home.HomeInteractorImpl;
import com.majcenovic.fitlog.data.inbox.ChatInteractorImpl;
import com.majcenovic.fitlog.data.inbox.ConversationsInteractorImpl;
import com.majcenovic.fitlog.data.log_in.LogInInteractorImpl;
import com.majcenovic.fitlog.data.meals.MealsInteractorImpl;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.people.PeopleInteractorImpl;
import com.majcenovic.fitlog.data.photos.PhotoInteractorImpl;
import com.majcenovic.fitlog.data.profile.ProfileInteractorImpl;
import com.majcenovic.fitlog.data.profile.edit.EditInteractorImpl;
import com.majcenovic.fitlog.data.progress.ProgressInteractorImpl;
import com.majcenovic.fitlog.data.progress.progress_month_year.ProgressMonthYearInteractorImpl;
import com.majcenovic.fitlog.data.progress.update_current_weight.UpdateGoalInteractorImpl;
import com.majcenovic.fitlog.data.search.SearchInteractorImpl;
import com.majcenovic.fitlog.data.sign_up.SignUpInteractorImpl;
import com.majcenovic.fitlog.data.status_editor.profile.StatusEditorInteractorImpl;
import com.majcenovic.fitlog.ui.comments.CommentsContract;
import com.majcenovic.fitlog.ui.comments.CommentsPresenter;
import com.majcenovic.fitlog.ui.goals.GoalsContract;
import com.majcenovic.fitlog.ui.goals.GoalsPresenter;
import com.majcenovic.fitlog.ui.log_in.LogInContract;
import com.majcenovic.fitlog.ui.log_in.LogInPresenter;
import com.majcenovic.fitlog.ui.main.activity.ActivityContract;
import com.majcenovic.fitlog.ui.main.activity.ActivityPresenter;
import com.majcenovic.fitlog.ui.main.activity.running_activity.RunningActivityContract;
import com.majcenovic.fitlog.ui.main.activity.running_activity.RunningActivityPresenter;
import com.majcenovic.fitlog.ui.main.diary.add_food_per_meal.AddFoodPerMealContract;
import com.majcenovic.fitlog.ui.main.diary.add_food_per_meal.AddFoodPerMealPresenter;
import com.majcenovic.fitlog.ui.main.diary.food_per_meal_list.FoodPerMealListContract;
import com.majcenovic.fitlog.ui.main.diary.food_per_meal_list.FoodPerMealListPresenter;
import com.majcenovic.fitlog.ui.main.diary.meals.MealsContract;
import com.majcenovic.fitlog.ui.main.diary.meals.MealsPresenter;
import com.majcenovic.fitlog.ui.main.home.HomeContract;
import com.majcenovic.fitlog.ui.main.home.HomePresenter;
import com.majcenovic.fitlog.ui.main.home.search.SearchContract;
import com.majcenovic.fitlog.ui.main.home.search.SearchPresenter;
import com.majcenovic.fitlog.ui.main.messages.chat.ChatContract;
import com.majcenovic.fitlog.ui.main.messages.chat.ChatPresenter;
import com.majcenovic.fitlog.ui.main.messages.conversations.ConversationsContract;
import com.majcenovic.fitlog.ui.main.messages.conversations.ConversationsPresenter;
import com.majcenovic.fitlog.ui.main.messages.friends.FriendsContract;
import com.majcenovic.fitlog.ui.main.messages.friends.FriendsPresenter;
import com.majcenovic.fitlog.ui.main.progress.ProgressContract;
import com.majcenovic.fitlog.ui.main.progress.ProgressPresenter;
import com.majcenovic.fitlog.ui.main.progress.month_year.ProgressMonthYearContract;
import com.majcenovic.fitlog.ui.main.progress.month_year.ProgressMonthYearPresenter;
import com.majcenovic.fitlog.ui.main.progress.update_current_weight.UpdateGoalContract;
import com.majcenovic.fitlog.ui.main.progress.update_current_weight.UpdateGoalPresenter;
import com.majcenovic.fitlog.ui.profile.ProfileContract;
import com.majcenovic.fitlog.ui.profile.ProfilePresenter;
import com.majcenovic.fitlog.ui.profile.edit.EditProfileActivity;
import com.majcenovic.fitlog.ui.profile.edit.EditProfileContract;
import com.majcenovic.fitlog.ui.profile.edit.EditProfilePresenter;
import com.majcenovic.fitlog.ui.profile.gallery.GalleryContract;
import com.majcenovic.fitlog.ui.profile.gallery.GalleryPresenter;
import com.majcenovic.fitlog.ui.profile.gallery.photo.PhotoContract;
import com.majcenovic.fitlog.ui.profile.gallery.photo.PhotoPresenter;
import com.majcenovic.fitlog.ui.profile.people.PeopleContract;
import com.majcenovic.fitlog.ui.profile.people.PeoplePresenter;
import com.majcenovic.fitlog.ui.sign_up.sign_up_first.SignUpFirstContract;
import com.majcenovic.fitlog.ui.sign_up.sign_up_first.SignUpFirstPresenter;
import com.majcenovic.fitlog.ui.sign_up.sign_up_second.SignUpSecondContract;
import com.majcenovic.fitlog.ui.sign_up.sign_up_second.SignUpSecondPresenter;
import com.majcenovic.fitlog.ui.sign_up.sign_up_third.SignUpThirdContract;
import com.majcenovic.fitlog.ui.sign_up.sign_up_third.SignUpThirdPresenter;
import com.majcenovic.fitlog.ui.status_editor.StatusEditorContract;
import com.majcenovic.fitlog.ui.status_editor.StatusEditorPresenter;

/**
 * Created by martina on 26/06/2017.
 */

public class Injection {

    public static LogInContract.Presenter provideLogInPresenter(LogInContract.View view, Context context) {
        return new LogInPresenter(view, new LogInInteractorImpl(context));
    }

    public static SignUpFirstContract.Presenter provideSignUpFirstPresenter(SignUpFirstContract.View view) {
        return new SignUpFirstPresenter(view);
    }

    public static SignUpSecondContract.Presenter provideSignUpSecondPresenter(SignUpSecondContract.View view) {
        return new SignUpSecondPresenter(view);
    }

    public static SignUpThirdContract.Presenter provideSignUpThirdPresenter(SignUpThirdContract.View view, Context context) {
        return new SignUpThirdPresenter(view, new SignUpInteractorImpl(context));
    }

    public static MealsContract.Presenter provideMealsPresenter(MealsContract.View view, Context context) {
        return new MealsPresenter(view, new MealsInteractorImpl(context));
    }

    public static GoalsContract.Presenter provideGoalsPresenter(GoalsContract.View view, Context context) {
        return new GoalsPresenter(view, new GoalsInteractorImpl(context));
    }

    public static AddFoodPerMealContract.Presenter provideAddFoodPerMealPresenter(AddFoodPerMealContract.View view, Context context) {
        return new AddFoodPerMealPresenter(view, new MealsInteractorImpl(context));
    }

    public static RunningActivityContract.Presenter provideRunningActivityPresenter(RunningActivityContract.View view, Context context) {
        return new RunningActivityPresenter(view, new RunningActivityInteractorImpl(context));
    }

    public static ActivityContract.Presenter provideActivityPresenter(ActivityContract.View view, Context context) {
        return new ActivityPresenter(view, new ActivityInteractorImpl(context));
    }

    public static ChatContract.Presenter provideChatPresenter(ChatContract.View view, Context context, User friend) {
        return new ChatPresenter(view, new ChatInteractorImpl(context), friend);
    }

    public static ConversationsContract.Presenter provideConversationPresenter(ConversationsContract.View view, Context context) {
        return new ConversationsPresenter(view, new ConversationsInteractorImpl(context));
    }

    public static ProgressMonthYearContract.Presenter provideProgressMonthYearPresenter(ProgressMonthYearContract.View view, Context context) {
        return new ProgressMonthYearPresenter(view, new ProgressMonthYearInteractorImpl(context));
    }

    public static UpdateGoalContract.Presenter provideUpdateCurrentWeightPresenter(UpdateGoalContract.View view, Context context) {
        return new UpdateGoalPresenter(view, new UpdateGoalInteractorImpl(context));
    }

    public static ProgressContract.Presenter provideProgressPresenter(ProgressContract.View view, Context context) {
        return new ProgressPresenter(view, new ProgressInteractorImpl(context));
    }

    public static GalleryContract.Presenter provideGalleryPresenter(GalleryContract.View view, Context context) {
        return new GalleryPresenter(view, new GalleryInteractorImpl(context));
    }

    public static PeopleContract.Presenter providePeoplePresenter(PeopleContract.View view, Context context) {
        return new PeoplePresenter(view, new PeopleInteractorImpl(context));
    }

    public static ProfileContract.Presenter provideProfilePresenter(ProfileContract.View view, Context context) {
        return new ProfilePresenter(view, new ProfileInteractorImpl(context));
    }

    public static HomeContract.Presenter provideHomePresenter(HomeContract.View view, Context context) {
        return new HomePresenter(view, new HomeInteractorImpl(context));
    }

    public static StatusEditorContract.Presenter provideStatusEditorPresenter(StatusEditorContract.View view, Context context) {
        return new StatusEditorPresenter(view, new StatusEditorInteractorImpl(context));
    }

    public static FoodPerMealListContract.Presenter provideFoodPerMealListPresenter(FoodPerMealListContract.View view, Context context) {
        return new FoodPerMealListPresenter(view, new MealsInteractorImpl(context));
    }

    public static FriendsContract.Presenter provideFriendsPresenter(FriendsContract.View view, Context context) {
        return new FriendsPresenter(view, new PeopleInteractorImpl(context));
    }

    public static CommentsContract.Presenter provideCommentsPresenter(CommentsContract.View view, Context context) {
        return new CommentsPresenter(view, new CommentsInteractorImpl(context));
    }

    public static PhotoContract.Presenter providePhotoPresenter(PhotoContract.View view, Context context) {
        return new PhotoPresenter(view, new PhotoInteractorImpl(context));
    }

    public static SearchContract.Presenter provideSearchPresenter(SearchContract.View view, Context context) {
        return new SearchPresenter(view, new SearchInteractorImpl(context));
    }

    public static EditProfileContract.Presenter provideEditProfilePresenter(EditProfileContract.View view, Context context) {
        return new EditProfilePresenter(view, new EditInteractorImpl(context));
    }
}
