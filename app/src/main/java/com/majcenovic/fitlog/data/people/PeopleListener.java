package com.majcenovic.fitlog.data.people;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 08/08/2017.
 */

public interface PeopleListener extends BaseListener {

    void onPeopleLoaded(List<User> people);

    interface FollowCallback extends BaseListener {
        void onFollow();
    }

    interface UnfollowCallback extends BaseListener {
        void onUnfollow();
    }

}
