package com.majcenovic.fitlog.data.network;

import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Comment;
import com.majcenovic.fitlog.data.models.Conversation;
import com.majcenovic.fitlog.data.models.FinishedActivity;
import com.majcenovic.fitlog.data.models.Food;
import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.LikeResponse;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.data.models.Message;
import com.majcenovic.fitlog.data.models.PeopleResponse;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.models.UserFollow;
import com.majcenovic.fitlog.data.models.UserFood;
import com.majcenovic.fitlog.data.models.User;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by martina on 26/06/2017.
 */

public interface ApiService {

    @Multipart
    @POST("users/register")
    Call<User> register(@PartMap Map<String, RequestBody> map);

    @FormUrlEncoded
    @POST("users/login")
    Call<User> login(@Field("email") String email, @Field("password") String password);

    @GET("messages")
    Call<List<Conversation>> getConversations();

    @GET("messages/{user_id}")
    Call<List<Message>> getMessagesWithUser(@Path("user_id") Long id, @Query("from_id") Long fromId, @Query("type") String type);

    @FormUrlEncoded
    @POST("messages/{id}")
    Call<Message> postMessage(@Path("id") Long id, @Field("message") String message);

    @GET("meals")
    Call<List<Meal>> getMealsByDay(@Query("day_offset") Integer dayIndex);

    @GET("meals/user-foods")
    Call<List<UserFood>> getFoodPerMeal(@Query("meal_type") Integer mealType, @Query("day_offset") Integer dayIndex);

    @FormUrlEncoded
    @POST("user-foods")
    Call<UserFood> postFoodPerMeal(@Field("meal_type") Integer mealType,
                                   @Field("amount") Integer amount,
                                   @Field("food_id") Long foodId,
                                   @Field("eaten_at") String eatenAt
    );

    @FormUrlEncoded
    @POST("activities/{activity_id}/finish")
    Call<FinishedActivity> postFinishedActivity(@Path("activity_id") Long activityId,
                                                @Field("duration") Integer duration,
                                                @Field("distance") Integer distance,
                                                @Field("average_speed") Double avgSpeed,
                                                @Field("top_speed") Double topSpeed);

    @FormUrlEncoded
    @POST("activities")
    Call<Activity> createActivity(@Field("type") String activityType);

    @GET("foods")
    Call<List<Food>> getAllFood(@Query("keyword") String keyword);

    @FormUrlEncoded
    @POST("activities/{activity_id}")
    Call<GpsLocation> postLocation(@Path("activity_id") Long id,
                                   @Field("latitude") Double latitude,
                                   @Field("longitude") Double longitude);

    @GET("activities")
    Call<List<Activity>> getAllActivities(@Query("day_offset") Integer dayIndex);

    @FormUrlEncoded
    @POST("me/pushes")
    Call<BaseModel> updatePushTokens(@FieldMap Map<String, Object> map);

    @GET("users/{id}")
    Call<User> getUser(@Path("id") Long id);

    @GET("progress")
    Call<List<Progress>> getProgress(@Query("period_type") String type);

    @FormUrlEncoded
    @POST("progress")
    Call<Progress> postProgress(@Field("current_weight") Double currentWeight);

    @GET("goals")
    Call<Goal> getGoals();

    @GET("users/{user_id}/board")
    Call<List<Post>> getBoardPosts(@Path("user_id") Long id);

    @GET("news")
    Call<List<Post>> getNews();

    @Multipart
    @POST("posts")
    Call<BaseModel> createPost(@PartMap Map<String, RequestBody> map);

    @Multipart
    @POST("users/{id}/posts")
    Call<BaseModel> postPostToSomeUser(@Path("id") Long id, @PartMap Map<String, RequestBody> map);

    @GET("users")
    Call<List<User>> getAllUsersByKeyword(@Query("keyword") String keyword);

    @GET("meals/calories-status")
    Call<CaloriesStatus> getCaloriesStatus();

    @DELETE("user-foods/{id}")
    Call<BaseModel> deleteSingleFood(@Path("id") Long id);

    @GET("users/{user_id}/friends")
    Call<List<User>> getUserFriends(@Path("user_id") Long id);

    @POST("users/follow/{id}")
    Call<UserFollow> follow(@Path("id") Long id);

    @DELETE("users/follow/{id}")
    Call<UserFollow> unfollow(@Path("id") Long id);

    @POST("posts/{id}/likes")
    Call<BaseModel> likePost(@Path("id") Long id);

    @DELETE("posts/{id}/likes")
    Call<BaseModel> dislikePost(@Path("id") Long id);

    @POST("comments/{id}/likes")
    Call<BaseModel> likeComment(@Path("id") Long id);

    @DELETE("comments/{id}/likes")
    Call<BaseModel> dislikeComment(@Path("id") Long id);

    @GET("posts/{id}/comments")
    Call<List<Comment>> getCommentList(@Path("id") Long id);

    @FormUrlEncoded
    @POST("posts/{id}/comments")
    Call<Comment> postComment(@Path("id") Long id, @Field("message") String comment);

    @GET("posts/{id}")
    Call<Post> getPost(@Path("id") Long id);

    @GET("users/{user_id}/photos")
    Call<List<Post>> getPhotoPosts(@Path("user_id") Long id);

    @FormUrlEncoded
    @POST("goals-weekly")
    Call<BaseModel> postWeeklyGoal(@Field("weekly_goal") Double weeklyGoal);

    @FormUrlEncoded
    @POST("goals-final")
    Call<BaseModel> postFinalGoal(@Field("final_goal") Double finalGoal);

    @FormUrlEncoded
    @PUT("activity-levels")
    Call<BaseModel> postActivityLevel(@Field("activity_level") String activityLevel);

    @Multipart
    @POST("me")
    Call<User> updateProfile(@PartMap Map<String, RequestBody> map);

    @GET("me")
    Call<User> getMyProfile();
}