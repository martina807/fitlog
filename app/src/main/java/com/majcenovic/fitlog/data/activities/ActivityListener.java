package com.majcenovic.fitlog.data.activities;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Activity;

import java.util.List;

/**
 * Created by martina on 05/08/2017.
 */

public interface ActivityListener {

    interface GetActivity extends BaseListener {
        void onActivitesLoaded(List<Activity> activities);
    }

    interface PostActivity extends BaseListener {
        void onActivityPosted();
    }
}
