package com.majcenovic.fitlog.data.progress;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.activities.ActivityListener;

/**
 * Created by martina on 09/08/2017.
 */

public interface ProgressInteractor extends BaseInteractor {

    void getGoals(ProgressListener.GoalsListener callback);
}
