package com.majcenovic.fitlog.data;

/**
 * Created by martina on 26/06/2017.
 */

public interface BaseInteractor<T extends BaseListener> {

    void cancel();

    void reset();
}
