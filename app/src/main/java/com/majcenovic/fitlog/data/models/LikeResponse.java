package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 08/09/2017.
 */

public class LikeResponse extends BaseModel {

    @SerializedName("type")
    private String type;

    @SerializedName("post_id")
    private Long postId;

    @SerializedName("comment_id")
    private Long commentId;

    @SerializedName("user_id")
    private Long userId;

    public String getType() {
        return type;
    }

    public Long getPostId() {
        return postId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public Long getUserId() {
        return userId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.type);
        dest.writeValue(this.postId);
        dest.writeValue(this.commentId);
        dest.writeValue(this.userId);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public LikeResponse() {
    }

    protected LikeResponse(Parcel in) {
        super(in);
        this.type = in.readString();
        this.postId = (Long) in.readValue(Long.class.getClassLoader());
        this.commentId = (Long) in.readValue(Long.class.getClassLoader());
        this.userId = (Long) in.readValue(Long.class.getClassLoader());
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<LikeResponse> CREATOR = new Creator<LikeResponse>() {
        @Override
        public LikeResponse createFromParcel(Parcel source) {
            return new LikeResponse(source);
        }

        @Override
        public LikeResponse[] newArray(int size) {
            return new LikeResponse[size];
        }
    };
}
