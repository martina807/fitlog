package com.majcenovic.fitlog.data.profile.edit;

import android.content.Context;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.UpdateUserDto;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.util.ApiErrorUtil;
import com.majcenovic.fitlog.util.RequestUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/09/2017.
 */

public class EditInteractorImpl implements EditInteractor {

    private final Context context;
    private boolean isCanceled;

    public EditInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void getMyProfile(final EditListener.GetMyProfileCallback callback) {
        ApiManager.getApiService(context).getMyProfile().enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(isCanceled) return;

                if (response.isSuccessful()) {
                    User user = SharedPrefs.readUserData(context);
                    String token = user.getAccessToken();
                    String email = user.getEmail();
                    User updated = response.body();
                    updated.setAccessToken(token);
                    updated.setEmail(email);
                    SharedPrefs.saveUserData(context, updated);
                    callback.onProfileLoaded(response.body());
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void updateProfile(UpdateUserDto updateUserDto, final EditListener.ProfileUpdateCallback callback) {
        ApiManager.getApiService(context).updateProfile(RequestUtil.mapOf(updateUserDto)).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(isCanceled) return;

                if (response.isSuccessful()) {
                    User user = SharedPrefs.readUserData(context);
                    String token = user.getAccessToken();
                    String email = user.getEmail();
                    User updated = response.body();
                    updated.setAccessToken(token);
                    updated.setEmail(email);
                    SharedPrefs.saveUserData(context, updated);
                    callback.onProfileUpdated(response.body());
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}