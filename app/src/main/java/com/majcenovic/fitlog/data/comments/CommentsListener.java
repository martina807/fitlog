package com.majcenovic.fitlog.data.comments;
import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Comment;

import java.util.List;

/**
 * Created by martina on 08/09/2017.
 */

public interface CommentsListener extends BaseListener {

    void onCommentsLoaded(List<Comment> commentList);

    interface PostComment extends BaseListener {
        void onCommentPosted(Comment comment);
    }

    interface LikeComment extends BaseListener {
        void onCommentLiked();
    }

    interface DislikeComment extends BaseListener {
        void onCommentDisliked();
    }
}
