package com.majcenovic.fitlog.data.home;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Food;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.UserFood;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface HomeListener {

    interface PostsListener extends BaseListener {

        void onNewsLoaded(List<Post> posts);
    }

    interface CalorieStatusCallback extends BaseListener {

        void onCaloriesStatusLoaded(CaloriesStatus calorieStatus);
    }

    interface LikeCallback extends BaseListener {
        void onLikePosted();
    }

    interface DislikeCallback extends BaseListener {
        void onDislikePosted();
    }
}
