package com.majcenovic.fitlog.data.inbox;

import android.content.Context;

import com.majcenovic.fitlog.data.models.Conversation;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 07/08/2017.
 */

public class ConversationsInteractorImpl implements ConversationsInteractor {

    private final Context context;
    private boolean isCanceled;

    public ConversationsInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void loadConversations(final InboxListener.ConversationsListener callback) {
        ApiManager.getApiService(context).getConversations().enqueue(new Callback<List<Conversation>>() {
            @Override
            public void onResponse(Call<List<Conversation>> call, Response<List<Conversation>> response) {
                if(isCanceled) return;

                if(response.isSuccessful()) {
                    List<Conversation> conversations = response.body();
                    callback.onConversationsLoaded(conversations);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Conversation>> call, Throwable t) {
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}
