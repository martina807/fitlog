package com.majcenovic.fitlog.data.activities;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.dto.PostDto;

/**
 * Created by martina on 05/08/2017.
 */

public interface ActivityInteractor extends BaseInteractor{

    void loadActivities(Integer dayOffset, ActivityListener.GetActivity callback);
    void postActivity(PostDto activity, ActivityListener.PostActivity callback);

}
