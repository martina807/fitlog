package com.majcenovic.fitlog.data.profile.edit;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.dto.UpdateUserDto;

/**
 * Created by martina on 09/09/2017.
 */

public interface EditInteractor extends BaseInteractor {

    void getMyProfile(EditListener.GetMyProfileCallback callback);
    void updateProfile(UpdateUserDto updateUserDto, EditListener.ProfileUpdateCallback callback);
}