package com.majcenovic.fitlog.data.search;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.User;

/**
 * Created by martina on 09/09/2017.
 */

public interface SearchInteractor extends BaseInteractor {

    void searchUsers(String input, SearchListener.PeopleCallback callback);

    void follow(User user, SearchListener.FollowCallback callback);

    void unfollow(User user, SearchListener.UnfollowCallback callback);
}