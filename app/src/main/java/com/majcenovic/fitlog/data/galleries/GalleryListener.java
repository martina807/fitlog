package com.majcenovic.fitlog.data.galleries;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Post;

import java.util.List;

/**
 * Created by martina on 08/08/2017.
 */

public interface GalleryListener extends BaseListener {

    interface PostCallback extends BaseListener {
        void onPostsLoaded(List<Post> posts);
    }

    interface LikeCallback extends BaseListener {
        void onLikePosted();
    }

    interface DislikeCallback extends BaseListener {
        void onDislikePosted();
    }
}
