package com.majcenovic.fitlog.data.goals;

import android.content.Context;

import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/09/2017.
 */

public class GoalsInteractorImpl implements GoalsInteractor {

    private final Context context;

    public GoalsInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }


    @Override
    public void getGoals(final GoalsListener.GetGoalsCallback callback) {
        ApiManager.getApiService(context).getGoals().enqueue(new Callback<Goal>() {
            @Override
            public void onResponse(Call<Goal> call, Response<Goal> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    Goal goal = response.body();
                    callback.onGoalsLoaded(goal);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<Goal> call, Throwable t) {
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void updateCurrentWeight(Double currentWeight, final GoalsListener.UpdateCurrentWeight callback) {
        ApiManager.getApiService(context).postProgress(currentWeight).enqueue(new Callback<Progress>() {
            @Override
            public void onResponse(Call<Progress> call, Response<Progress> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onCurrentWeightUpdated();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<Progress> call, Throwable t) {
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void updateWeeklyGoal(Double weeklyGoal, final GoalsListener.UpdateWeeklyGoal callback) {
        ApiManager.getApiService(context).postWeeklyGoal(weeklyGoal).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onWeeklyGoalUpdated();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void updateFinalGoal(Double finalGoal, final GoalsListener.UpdateFinalGoal callback) {
        ApiManager.getApiService(context).postFinalGoal(finalGoal).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onFinalGoalUpdated();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void updateActivityLevel(String activityLevel, final GoalsListener.UpdateActivityLevel callback) {
        ApiManager.getApiService(context).postActivityLevel(activityLevel).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onActivityLevelUpdated();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}