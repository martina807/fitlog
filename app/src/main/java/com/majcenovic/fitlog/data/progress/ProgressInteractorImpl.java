package com.majcenovic.fitlog.data.progress;

import android.content.Context;

import com.majcenovic.fitlog.data.activities.ActivityListener;
import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/08/2017.
 */

public class ProgressInteractorImpl implements ProgressInteractor {
    private final Context context;

    public ProgressInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }


    @Override
    public void getGoals(final ProgressListener.GoalsListener callback) {
        ApiManager.getApiService(context).getGoals().enqueue(new Callback<Goal>() {
            @Override
            public void onResponse(Call<Goal> call, Response<Goal> response) {
                if(isCanceled) return;

                if(response.isSuccessful()) {
                    Goal goal = response.body();
                    callback.onGoalsLoaded(goal);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<Goal> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}
