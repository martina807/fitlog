package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Activity extends BaseModel {

    @SerializedName("duration")
    private int duration;

    @SerializedName("is_active")
    private boolean isActive;

    @SerializedName("distance")
    private int distance;

    @SerializedName("calories_burned")
    private int caloriesBurned;

    @SerializedName("top_speed")
    private double topSpeed;

    @SerializedName("locations")
    private List<GpsLocation> locations;

    @SerializedName("type")
    private String type;

    @SerializedName("average_speed")
    private double averageSpeed;

    public Activity(int duration,
                    int distance,
                    double topSpeed,
                    double averageSpeed) {
        this.duration = duration;
        this.distance = distance;
        this.topSpeed = topSpeed;
        this.averageSpeed = averageSpeed;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDistance() {
        return distance;
    }

    public void setCaloriesBurned(int caloriesBurned) {
        this.caloriesBurned = caloriesBurned;
    }

    public int getCaloriesBurned() {
        return caloriesBurned;
    }

    public void setTopSpeed(double topSpeed) {
        this.topSpeed = topSpeed;
    }

    public double getTopSpeed() {
        return topSpeed;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setLocations(List<GpsLocation> locations) {
        this.locations = locations;
    }

    public List<GpsLocation> getLocations() {
        return locations;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.duration);
        dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
        dest.writeInt(this.distance);
        dest.writeInt(this.caloriesBurned);
        dest.writeDouble(this.topSpeed);
        dest.writeList(this.locations);
        dest.writeString(this.type);
        dest.writeDouble(this.averageSpeed);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public Activity() {
    }

    protected Activity(Parcel in) {
        super(in);
        this.duration = in.readInt();
        this.isActive = in.readByte() != 0;
        this.distance = in.readInt();
        this.caloriesBurned = in.readInt();
        this.topSpeed = in.readDouble();
        this.locations = new ArrayList<GpsLocation>();
        in.readList(this.locations, GpsLocation.class.getClassLoader());
        this.type = in.readString();
        this.averageSpeed = in.readDouble();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<Activity> CREATOR = new Creator<Activity>() {
        @Override
        public Activity createFromParcel(Parcel source) {
            return new Activity(source);
        }

        @Override
        public Activity[] newArray(int size) {
            return new Activity[size];
        }
    };
}