package com.majcenovic.fitlog.data.progress.progress_month_year;

import android.content.Context;

import com.majcenovic.fitlog.data.activities.ActivityListener;
import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.models.dto.PostDto;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.data.progress.ProgressListener;
import com.majcenovic.fitlog.util.ApiErrorUtil;
import com.majcenovic.fitlog.util.RequestUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 08/08/2017.
 */

public class ProgressMonthYearInteractorImpl implements ProgressMonthYearInteractor {

    private final Context context;

    public ProgressMonthYearInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void getProgressForPeriod(String period, final com.majcenovic.fitlog.data.progress.ProgressListener.ProgressMonthYearListener callback) {

        ApiManager.getApiService(context).getProgress(period).enqueue(new Callback<List<Progress>>() {
            @Override
            public void onResponse(Call<List<Progress>> call, Response<List<Progress>> response) {
                if(isCanceled) return;

                if(response.isSuccessful()) {
                    List<Progress> progresses = response.body();
                    callback.onProgressLoaded(progresses);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Progress>> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void postProgress(PostDto progress, final ProgressListener.PostProgress callback) {
        ApiManager.getApiService(context)
                .createPost(RequestUtil.mapOf(progress))
                .enqueue(new Callback<BaseModel>() {
                    @Override
                    public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                        if(isCanceled) return;

                        if(response.isSuccessful()) {
                            callback.onProgressPosted();
                        } else {
                            callback.onFailure(ApiErrorUtil.getMessage(context, response));
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseModel> call, Throwable t) {
                        if (isCanceled) return;
                        callback.onFailure(ApiErrorUtil.getMessage(context, t));
                    }
                });
    }
}
