package com.majcenovic.fitlog.data.home;

import android.content.Context;

import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.LikeResponse;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.data.profile.ProfileListener;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 28/06/2017.
 */

public class HomeInteractorImpl implements HomeInteractor {

    private final Context context;

    public HomeInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void getNews(final HomeListener.PostsListener callback) {
        ApiManager.getApiService(context).getNews().enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    List<Post> posts = response.body();

                    callback.onNewsLoaded(posts);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void getCaloriesStatus(final HomeListener.CalorieStatusCallback callback) {
        ApiManager.getApiService(context).getCaloriesStatus().enqueue(new Callback<CaloriesStatus>() {
            @Override
            public void onResponse(Call<CaloriesStatus> call, Response<CaloriesStatus> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    CaloriesStatus caloriesStatus = response.body();
                    callback.onCaloriesStatusLoaded(caloriesStatus);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }

            }

            @Override
            public void onFailure(Call<CaloriesStatus> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void like(Post post, final HomeListener.LikeCallback callback) {
        ApiManager.getApiService(context).likePost(post.getId()).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;
                if (response.isSuccessful()) {
                    callback.onLikePosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void dislike(Post post, final HomeListener.DislikeCallback callback) {
        ApiManager.getApiService(context).dislikePost(post.getId()).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;
                if (response.isSuccessful()) {
                    callback.onDislikePosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}