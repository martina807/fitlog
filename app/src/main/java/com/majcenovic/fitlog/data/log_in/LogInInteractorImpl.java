package com.majcenovic.fitlog.data.log_in;

import android.content.Context;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 26/06/2017.
 */

public class LogInInteractorImpl implements LogInInteractor {

    private final Context context;

    public LogInInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void loginUser(String email, String password, final LogInListener callback) {

        ApiManager.getApiService(context).login(email, password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(isCanceled) return;

                if(response.isSuccessful()) {

                    User user = response.body();

                    SharedPrefs.saveUserData(context, user);

                    ApiManager.currentUser = user;

                    callback.onLoggedIn(user);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}
