package com.majcenovic.fitlog.data.photos;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Post;

import java.util.List;

/**
 * Created by martina on 08/08/2017.
 */

public interface PhotoListener extends BaseListener {

    interface LikeCallback extends BaseListener {
        void onLikePosted();
    }

    interface DislikeCallback extends BaseListener {
        void onDislikePosted();
    }

    interface PostCallback extends BaseListener {
        void onPostLoaded(Post post);
    }
}