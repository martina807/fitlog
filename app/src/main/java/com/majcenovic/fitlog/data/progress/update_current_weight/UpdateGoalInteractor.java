package com.majcenovic.fitlog.data.progress.update_current_weight;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.progress.ProgressListener;

/**
 * Created by martina on 09/08/2017.
 */

public interface UpdateGoalInteractor extends BaseInteractor {

    void postCurrentWeight(Double currentWeight, ProgressListener.UpdateCurrentWeightListener callback);
    void postWeeklyGoal(Double weeklyGoal, ProgressListener.UpdateWeeklyGoalListener callback);
    void postFinalGoal(Double finalGoal, ProgressListener.UpdateFinalGoalListener callback);
    void postActivityLevel(String activityLevel, ProgressListener.UpdateActivityLevelListener callback);
}