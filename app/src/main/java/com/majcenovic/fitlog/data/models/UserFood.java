package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 18/03/2017.
 */

public class UserFood extends BaseModel {

    @SerializedName("food")
    private Food food;

    @SerializedName("amount")
    private Integer amount;

    @SerializedName("calories")
    private Double calories;

    @SerializedName("eaten_at")
    private String eatenAt;

    public Food getFood() {
        return food;
    }

    public Integer getAmount() {
        return amount;
    }

    public Double getCalories() {
        return calories;
    }

    public String getEatenAt() {
        return eatenAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(this.food, flags);
        dest.writeValue(this.amount);
        dest.writeValue(this.calories);
        dest.writeString(this.eatenAt);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public UserFood() {
    }

    protected UserFood(Parcel in) {
        super(in);
        this.food = in.readParcelable(Food.class.getClassLoader());
        this.amount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.calories = (Double) in.readValue(Double.class.getClassLoader());
        this.eatenAt = in.readString();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<UserFood> CREATOR = new Creator<UserFood>() {
        @Override
        public UserFood createFromParcel(Parcel source) {
            return new UserFood(source);
        }

        @Override
        public UserFood[] newArray(int size) {
            return new UserFood[size];
        }
    };
}
