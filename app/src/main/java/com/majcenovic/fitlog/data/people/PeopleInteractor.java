package com.majcenovic.fitlog.data.people;

import com.majcenovic.fitlog.data.BaseInteractor;

/**
 * Created by martina on 09/08/2017.
 */

public interface PeopleInteractor extends BaseInteractor {

    void getPeople(Long userId, PeopleListener callback);
    void follow(Long userId, PeopleListener.FollowCallback callback);
    void unfollow(Long userId, PeopleListener.UnfollowCallback callback);

}
