package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;
import com.majcenovic.fitlog.util.ListRowTypeUtils;

/**
 * Created by martina on 30/01/2017.
 */

public class Message extends BaseModel {

    public Message() {

    }

    private boolean isHeader;
    private boolean isShowDate;

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public boolean isShowDate() {
        return isShowDate;
    }

    public void setShowDate(boolean showDate) {
        isShowDate = showDate;
    }


    @SerializedName("sender_id")
    private Long senderId;

    @SerializedName("receiver_id")
    private Long receiverId;

    public Long getSenderId() {
        return senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public Integer getListType() {
        return listType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte(this.isHeader ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isShowDate ? (byte) 1 : (byte) 0);
        dest.writeValue(this.listType);
        dest.writeValue(this.senderId);
        dest.writeValue(this.receiverId);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    protected Message(Parcel in) {
        super(in);
        this.isHeader = in.readByte() != 0;
        this.isShowDate = in.readByte() != 0;
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.senderId = (Long) in.readValue(Long.class.getClassLoader());
        this.receiverId = (Long) in.readValue(Long.class.getClassLoader());
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
