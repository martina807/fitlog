package com.majcenovic.fitlog.data.inbox;

import com.majcenovic.fitlog.data.BaseInteractor;

/**
 * Created by martina on 07/08/2017.
 */

public interface ChatInteractor extends BaseInteractor {

    void getUserById(Long userId, InboxListener.UserListener callback);
    void postMessage(Long userId, String message, InboxListener.CreateChatMessageListener callback);
    void getMessagesWithUser(Long userId, Long fromMessageId, String type, InboxListener.GetChatMessagesListener callback);
    void chatStarted(Long friendId);
    void chatStopped(Long friendId);
}