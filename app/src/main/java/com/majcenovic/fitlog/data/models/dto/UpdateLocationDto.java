package com.majcenovic.fitlog.data.models.dto;

/**
 * Created by martina on 10/09/2017.
 */

public class UpdateLocationDto {

    private Double latitude;
    private Double longitude;

    public UpdateLocationDto(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}