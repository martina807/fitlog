package com.majcenovic.fitlog.data.search;

import android.content.Context;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.UserFollow;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/09/2017.
 */

public class SearchInteractorImpl implements SearchInteractor {

    private final Context context;

    public SearchInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void searchUsers(String input, final SearchListener.PeopleCallback callback) {
        ApiManager.getApiService(context).getAllUsersByKeyword(input).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onPeopleLoaded(response.body());
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void follow(User user, final SearchListener.FollowCallback callback) {
        ApiManager.getApiService(context).follow(user.getId()).enqueue(new Callback<UserFollow>() {
            @Override
            public void onResponse(Call<UserFollow> call, Response<UserFollow> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onFollow();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<UserFollow> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void unfollow(User user, final SearchListener.UnfollowCallback callback) {
        ApiManager.getApiService(context).unfollow(user.getId()).enqueue(new Callback<UserFollow>() {
            @Override
            public void onResponse(Call<UserFollow> call, Response<UserFollow> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onUnfollow();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<UserFollow> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}