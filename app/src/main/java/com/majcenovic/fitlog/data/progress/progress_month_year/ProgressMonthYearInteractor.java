package com.majcenovic.fitlog.data.progress.progress_month_year;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.dto.PostDto;
import com.majcenovic.fitlog.data.progress.ProgressListener;

/**
 * Created by martina on 08/08/2017.
 */

public interface ProgressMonthYearInteractor extends BaseInteractor {

    void getProgressForPeriod(String period, ProgressListener.ProgressMonthYearListener callback);
    void postProgress(PostDto progress, ProgressListener.PostProgress callback);
}
