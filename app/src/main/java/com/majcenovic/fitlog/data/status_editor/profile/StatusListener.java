package com.majcenovic.fitlog.data.status_editor.profile;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 04/09/2017.
 */

public class StatusListener {

    public interface StatusEditorListener extends BaseListener {
        void onStatusPosted();
    }

    public interface AllUsersListener extends BaseListener {
        void onAllUsersLoaded(List<User> users);
    }
}
