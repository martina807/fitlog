package com.majcenovic.fitlog.data.progress.update_current_weight;

import android.content.Context;

import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.data.progress.ProgressListener;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/08/2017.
 */

public class UpdateGoalInteractorImpl implements UpdateGoalInteractor {
    private final Context context;

    public UpdateGoalInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void postCurrentWeight(Double currentWeight, final ProgressListener.UpdateCurrentWeightListener callback) {
        ApiManager.getApiService(context).postProgress(currentWeight).enqueue(new Callback<Progress>() {
            @Override
            public void onResponse(Call<Progress> call, Response<Progress> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onCurrentWeightPosted();
                } else {
                    if (isCanceled) return;

                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<Progress> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void postWeeklyGoal(Double weeklyGoal, final ProgressListener.UpdateWeeklyGoalListener callback) {
        ApiManager.getApiService(context).postWeeklyGoal(weeklyGoal).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onWeeklyGoalPosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void postFinalGoal(Double finalGoal, final ProgressListener.UpdateFinalGoalListener callback) {
        ApiManager.getApiService(context).postFinalGoal(finalGoal).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onFinalGoalPosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void postActivityLevel(String activityLevel, final ProgressListener.UpdateActivityLevelListener callback) {
        ApiManager.getApiService(context).postActivityLevel(activityLevel).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onActivityLevelPosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}
