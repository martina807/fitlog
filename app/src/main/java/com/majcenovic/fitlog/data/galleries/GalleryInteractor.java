package com.majcenovic.fitlog.data.galleries;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.Post;

/**
 * Created by martina on 09/08/2017.
 */

public interface GalleryInteractor extends BaseInteractor {

    void like(Post post, GalleryListener.LikeCallback callback);

    void dislike(Post post, GalleryListener.DislikeCallback callback);

    void loadPhotoPosts(Long userId, GalleryListener.PostCallback callback);
}
