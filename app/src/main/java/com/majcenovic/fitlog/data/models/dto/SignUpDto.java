package com.majcenovic.fitlog.data.models.dto;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpDto implements Parcelable {

    String firstName;
    String lastName;
    String password;
    String email;
    int gender;
    String birthday;
    int height;
    Double currentWeight;
    String activityLevel;
    String weeklyGoalWeight;
    Double goalWeight;
    File image;

    public SignUpDto(String firstName, String lastName, String password, String email, File image) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.image = image;
    }

    public SignUpDto(int gender, String birthday, int height, Double currentWeight) {
        this.gender = gender;
        this.birthday = birthday;
        this.height = height;
        this.currentWeight = currentWeight;
    }

    public SignUpDto(String activityLevel, String weeklyGoalWeight, Double goalWeight) {
        this.activityLevel = activityLevel;
        this.weeklyGoalWeight = weeklyGoalWeight;
        this.goalWeight = goalWeight;
    }

    public File getImage() {
        return image;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getGender() {
        return gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public int getHeight() {
        return height;
    }

    public Double getCurrentWeight() {
        return currentWeight;
    }

    public String getActivityLevel() {
        return activityLevel;
    }

    public String getWeeklyGoalWeight() {
        return weeklyGoalWeight;
    }

    public Double getGoalWeight() {
        return goalWeight;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setCurrentWeight(Double currentWeight) {
        this.currentWeight = currentWeight;
    }

    public void setActivityLevel(String activityLevel) {
        this.activityLevel = activityLevel;
    }

    public void setWeeklyGoalWeight(String weeklyGoalWeight) {
        this.weeklyGoalWeight = weeklyGoalWeight;
    }

    public void setGoalWeight(Double goalWeight) {
        this.goalWeight = goalWeight;
    }

    public void setImage(File image) {
        this.image = image;
    }


    public SignUpDto() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.password);
        dest.writeString(this.email);
        dest.writeInt(this.gender);
        dest.writeString(this.birthday);
        dest.writeInt(this.height);
        dest.writeValue(this.currentWeight);
        dest.writeString(this.activityLevel);
        dest.writeString(this.weeklyGoalWeight);
        dest.writeValue(this.goalWeight);
        dest.writeSerializable(this.image);
    }

    protected SignUpDto(Parcel in) {
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.password = in.readString();
        this.email = in.readString();
        this.gender = in.readInt();
        this.birthday = in.readString();
        this.height = in.readInt();
        this.currentWeight = (Double) in.readValue(Double.class.getClassLoader());
        this.activityLevel = in.readString();
        this.weeklyGoalWeight = in.readString();
        this.goalWeight = (Double) in.readValue(Double.class.getClassLoader());
        this.image = (File) in.readSerializable();
    }

    public static final Creator<SignUpDto> CREATOR = new Creator<SignUpDto>() {
        @Override
        public SignUpDto createFromParcel(Parcel source) {
            return new SignUpDto(source);
        }

        @Override
        public SignUpDto[] newArray(int size) {
            return new SignUpDto[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SignUpDto)) return false;

        SignUpDto signUpDto = (SignUpDto) o;

        if (getGender() != signUpDto.getGender()) return false;
        if (getHeight() != signUpDto.getHeight()) return false;
        if (getFirstName() != null ? !getFirstName().equals(signUpDto.getFirstName()) : signUpDto.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(signUpDto.getLastName()) : signUpDto.getLastName() != null)
            return false;
        if (getPassword() != null ? !getPassword().equals(signUpDto.getPassword()) : signUpDto.getPassword() != null)
            return false;
        if (getEmail() != null ? !getEmail().equals(signUpDto.getEmail()) : signUpDto.getEmail() != null)
            return false;
        if (getBirthday() != null ? !getBirthday().equals(signUpDto.getBirthday()) : signUpDto.getBirthday() != null)
            return false;
        if (getCurrentWeight() != null ? !getCurrentWeight().equals(signUpDto.getCurrentWeight()) : signUpDto.getCurrentWeight() != null)
            return false;
        if (getActivityLevel() != null ? !getActivityLevel().equals(signUpDto.getActivityLevel()) : signUpDto.getActivityLevel() != null)
            return false;
        if (getWeeklyGoalWeight() != null ? !getWeeklyGoalWeight().equals(signUpDto.getWeeklyGoalWeight()) : signUpDto.getWeeklyGoalWeight() != null)
            return false;
        if (getGoalWeight() != null ? !getGoalWeight().equals(signUpDto.getGoalWeight()) : signUpDto.getGoalWeight() != null)
            return false;
        return getImage() != null ? getImage().equals(signUpDto.getImage()) : signUpDto.getImage() == null;

    }

    @Override
    public int hashCode() {
        int result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + getGender();
        result = 31 * result + (getBirthday() != null ? getBirthday().hashCode() : 0);
        result = 31 * result + getHeight();
        result = 31 * result + (getCurrentWeight() != null ? getCurrentWeight().hashCode() : 0);
        result = 31 * result + (getActivityLevel() != null ? getActivityLevel().hashCode() : 0);
        result = 31 * result + (getWeeklyGoalWeight() != null ? getWeeklyGoalWeight().hashCode() : 0);
        result = 31 * result + (getGoalWeight() != null ? getGoalWeight().hashCode() : 0);
        result = 31 * result + (getImage() != null ? getImage().hashCode() : 0);
        return result;
    }
}
