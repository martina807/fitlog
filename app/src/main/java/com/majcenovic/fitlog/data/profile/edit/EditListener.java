package com.majcenovic.fitlog.data.profile.edit;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.User;

/**
 * Created by martina on 09/09/2017.
 */

public interface EditListener {

    interface GetMyProfileCallback extends BaseListener {
        void onProfileLoaded(User user);
    }

    interface ProfileUpdateCallback extends BaseListener {
        void onProfileUpdated(User user);
    }
}