package com.majcenovic.fitlog.data.activities;

import android.content.Context;

import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.dto.PostDto;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;
import com.majcenovic.fitlog.util.RequestUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 05/08/2017.
 */

public class ActivityInteractorImpl implements ActivityInteractor {

    private final Context context;

    boolean isCanceled;

    public ActivityInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void loadActivities(Integer dayOffset, final ActivityListener.GetActivity callback) {
        ApiManager.getApiService(context).getAllActivities(dayOffset).enqueue(new Callback<List<Activity>>() {
            @Override
            public void onResponse(Call<List<Activity>> call, Response<List<Activity>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    List<Activity> activities = response.body();

                    callback.onActivitesLoaded(activities);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Activity>> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void postActivity(PostDto activity, final ActivityListener.PostActivity callback) {
        ApiManager.getApiService(context)
                .createPost(RequestUtil.mapOf(activity))
                .enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if(isCanceled) return;

                if(response.isSuccessful()) {
                    callback.onActivityPosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}
