package com.majcenovic.fitlog.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.majcenovic.fitlog.util.ListRowTypeUtils;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by martina on 18/03/2017.
 */

public class BaseModel implements Parcelable {

    protected Integer listType = ListRowTypeUtils.CONTENT;

    @SerializedName("id")
    protected Long id;

    @SerializedName("created_at")
    protected String createdAt;

    @SerializedName("updated_at")
    protected String updateAt;

    @SerializedName("message")
    protected String message;

    public String getMessage() {
        return message;
    }

    public Long getId() { return id; }

    public String getCreatedAt() { return createdAt; }

    public String getUpdateAt() { return updateAt; }

    public BaseModel() {
    }

    public Integer getListType() {
        return listType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    protected BaseModel(Parcel in) {
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

}
