package com.majcenovic.fitlog.data.meals;

import android.content.Context;

import com.majcenovic.fitlog.data.home.HomeListener;
import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Food;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.data.models.UserFood;
import com.majcenovic.fitlog.data.models.dto.AddFoodPerMealDto;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 28/06/2017.
 */

public class MealsInteractorImpl implements MealsInteractor {

    private final Context context;

    public MealsInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void getMeals(int dayOffset, final MealsListener.GetMealsCallback callback) {

        ApiManager.getApiService(context).getMealsByDay(dayOffset).enqueue(new Callback<List<Meal>>() {
            @Override
            public void onResponse(Call<List<Meal>> call, Response<List<Meal>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    List<Meal> list = response.body();

                    callback.onMealsLoaded(list);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Meal>> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void getSingleMealFoods(int dayOffset, int mealType, final MealsListener.GetSingleMealFoodCallback callback) {

        ApiManager.getApiService(context).getFoodPerMeal(mealType, dayOffset).enqueue(new Callback<List<UserFood>>() {
            @Override
            public void onResponse(Call<List<UserFood>> call, Response<List<UserFood>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    List<UserFood> list = response.body();

                    callback.onSingleMealFoodLoaded(list);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<UserFood>> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void getAllFood(String keyword, final MealsListener.GetAllFoodCallback callback) {

        ApiManager.getApiService(context).getAllFood(keyword).enqueue(new Callback<List<Food>>() {
            @Override
            public void onResponse(Call<List<Food>> call, Response<List<Food>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    List<Food> list = response.body();

                    callback.onAllFoodLoaded(list);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Food>> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });

    }

    @Override
    public void postSingleFoodPerMeal(AddFoodPerMealDto addFoodPerMealDto, final MealsListener.PostSingleFoodPerMealCallback callback) {
        ApiManager.getApiService(context).postFoodPerMeal(addFoodPerMealDto.getMealType(),
                addFoodPerMealDto.getAmount(),
                addFoodPerMealDto.getFoodId(),
                addFoodPerMealDto.getEatenAt())
                .enqueue(new Callback<UserFood>() {
                    @Override
                    public void onResponse(Call<UserFood> call, Response<UserFood> response) {
                        if (isCanceled) return;

                        if (response.isSuccessful()) {
                            callback.onSingleFoodPerMealAdded();
                        } else {
                            callback.onFailure(ApiErrorUtil.getMessage(context, response));
                        }
                    }

                    @Override
                    public void onFailure(Call<UserFood> call, Throwable t) {
                        if(isCanceled) return;

                        callback.onFailure(ApiErrorUtil.getMessage(context, t));
                    }
                });
    }

    @Override
    public void deleteUserFood(Long id, final MealsListener.DeleteSingleFoodStatusCallback callback) {
        ApiManager.getApiService(context).deleteSingleFood(id).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onSingleFoodDeleted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }


    @Override
    public void getCaloriesStatus(final HomeListener.CalorieStatusCallback callback) {
        ApiManager.getApiService(context).getCaloriesStatus().enqueue(new Callback<CaloriesStatus>() {
            @Override
            public void onResponse(Call<CaloriesStatus> call, Response<CaloriesStatus> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    CaloriesStatus caloriesStatus = response.body();
                    callback.onCaloriesStatusLoaded(caloriesStatus);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }

            }

            @Override
            public void onFailure(Call<CaloriesStatus> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}