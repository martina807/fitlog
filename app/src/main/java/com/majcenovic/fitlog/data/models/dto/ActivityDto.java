package com.majcenovic.fitlog.data.models.dto;

/**
 * Created by martina on 06/08/2017.
 */

public class ActivityDto {

    Long activityId;
    Integer distance;
    Integer duration;
    Double topSpeed;
    Double avgSpeed;

    public ActivityDto(Long activityId, Integer distance, Integer duration, Double topSpeed, Double avgSpeed) {
        this.activityId = activityId;
        this.distance = distance;
        this.duration = duration;
        this.topSpeed = topSpeed;
        this.avgSpeed = avgSpeed;
    }

    public Long getActivityId() {
        return activityId;
    }

    public Integer getDistance() {
        return distance;
    }

    public Integer getDuration() {
        return duration;
    }

    public Double getTopSpeed() {
        return topSpeed;
    }

    public Double getAvgSpeed() {
        return avgSpeed;
    }
}
