package com.majcenovic.fitlog.data.comments;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.Comment;
import com.majcenovic.fitlog.data.models.Post;

/**
 * Created by martina on 08/09/2017.
 */

public interface CommentsInteractor extends BaseInteractor {

    void loadComments(Post post, CommentsListener callback);
    void postComment(Post post, String comment, CommentsListener.PostComment callback);
    void likeComment(Comment comment, CommentsListener.LikeComment callback);
    void dislikeComment(Comment comment, CommentsListener.DislikeComment callback);
}
