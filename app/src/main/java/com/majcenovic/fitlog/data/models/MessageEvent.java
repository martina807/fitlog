package com.majcenovic.fitlog.data.models;

/**
 * Created by martina on 08/08/2017.
 */

public class MessageEvent {

    private Long openLocationId;

    public MessageEvent(Long openLocationId) {
        this.openLocationId = openLocationId;
    }

    public Long getOpenLocationId() {
        return openLocationId;
    }
}