package com.majcenovic.fitlog.data.profile;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.LikeResponse;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;

import org.w3c.dom.Comment;

import java.util.List;

/**
 * Created by martina on 08/08/2017.
 */

public interface ProfileListener extends BaseListener {

    interface ProfileCallback extends BaseListener {
        void onProfileLoaded(User user);
    }

    void onPostsLoaded(List<Post> posts);

    interface LikeCallback extends BaseListener {
        void onLikePosted();
    }

    interface DislikeCallback extends BaseListener {
        void onDislikePosted();
    }

    interface FollowCallback extends BaseListener {
        void onFollow();
    }

    interface UnfollowCallback extends BaseListener {
        void onUnfollow();
    }
}