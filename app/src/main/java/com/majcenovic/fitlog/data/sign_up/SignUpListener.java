package com.majcenovic.fitlog.data.sign_up;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.User;

/**
 * Created by martina on 27/06/2017.
 */

public interface SignUpListener extends BaseListener {

    void onSignedUp(User user);
}
