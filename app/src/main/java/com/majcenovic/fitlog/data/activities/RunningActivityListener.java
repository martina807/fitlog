package com.majcenovic.fitlog.data.activities;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Activity;

/**
 * Created by martina on 05/08/2017.
 */

public interface RunningActivityListener {

    interface FinishActivityListener extends BaseListener {
        void onFinishedActivityPosted();
    }

    interface CreateActivityListener extends BaseListener {
        void onActivityCreated(Activity activity);

    }

    interface ActivityLocationListener extends BaseListener {
        void onActivityLocationPosted();
    }
}
