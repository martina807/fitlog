package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by martina on 07/09/2017.
 */

public class PeopleResponse extends BaseModel {

    @SerializedName("following")
    private List<User> following;

    @SerializedName("followers")
    private List<User> followers;

    public List<User> getFollowing() {
        return following;
    }

    public List<User> getFollowers() {
        return followers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.following);
        dest.writeTypedList(this.followers);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public PeopleResponse() {
    }

    protected PeopleResponse(Parcel in) {
        super(in);
        this.following = in.createTypedArrayList(User.CREATOR);
        this.followers = in.createTypedArrayList(User.CREATOR);
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<PeopleResponse> CREATOR = new Creator<PeopleResponse>() {
        @Override
        public PeopleResponse createFromParcel(Parcel source) {
            return new PeopleResponse(source);
        }

        @Override
        public PeopleResponse[] newArray(int size) {
            return new PeopleResponse[size];
        }
    };
}