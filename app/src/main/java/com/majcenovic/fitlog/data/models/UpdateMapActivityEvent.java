package com.majcenovic.fitlog.data.models;

import com.majcenovic.fitlog.data.models.dto.UpdateLocationDto;

/**
 * Created by martina on 10/08/2017.
 */

public class UpdateMapActivityEvent {

    private final float distance;
    private final UpdateLocationDto gpsLocation;

    public UpdateMapActivityEvent(float distance, UpdateLocationDto gpsLocation) {
        this.distance = distance;
        this.gpsLocation = gpsLocation;
    }

    public UpdateLocationDto getGpsLocation() {
        return gpsLocation;
    }

    public float getDistance() {
        return distance;
    }
}