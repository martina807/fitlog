package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 08/09/2017.
 */

public class Comment extends BaseModel {

    @SerializedName("user")
    private User user;

    @SerializedName("likes_count")
    private Long likesCount;

    @SerializedName("dislikes_count")
    private Long dislikesCount;

    @SerializedName("is_liked")
    private boolean isLiked;

    @SerializedName("is_disliked")
    private boolean isDisliked;

    public User getUser() {
        return user;
    }

    public Long getLikesCount() {
        return likesCount;
    }

    public Long getDislikesCount() {
        return dislikesCount;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setLikesCount(Long likesCount) {
        this.likesCount = likesCount;
    }

    public void incrementLikes() {
        likesCount++;
    }

    public void decrementLikes() {
        likesCount--;
    }

    public void setDislikesCount(Long dislikesCount) {
        this.dislikesCount = dislikesCount;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public boolean isDisliked() {
        return isDisliked;
    }

    public void setDisliked(boolean disliked) {
        isDisliked = disliked;
    }

    public Comment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(this.user, flags);
        dest.writeValue(this.likesCount);
        dest.writeValue(this.dislikesCount);
        dest.writeByte(this.isLiked ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isDisliked ? (byte) 1 : (byte) 0);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    protected Comment(Parcel in) {
        super(in);
        this.user = in.readParcelable(User.class.getClassLoader());
        this.likesCount = (Long) in.readValue(Long.class.getClassLoader());
        this.dislikesCount = (Long) in.readValue(Long.class.getClassLoader());
        this.isLiked = in.readByte() != 0;
        this.isDisliked = in.readByte() != 0;
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}
