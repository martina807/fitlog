package com.majcenovic.fitlog.data.models.dto;

import java.io.File;

/**
 * Created by martina on 09/09/2017.
 */

public class UpdateUserDto {

    private String firstName;
    private String lastName;
    private Integer height;
    private String description;
    private String gender;
    private String birthday;
    private File profilePicture;

    public UpdateUserDto(String firstName, String lastName, Integer height, String description, String gender, String birthday, File profilePicture) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
        this.description = description;
        this.gender = gender;
        this.birthday = birthday;
        this.profilePicture = profilePicture;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getHeight() {
        return height;
    }

    public String getDescription() {
        return description;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public File getProfilePicture() {
        return profilePicture;
    }
}