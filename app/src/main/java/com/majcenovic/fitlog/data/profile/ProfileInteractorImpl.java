package com.majcenovic.fitlog.data.profile;

import android.content.Context;

import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.LikeResponse;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.UserFollow;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.data.people.PeopleListener;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/08/2017.
 */

public class ProfileInteractorImpl implements ProfileInteractor {

    private final Context context;

    public ProfileInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void getUserDetails(Long userId, final ProfileListener.ProfileCallback callback) {
        ApiManager.getApiService(context).getUser(userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onProfileLoaded(response.body());
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void getUserBoardPosts(Long userId, final ProfileListener callback) {
        ApiManager.getApiService(context).getBoardPosts(userId).enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    List<Post> posts = response.body();

                    callback.onPostsLoaded(posts);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void like(Post post, final ProfileListener.LikeCallback callback) {
        ApiManager.getApiService(context).likePost(post.getId()).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onLikePosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void dislike(Post post, final ProfileListener.DislikeCallback callback) {
        ApiManager.getApiService(context).dislikePost(post.getId()).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onDislikePosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void follow(User user, final ProfileListener.FollowCallback callback) {
        ApiManager.getApiService(context).follow(user.getId()).enqueue(new Callback<UserFollow>() {
            @Override
            public void onResponse(Call<UserFollow> call, Response<UserFollow> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onFollow();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<UserFollow> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void unfollow(User user, final ProfileListener.UnfollowCallback callback) {
        ApiManager.getApiService(context).unfollow(user.getId()).enqueue(new Callback<UserFollow>() {
            @Override
            public void onResponse(Call<UserFollow> call, Response<UserFollow> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onUnfollow();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<UserFollow> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}
