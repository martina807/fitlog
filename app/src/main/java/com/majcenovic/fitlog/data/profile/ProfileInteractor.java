package com.majcenovic.fitlog.data.profile;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 09/08/2017.
 */

public interface ProfileInteractor extends BaseInteractor {

    void getUserDetails(Long userId, ProfileListener.ProfileCallback callback);

    void getUserBoardPosts(Long userId, ProfileListener callback);

    void like(Post post, ProfileListener.LikeCallback callback);

    void dislike(Post post, ProfileListener.DislikeCallback callback);

    void follow(User user, ProfileListener.FollowCallback callback);

    void unfollow(User user, ProfileListener.UnfollowCallback callback);
}
