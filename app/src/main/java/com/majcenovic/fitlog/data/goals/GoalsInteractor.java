package com.majcenovic.fitlog.data.goals;

import com.majcenovic.fitlog.data.BaseInteractor;

/**
 * Created by martina on 09/09/2017.
 */

public interface GoalsInteractor extends BaseInteractor {

    void getGoals(GoalsListener.GetGoalsCallback callback);
    void updateCurrentWeight(Double currentWeight, GoalsListener.UpdateCurrentWeight callback);
    void updateWeeklyGoal(Double weeklyGoal, GoalsListener.UpdateWeeklyGoal callback);
    void updateFinalGoal(Double finalGoal, GoalsListener.UpdateFinalGoal callback);
    void updateActivityLevel(String activityLevel, GoalsListener.UpdateActivityLevel callback);
}