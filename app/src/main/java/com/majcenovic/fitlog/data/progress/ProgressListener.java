package com.majcenovic.fitlog.data.progress;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.models.Progress;

import java.util.List;

/**
 * Created by martina on 08/08/2017.
 */

public interface ProgressListener extends BaseListener {

    interface ProgressMonthYearListener extends BaseListener {
        void onProgressLoaded(List<Progress> progresses);
    }

    interface UpdateCurrentWeightListener extends BaseListener {
        void onCurrentWeightPosted();
    }

    interface UpdateFinalGoalListener extends BaseListener {
        void onFinalGoalPosted();
    }

    interface UpdateWeeklyGoalListener extends BaseListener {
        void onWeeklyGoalPosted();
    }

    interface UpdateActivityLevelListener extends BaseListener {
        void onActivityLevelPosted();
    }

    interface GoalsListener extends BaseListener {
        void onGoalsLoaded(Goal goal);
    }

    interface PostProgress extends BaseListener {
        void onProgressPosted();
    }
}
