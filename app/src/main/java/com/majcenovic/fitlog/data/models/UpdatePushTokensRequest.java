package com.majcenovic.fitlog.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by martina on 08/08/2017.
 */

public class UpdatePushTokensRequest {

    @SerializedName("onesignal_id")
    private String onesignalId;

    @SerializedName("google_reg_id")
    private String googleRegId;

    @SerializedName("device_id")
    private String deviceId;

    public UpdatePushTokensRequest(String onesignalId, String googleRegId, String deviceId) {
        this.onesignalId = onesignalId;
        this.googleRegId = googleRegId;
        this.deviceId = deviceId;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        if (onesignalId != null) map.put("onesignal_id", onesignalId);
        if (googleRegId != null) map.put("google_reg_id", googleRegId);
        if (deviceId != null) map.put("device_id", deviceId);

        return map;
    }
}