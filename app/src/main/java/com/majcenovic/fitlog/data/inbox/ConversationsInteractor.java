package com.majcenovic.fitlog.data.inbox;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.Conversation;

import java.util.List;

/**
 * Created by martina on 07/08/2017.
 */

public interface ConversationsInteractor extends BaseInteractor {

    void loadConversations(InboxListener.ConversationsListener callback);
}