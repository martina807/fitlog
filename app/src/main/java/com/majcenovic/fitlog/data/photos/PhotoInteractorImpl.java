package com.majcenovic.fitlog.data.photos;

import android.content.Context;

import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/08/2017.
 */

public class PhotoInteractorImpl implements PhotoInteractor {

    private final Context context;

    public PhotoInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void like(Post post, final PhotoListener.LikeCallback callback) {
        ApiManager.getApiService(context).likePost(post.getId()).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onLikePosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void dislike(Post post, final PhotoListener.DislikeCallback callback) {
        ApiManager.getApiService(context).dislikePost(post.getId()).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onDislikePosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void getPost(Long postId, final PhotoListener.PostCallback callback) {
        ApiManager.getApiService(context).getPost(postId).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onPostLoaded(response.body());
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}