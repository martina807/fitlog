package com.majcenovic.fitlog.data.log_in;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.User;

/**
 * Created by martina on 26/06/2017.
 */

public interface LogInListener extends BaseListener {

    void onLoggedIn(User user);
}
