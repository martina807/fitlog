package com.majcenovic.fitlog.data.comments;

import android.content.Context;

import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.Comment;
import com.majcenovic.fitlog.data.models.LikeResponse;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.data.profile.ProfileListener;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 08/09/2017.
 */

public class CommentsInteractorImpl implements CommentsInteractor {

    private boolean isCanceled;
    private final Context context;

    public CommentsInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void loadComments(Post post, final CommentsListener callback) {
        ApiManager.getApiService(context).getCommentList(post.getId()).enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    List<Comment> comments = response.body();
                    callback.onCommentsLoaded(comments);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void postComment(Post post, final String comment, final CommentsListener.PostComment callback) {
        ApiManager.getApiService(context).postComment(post.getId(), comment).enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    Comment comment = response.body();
                    callback.onCommentPosted(comment);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void likeComment(final Comment comment, final CommentsListener.LikeComment callback) {
        ApiManager.getApiService(context).likeComment(comment.getId()).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onCommentLiked();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void dislikeComment(Comment comment, final CommentsListener.DislikeComment callback) {
        ApiManager.getApiService(context).dislikeComment(comment.getId()).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onCommentDisliked();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}
