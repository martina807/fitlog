package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import retrofit2.http.Field;

/**
 * Created by martina on 05/08/2017.
 */

public class FinishedActivity extends BaseModel {

    @SerializedName("duration")
    Integer duration;
    @SerializedName("distance")
    Integer distance;
    @SerializedName("avg_speed")
    Double avgSpeed;
    @SerializedName("top_speed")
    Double topSpeed;

    public FinishedActivity(Integer duration, Integer distance, Double avgSpeed, Double topSpeed) {
        this.duration = duration;
        this.distance = distance;
        this.avgSpeed = avgSpeed;
        this.topSpeed = topSpeed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.duration);
        dest.writeValue(this.distance);
        dest.writeValue(this.avgSpeed);
        dest.writeValue(this.topSpeed);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public FinishedActivity() {
    }

    protected FinishedActivity(Parcel in) {
        super(in);
        this.duration = (Integer) in.readValue(Integer.class.getClassLoader());
        this.distance = (Integer) in.readValue(Integer.class.getClassLoader());
        this.avgSpeed = (Double) in.readValue(Double.class.getClassLoader());
        this.topSpeed = (Double) in.readValue(Double.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<FinishedActivity> CREATOR = new Creator<FinishedActivity>() {
        @Override
        public FinishedActivity createFromParcel(Parcel source) {
            return new FinishedActivity(source);
        }

        @Override
        public FinishedActivity[] newArray(int size) {
            return new FinishedActivity[size];
        }
    };
}
