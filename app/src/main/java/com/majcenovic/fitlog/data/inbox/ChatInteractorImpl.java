package com.majcenovic.fitlog.data.inbox;

import android.content.Context;

import com.majcenovic.fitlog.data.models.Message;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;
import com.majcenovic.fitlog.util.NotificationTypeUtil;
import com.majcenovic.fitlog.util.PushUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 07/08/2017.
 */

public class ChatInteractorImpl implements ChatInteractor {

    private final Context context;
    private boolean isCanceled;

    public ChatInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void getUserById(Long userId, final InboxListener.UserListener callback) {
        ApiManager.getApiService(context).getUser(userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(isCanceled) return;

                if (response.isSuccessful()) {
                    User friend = response.body();
                    callback.onUserLoaded(friend);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void postMessage(Long userId, String message, final InboxListener.CreateChatMessageListener callback) {
        ApiManager.getApiService(context).postMessage(userId, message).enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if(isCanceled) return;

                if(response.isSuccessful()) {
                    Message message = response.body();
                    callback.onMessagePosted(message);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void getMessagesWithUser(Long userId, Long fromMessageId, String type, final InboxListener.GetChatMessagesListener callback) {
        ApiManager.getApiService(context).getMessagesWithUser(userId, fromMessageId, type).enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                if(isCanceled) return;

                if(response.isSuccessful()) {
                    List<Message> messages = response.body();
                    callback.onMessagesReceived(messages);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void chatStarted(Long friendId) {
        PushUtil.setInChat(context, true, String.valueOf(friendId));
        PushUtil.clearPushes(context, NotificationTypeUtil.CHAT, friendId);
    }

    @Override
    public void chatStopped(Long friendId) {
        PushUtil.setInChat(context, false, String.valueOf(friendId));
    }
}