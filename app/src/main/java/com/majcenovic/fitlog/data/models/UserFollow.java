package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 07/09/2017.
 */

public class UserFollow extends BaseModel {

    @SerializedName("user_id")
    private Long userId;

    @SerializedName("followed_user_id")
    private Long followed_user_id;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.userId);
        dest.writeValue(this.followed_user_id);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public UserFollow() {
    }

    protected UserFollow(Parcel in) {
        super(in);
        this.userId = (Long) in.readValue(Long.class.getClassLoader());
        this.followed_user_id = (Long) in.readValue(Long.class.getClassLoader());
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<UserFollow> CREATOR = new Creator<UserFollow>() {
        @Override
        public UserFollow createFromParcel(Parcel source) {
            return new UserFollow(source);
        }

        @Override
        public UserFollow[] newArray(int size) {
            return new UserFollow[size];
        }
    };
}
