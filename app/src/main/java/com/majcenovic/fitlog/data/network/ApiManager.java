package com.majcenovic.fitlog.data.network;

/**
 * Created by martina on 21/06/2017.
 */

import android.content.Context;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.storage.SharedPrefs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Martina on 9.12.2016..
 */

public class ApiManager {

    private static final String API_BASE_URL = "http://104.236.18.28/fitlog/v1/";
    private static final int TIMEOUT = 1000;

    public static ApiService apiService;
    public static Retrofit retrofit;
    public static User currentUser;

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static ApiService getApiService(Context context) {

        if (retrofit == null || currentUser == null) {

            if(currentUser == null) {
                currentUser = SharedPrefs.readUserData(context);
            }

            Interceptor requestInterceptor = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();
                    Request.Builder rb = request.newBuilder();
                    if(currentUser != null) {
                        rb.addHeader("Access-Token", currentUser.getAccessToken());
                        rb.addHeader("User-ID", String.valueOf(currentUser.getId()));
                    }
                    rb.addHeader("Time-Zone", java.util.TimeZone.getDefault().getID());
                    return chain.proceed(rb.build());
                }
            };

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(requestInterceptor)
                    .addInterceptor(logging)
                    .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                    .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                    .build();

            retrofit = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            apiService = retrofit.create(ApiService.class);
        }

        if (apiService == null) {
            apiService = retrofit.create(ApiService.class);
        }

        return apiService;
    }
}

