package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 08/08/2017.
 */

public class PushNotificationModel extends BaseModel {

    @SerializedName("open_location")
    private String openLocation;

    @SerializedName("open_location_id")
    private Long openLocationId;

    public String getOpenLocation() {
        return openLocation;
    }

    public Long getOpenLocationId() {
        return openLocationId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.openLocation);
        dest.writeValue(this.openLocationId);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public PushNotificationModel() {
    }

    protected PushNotificationModel(Parcel in) {
        super(in);
        this.openLocation = in.readString();
        this.openLocationId = (Long) in.readValue(Long.class.getClassLoader());
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<PushNotificationModel> CREATOR = new Creator<PushNotificationModel>() {
        @Override
        public PushNotificationModel createFromParcel(Parcel source) {
            return new PushNotificationModel(source);
        }

        @Override
        public PushNotificationModel[] newArray(int size) {
            return new PushNotificationModel[size];
        }
    };
}