package com.majcenovic.fitlog.data.goals;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Goal;

/**
 * Created by martina on 09/09/2017.
 */

public interface GoalsListener {

    interface GetGoalsCallback extends BaseListener {
        void onGoalsLoaded(Goal goal);
    }

    interface UpdateCurrentWeight extends BaseListener {
        void onCurrentWeightUpdated();
    }

    interface UpdateWeeklyGoal extends BaseListener {
        void onWeeklyGoalUpdated();
    }

    interface UpdateFinalGoal extends BaseListener {
        void onFinalGoalUpdated();
    }

    interface UpdateActivityLevel extends BaseListener {
        void onActivityLevelUpdated();
    }
}
