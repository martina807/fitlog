package com.majcenovic.fitlog.data.photos;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.Post;

/**
 * Created by martina on 09/08/2017.
 */

public interface PhotoInteractor extends BaseInteractor {

    void like(Post post, PhotoListener.LikeCallback callback);

    void dislike(Post post, PhotoListener.DislikeCallback callback);

    void getPost(Long postId, PhotoListener.PostCallback callback);
}
