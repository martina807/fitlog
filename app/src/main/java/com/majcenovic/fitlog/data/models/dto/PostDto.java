package com.majcenovic.fitlog.data.models.dto;

import java.io.File;
import java.util.List;

/**
 * Created by martina on 03/09/2017.
 */

public class PostDto {

    private Long friendId;
    private String type;
    private String message;
    private List<File> photos;
    private String progressPeriodType;
    private Long progressStartWeightId;
    private Long progressEndWeightId;
    private List<Long> tagIds;
    private Long activityId;

    public PostDto(Long friendId, String message, List<File> photos, List<Long> tagIds) {
        this.friendId = friendId;
        this.message = message;
        this.photos = photos;
        this.tagIds = tagIds;
        generateType();
    }

    private void generateType() {
        if(progressPeriodType != null && progressStartWeightId != null && progressEndWeightId != null) {
            type = "progress";
        } else if (activityId != null) {
            type = "activity";
        } else if (photos != null && !photos.isEmpty()) {
            type = "photo";
        } else if (message != null && !message.trim().isEmpty()) {
            type = "message";
        } else {
            type = null;
        }
    }

    public PostDto(Long activityId) {
        this.activityId = activityId;
        generateType();
    }

    public PostDto(String progressPeriodType, Long progressStartWeightId, Long progressEndWeightId) {
        this.progressPeriodType = progressPeriodType;
        this.progressStartWeightId = progressStartWeightId;
        this.progressEndWeightId = progressEndWeightId;
        generateType();
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public List<File> getPhotos() {
        return photos;
    }

    public String getProgressPeriodType() {
        return progressPeriodType;
    }

    public Long getProgressStartWeightId() {
        return progressStartWeightId;
    }

    public Long getProgressEndWeightId() {
        return progressEndWeightId;
    }

    public List<Long> getTagIds() {
        return tagIds;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPhotos(List<File> photos) {
        this.photos = photos;
    }

    public void setProgressPeriodType(String progressPeriodType) {
        this.progressPeriodType = progressPeriodType;
    }

    public void setProgressStartWeightId(Long progressStartWeightId) {
        this.progressStartWeightId = progressStartWeightId;
    }

    public void setProgressEndWeightId(Long progressEndWeightId) {
        this.progressEndWeightId = progressEndWeightId;
    }

    public void setTagIds(List<Long> tagIds) {
        this.tagIds = tagIds;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getFriendId() {
        return friendId;
    }

    public boolean isValid() {
        return type != null;
    }
}
