package com.majcenovic.fitlog.data.log_in;

import com.majcenovic.fitlog.data.BaseInteractor;

/**
 * Created by martina on 26/06/2017.
 */

public interface LogInInteractor extends BaseInteractor {

    void loginUser(String email, String password, LogInListener callback);
}
