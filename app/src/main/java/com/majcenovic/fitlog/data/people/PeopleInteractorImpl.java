package com.majcenovic.fitlog.data.people;

import android.content.Context;

import com.majcenovic.fitlog.data.models.PeopleResponse;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.UserFollow;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/08/2017.
 */

public class PeopleInteractorImpl implements PeopleInteractor {

    private final Context context;

    public PeopleInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void getPeople(Long userId, final PeopleListener callback) {
        ApiManager.getApiService(context).getUserFriends(userId).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    List<User> people = response.body();
                    if (people == null) {
                        people = new ArrayList<>();
                    }
                    callback.onPeopleLoaded(people);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                if (isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void follow(Long userId, final PeopleListener.FollowCallback callback) {
        ApiManager.getApiService(context).follow(userId).enqueue(new Callback<UserFollow>() {
            @Override
            public void onResponse(Call<UserFollow> call, Response<UserFollow> response) {
                if(isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onFollow();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<UserFollow> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void unfollow(Long userId, final PeopleListener.UnfollowCallback callback) {
        ApiManager.getApiService(context).unfollow(userId).enqueue(new Callback<UserFollow>() {
            @Override
            public void onResponse(Call<UserFollow> call, Response<UserFollow> response) {
                if(isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onUnfollow();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<UserFollow> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}