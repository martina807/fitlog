package com.majcenovic.fitlog.data.home;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.Post;

/**
 * Created by martina on 28/06/2017.
 */

public interface HomeInteractor extends BaseInteractor {

    void getNews(HomeListener.PostsListener callback);

    void getCaloriesStatus(HomeListener.CalorieStatusCallback callback);

    void like(Post post, HomeListener.LikeCallback callback);

    void dislike(Post post, HomeListener.DislikeCallback callback);
}