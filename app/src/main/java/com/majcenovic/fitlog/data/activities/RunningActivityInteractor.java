package com.majcenovic.fitlog.data.activities;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.GpsLocation;

/**
 * Created by martina on 05/08/2017.
 */

public interface RunningActivityInteractor extends BaseInteractor {
    void postFinishedActivity(Long activityType,
                              Integer distance,
                              Integer duration,
                              Double avgSpeed,
                              Double topSpeed,
                              RunningActivityListener.FinishActivityListener callback);

    void createActivity(String activityType, RunningActivityListener.CreateActivityListener callback);

}
