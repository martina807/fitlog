package com.majcenovic.fitlog.data.inbox;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.Conversation;
import com.majcenovic.fitlog.data.models.Message;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 07/08/2017.
 */

public interface InboxListener {

    interface ConversationsListener extends BaseListener {
        void onConversationsLoaded(List<Conversation> conversations);
    }

    interface UserListener extends BaseListener {
        void onUserLoaded(User friend);
    }

    interface CreateChatMessageListener extends BaseListener {
        void onMessagePosted(Message message);
    }

    interface GetChatMessagesListener extends BaseListener {
        void onMessagesReceived(List<Message> messages);
    }
}