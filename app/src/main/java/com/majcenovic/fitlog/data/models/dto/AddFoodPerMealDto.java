package com.majcenovic.fitlog.data.models.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 05/07/2017.
 */

public class AddFoodPerMealDto implements Parcelable {

    private Integer amount;
    private Integer mealType;
    private Long foodId;
    private String eatenAt;

    public AddFoodPerMealDto(Integer amount, Integer mealType, Long foodId, String eatenAt) {
        this.amount = amount;
        this.mealType = mealType;
        this.foodId = foodId;
        this.eatenAt = eatenAt;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.amount);
        dest.writeValue(this.mealType);
        dest.writeValue(this.foodId);
        dest.writeString(this.eatenAt);
    }

    protected AddFoodPerMealDto(Parcel in) {
        this.amount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mealType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.foodId = (Long) in.readValue(Long.class.getClassLoader());
        this.eatenAt = in.readString();
    }

    public static final Parcelable.Creator<AddFoodPerMealDto> CREATOR = new Parcelable.Creator<AddFoodPerMealDto>() {
        @Override
        public AddFoodPerMealDto createFromParcel(Parcel source) {
            return new AddFoodPerMealDto(source);
        }

        @Override
        public AddFoodPerMealDto[] newArray(int size) {
            return new AddFoodPerMealDto[size];
        }
    };

    public Integer getAmount() {
        return amount;
    }

    public Integer getMealType() {
        return mealType;
    }

    public Long getFoodId() {
        return foodId;
    }

    public String getEatenAt() {
        return eatenAt;
    }
}
