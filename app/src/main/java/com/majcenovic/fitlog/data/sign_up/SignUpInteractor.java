package com.majcenovic.fitlog.data.sign_up;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;

/**
 * Created by martina on 27/06/2017.
 */

public interface SignUpInteractor extends BaseInteractor {

    void signUpUser(SignUpDto signUpTransferObject, SignUpListener callback);
}
