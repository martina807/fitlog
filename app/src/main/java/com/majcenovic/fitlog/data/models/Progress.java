package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 08/08/2017.
 */

public class Progress extends BaseModel {

    @SerializedName("weight")
    private Double weight;

    public Double getWeight() {
        return weight;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.weight);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public Progress() {
    }

    protected Progress(Parcel in) {
        super(in);
        this.weight = (Double) in.readValue(Double.class.getClassLoader());
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<Progress> CREATOR = new Creator<Progress>() {
        @Override
        public Progress createFromParcel(Parcel source) {
            return new Progress(source);
        }

        @Override
        public Progress[] newArray(int size) {
            return new Progress[size];
        }
    };
}
