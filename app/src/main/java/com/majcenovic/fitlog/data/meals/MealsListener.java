package com.majcenovic.fitlog.data.meals;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Food;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.data.models.UserFood;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface MealsListener {

    interface GetMealsCallback extends BaseListener {

        void onMealsLoaded(List<Meal> meals);
    }

    interface GetSingleMealFoodCallback extends BaseListener {

        void onSingleMealFoodLoaded(List<UserFood> userFood);
    }

    interface PostSingleFoodPerMealCallback extends BaseListener {
       void onSingleFoodPerMealAdded();
    }

    interface GetAllFoodCallback extends BaseListener {
        void onAllFoodLoaded(List<Food> allFood);
    }

    interface GetCaloriesStatusCallback extends BaseListener {
        void onCaloriesStatusLoaded(CaloriesStatus calorieStatus);
    }

    interface DeleteSingleFoodStatusCallback extends BaseListener {
        void onSingleFoodDeleted();
    }
}
