package com.majcenovic.fitlog.data.activities;

import android.content.Context;

import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.FinishedActivity;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.dto.UpdateLocationDto;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 05/08/2017.
 */

public class RunningActivityInteractorImpl implements RunningActivityInteractor {

    private final Context context;

    public RunningActivityInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void postFinishedActivity(Long activityId,
                                     Integer distance,
                                     Integer duration,
                                     Double avgSpeed,
                                     Double topSpeed, final RunningActivityListener.FinishActivityListener callback) {
        ApiManager.getApiService(context).postFinishedActivity(activityId, duration, distance, avgSpeed, topSpeed)
                .enqueue(new Callback<FinishedActivity>() {
                    @Override
                    public void onResponse(Call<FinishedActivity> call, Response<FinishedActivity> response) {
                        if (isCanceled) return;

                        if (response.isSuccessful()) {
                            callback.onFinishedActivityPosted();
                        } else {
                            callback.onFailure(ApiErrorUtil.getMessage(context, response));
                        }
                    }

                    @Override
                    public void onFailure(Call<FinishedActivity> call, Throwable t) {
                        callback.onFailure(ApiErrorUtil.getMessage(context, t));
                    }
                });
    }

    @Override
    public void createActivity(String activityType, final RunningActivityListener.CreateActivityListener callback) {
        ApiManager.getApiService(context).createActivity(activityType).enqueue(new Callback<Activity>() {
            @Override
            public void onResponse(Call<Activity> call, Response<Activity> response) {
                if (isCanceled) return;

                if (response.isSuccessful()) {
                    Activity activity = response.body();
                    callback.onActivityCreated(activity);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<Activity> call, Throwable t) {
                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}