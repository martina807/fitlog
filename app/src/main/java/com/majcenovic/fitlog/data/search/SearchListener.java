package com.majcenovic.fitlog.data.search;

import com.majcenovic.fitlog.data.BaseListener;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 09/09/2017.
 */

public interface SearchListener extends BaseListener {

    interface PeopleCallback extends BaseListener {
        void onPeopleLoaded(List<User> users);
    }

    interface FollowCallback extends BaseListener {
        void onFollow();
    }

    interface UnfollowCallback extends BaseListener {
        void onUnfollow();
    }
}
