package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.Streams;

import java.util.List;

/**
 * Created by martina on 15/08/2017.
 */

public class Photo extends BaseModel {

    @SerializedName("photo_url")
    private String photoUrl;

    private Post post;
    private String description;
    private List<Tag> tags;
    private long commentCount;
    private long likesCount;
    private boolean isLiked;

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public long getLikesCount() {
        return likesCount;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Photo(Post post, String photoUrl, String description, List<Tag> tags, long commentCount, long likesCount, boolean isLiked) {
        this.post = post;
        this.photoUrl = photoUrl;
        this.description = description;
        this.tags = tags;
        this.commentCount = commentCount;
        this.likesCount = likesCount;
        this.isLiked = isLiked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.photoUrl);
        dest.writeParcelable(this.post, flags);
        dest.writeString(this.description);
        dest.writeTypedList(this.tags);
        dest.writeLong(this.commentCount);
        dest.writeLong(this.likesCount);
        dest.writeByte(this.isLiked ? (byte) 1 : (byte) 0);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    protected Photo(Parcel in) {
        super(in);
        this.photoUrl = in.readString();
        this.post = in.readParcelable(Post.class.getClassLoader());
        this.description = in.readString();
        this.tags = in.createTypedArrayList(Tag.CREATOR);
        this.commentCount = in.readLong();
        this.likesCount = in.readLong();
        this.isLiked = in.readByte() != 0;
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
