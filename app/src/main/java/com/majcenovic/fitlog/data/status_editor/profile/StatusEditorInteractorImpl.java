package com.majcenovic.fitlog.data.status_editor.profile;

import android.content.Context;

import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.PostDto;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.util.ApiErrorUtil;
import com.majcenovic.fitlog.util.RequestUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 09/08/2017.
 */

public class StatusEditorInteractorImpl implements StatusEditorInteractor {
    private final Context context;

    public StatusEditorInteractorImpl(Context context) {
        this.context = context;
    }

    private boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void postStatus(PostDto postDto, final StatusListener.StatusEditorListener callback) {
        ApiManager.getApiService(context).createPost(RequestUtil.mapOf(postDto)).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if(isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onStatusPosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void postPostToFriend(PostDto status, final StatusListener.StatusEditorListener callback) {
        ApiManager.getApiService(context).postPostToSomeUser(status.getFriendId(), RequestUtil.mapOf(status)).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if(isCanceled) return;

                if (response.isSuccessful()) {
                    callback.onStatusPosted();
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }

    @Override
    public void getAllUsersByKeyword(String keyword, final StatusListener.AllUsersListener callback) {
        ApiManager.getApiService(context).getAllUsersByKeyword(keyword).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if(isCanceled) return;

                if(response.isSuccessful()) {
                    List<User> users = response.body();
                    callback.onAllUsersLoaded(users);
                } else {
                    callback.onFailure(ApiErrorUtil.getMessage(context, response));
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                if(isCanceled) return;

                callback.onFailure(ApiErrorUtil.getMessage(context, t));
            }
        });
    }
}
