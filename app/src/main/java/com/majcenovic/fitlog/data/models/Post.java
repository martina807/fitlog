package com.majcenovic.fitlog.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.common.api.BooleanResult;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public class Post extends BaseModel {

    @SerializedName("type")
    private String type;

    @SerializedName("title")
    private String title;

    @SerializedName("sender")
    private User sender;

    @SerializedName("receiver")
    private User receiver;

    @SerializedName("activity")
    private Activity activity;

    @SerializedName("progress")
    private List<Progress> progress;

    @SerializedName("photos")
    private List<Photo> photos;

    @SerializedName("tags")
    private List<Tag> tags;

    @SerializedName("comments_count")
    private Integer commentCount;

    @SerializedName("likes_count")
    private Integer likesCount;

    @SerializedName("dislikes_count")
    private Integer dislikesCount;

    @SerializedName("is_liked")
    private boolean isLiked;

    @SerializedName("is_disliked")
    private boolean isDisliked;

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public Activity getActivity() {
        return activity;
    }

    public List<Progress> getProgress() {
        return progress;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public Integer getDislikesCount() {
        return dislikesCount;
    }

    public boolean isDisliked() {
        return isDisliked;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public boolean getDisliked() {
        return isDisliked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public void incrementLikes() {
        likesCount++;
    }

    public void decrementLikes() {
        likesCount--;
    }

    public Post(String title, List<Tag> tags, Integer commentCount, Integer likesCount, Boolean isLiked) {
        this.title = title;
        this.tags = tags;
        this.commentCount = commentCount;
        this.likesCount = likesCount;
        this.isLiked = isLiked;
    }

    public Post() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.type);
        dest.writeString(this.title);
        dest.writeParcelable(this.sender, flags);
        dest.writeParcelable(this.receiver, flags);
        dest.writeParcelable(this.activity, flags);
        dest.writeTypedList(this.progress);
        dest.writeTypedList(this.photos);
        dest.writeTypedList(this.tags);
        dest.writeValue(this.commentCount);
        dest.writeValue(this.likesCount);
        dest.writeValue(this.dislikesCount);
        dest.writeByte(this.isLiked ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isDisliked ? (byte) 1 : (byte) 0);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    protected Post(Parcel in) {
        super(in);
        this.type = in.readString();
        this.title = in.readString();
        this.sender = in.readParcelable(User.class.getClassLoader());
        this.receiver = in.readParcelable(User.class.getClassLoader());
        this.activity = in.readParcelable(Activity.class.getClassLoader());
        this.progress = in.createTypedArrayList(Progress.CREATOR);
        this.photos = in.createTypedArrayList(Photo.CREATOR);
        this.tags = in.createTypedArrayList(Tag.CREATOR);
        this.commentCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.likesCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.dislikesCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isLiked = in.readByte() != 0;
        this.isDisliked = in.readByte() != 0;
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };
}