package com.majcenovic.fitlog.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 07/09/2017.
 */

public class CaloriesStatus {

    @SerializedName("calories_remaining")
    Double caloriesRemaining;

    @SerializedName("calories_eaten")
    Double caloriesEaten;

    public Double getCaloriesRemaining() {
        return caloriesRemaining;
    }

    public Double getCaloriesEaten() {
        return caloriesEaten;
    }
}
