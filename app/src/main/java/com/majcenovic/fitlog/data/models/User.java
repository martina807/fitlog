package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 11/01/2017.
 */
public class User extends BaseModel {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("email")
    private String email;

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("height")
    private Integer height;

    @SerializedName("gender")
    private String gender;

    @SerializedName("profile_picture")
    private String profilePicture;

    @SerializedName("description")
    private String description;

    @SerializedName("is_following")
    private boolean isFollowing;

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String chatTitle;

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setFollowing(boolean following) {
        isFollowing = following;
    }

    public String getDescription() {
        return description;
    }

    public String getChatTitle() {
        return chatTitle;
    }

    public User(Long id, String chatTitle, String profilePicture) {
        this.id = id;
        this.chatTitle = chatTitle;
        this.profilePicture = profilePicture;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthday() {
        return birthday;
    }

    public Integer getHeight() {
        return height;
    }

    public String getGender() {
        return gender;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public User() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return getId() != null ? getId().equals(user.getId()) : user.getId() == null;

    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.accessToken);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.email);
        dest.writeString(this.birthday);
        dest.writeValue(this.height);
        dest.writeString(this.gender);
        dest.writeString(this.profilePicture);
        dest.writeString(this.description);
        dest.writeByte(this.isFollowing ? (byte) 1 : (byte) 0);
        dest.writeString(this.chatTitle);
        dest.writeValue(this.listType);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    protected User(Parcel in) {
        super(in);
        this.accessToken = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.email = in.readString();
        this.birthday = in.readString();
        this.height = (Integer) in.readValue(Integer.class.getClassLoader());
        this.gender = in.readString();
        this.profilePicture = in.readString();
        this.description = in.readString();
        this.isFollowing = in.readByte() != 0;
        this.chatTitle = in.readString();
        this.listType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
