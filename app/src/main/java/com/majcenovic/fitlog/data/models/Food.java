package com.majcenovic.fitlog.data.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by martina on 18/03/2017.
 */

public class Food extends BaseModel {

    @SerializedName("title")
    private String title;

    @SerializedName("calories_per_100g")
    private Double caloriesPer100g;

    @SerializedName("amount")
    private int amount;

    public String getTitle() {
        return title;
    }

    public Double getCaloriesPer100g() {
        return caloriesPer100g;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.title);
        dest.writeValue(this.caloriesPer100g);
        dest.writeInt(this.amount);
        dest.writeValue(this.id);
        dest.writeString(this.createdAt);
        dest.writeString(this.updateAt);
        dest.writeString(this.message);
    }

    public Food() {
    }

    protected Food(Parcel in) {
        super(in);
        this.title = in.readString();
        this.caloriesPer100g = (Double) in.readValue(Double.class.getClassLoader());
        this.amount = in.readInt();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.createdAt = in.readString();
        this.updateAt = in.readString();
        this.message = in.readString();
    }

    public static final Creator<Food> CREATOR = new Creator<Food>() {
        @Override
        public Food createFromParcel(Parcel source) {
            return new Food(source);
        }

        @Override
        public Food[] newArray(int size) {
            return new Food[size];
        }
    };
}
