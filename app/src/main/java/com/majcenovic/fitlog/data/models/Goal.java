package com.majcenovic.fitlog.data.models;

import com.google.gson.annotations.SerializedName;
import com.majcenovic.fitlog.ui.goals.GoalTypes;

/**
 * Created by martina on 09/08/2017.
 */

public class Goal extends BaseModel {

    @SerializedName("current_weight")
    private Double currentWeight;

    @SerializedName("weekly_goal")
    private Double weeklyGoal;

    @SerializedName("final_goal")
    private Double finalGoal;

    @SerializedName("activity_level")
    private String activityLevel;

    public Goal(Double currentWeight, Double weeklyGoal, Double finalGoal, String activityLevel) {
        this.currentWeight = currentWeight;
        this.weeklyGoal = weeklyGoal;
        this.finalGoal = finalGoal;
        this.activityLevel = activityLevel;
    }

    public String getValueForType(int weightType) {
        switch (weightType) {
            case GoalTypes.CURRENT_WEIGHT:
                return currentWeight != null ? String.valueOf(currentWeight) : "";
            case GoalTypes.WEEKLY_GOAL:
                return weeklyGoal != null ? String.valueOf(weeklyGoal) : "";
            case GoalTypes.FINAL_GOAL:
                return finalGoal != null ? String.valueOf(finalGoal) : "";
            case GoalTypes.ACTIVITY_LEVEL:
                return activityLevel != null ? activityLevel : "";
        }
        return "";
    }

    public Double getCurrentWeight() {
        return currentWeight;
    }

    public Double getWeeklyGoal() {
        return weeklyGoal;
    }

    public Double getFinalGoal() {
        return finalGoal;
    }

    public String getActivityLevel() {
        return activityLevel;
    }
}
