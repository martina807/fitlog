package com.majcenovic.fitlog.data.meals;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.home.HomeListener;
import com.majcenovic.fitlog.data.models.dto.AddFoodPerMealDto;

/**
 * Created by martina on 28/06/2017.
 */

public interface MealsInteractor extends BaseInteractor {

    void getMeals(int dayOffset, MealsListener.GetMealsCallback callback);

    void getCaloriesStatus(HomeListener.CalorieStatusCallback callback);

    void getSingleMealFoods(int dayOffset, int mealType, MealsListener.GetSingleMealFoodCallback callback);

    void getAllFood(String keyword, MealsListener.GetAllFoodCallback callback);

    void postSingleFoodPerMeal(AddFoodPerMealDto addFoodPerMealDto, MealsListener.PostSingleFoodPerMealCallback callback);

    void deleteUserFood(Long id, MealsListener.DeleteSingleFoodStatusCallback callback);
}