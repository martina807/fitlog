package com.majcenovic.fitlog.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by martina on 18/03/2017.
 */

public class Meal extends BaseModel {

    @SerializedName("title")
    private String title;

    @SerializedName("meal_type")
    private Integer mealType;

    @SerializedName("calories")
    private Double calories;

    @SerializedName("user_foods")
    private List<UserFood> userFoods;

    public String getTitle() { return title; }

    public Integer getMealType() { return mealType; }

    public Double getCalories() { return calories; }

    public List<UserFood> getUserFoods() { return userFoods; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.title);
        dest.writeValue(this.mealType);
        dest.writeValue(this.calories);
        dest.writeTypedList(this.userFoods);
    }

    public Meal() {
    }

    protected Meal(Parcel in) {
        super(in);
        this.title = in.readString();
        this.mealType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.calories = (Double) in.readValue(Double.class.getClassLoader());
        this.userFoods = in.createTypedArrayList(UserFood.CREATOR);
    }

    public static final Parcelable.Creator<Meal> CREATOR = new Parcelable.Creator<Meal>() {
        @Override
        public Meal createFromParcel(Parcel source) {
            return new Meal(source);
        }

        @Override
        public Meal[] newArray(int size) {
            return new Meal[size];
        }
    };
}
