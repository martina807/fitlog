package com.majcenovic.fitlog.data.status_editor.profile;

import com.majcenovic.fitlog.data.BaseInteractor;
import com.majcenovic.fitlog.data.models.dto.PostDto;

/**
 * Created by martina on 09/08/2017.
 */

public interface StatusEditorInteractor extends BaseInteractor {

    void postStatus(PostDto status, StatusListener.StatusEditorListener callback);
    void postPostToFriend(PostDto status, StatusListener.StatusEditorListener callback);
    void getAllUsersByKeyword(String keyword, StatusListener.AllUsersListener callback);

}
