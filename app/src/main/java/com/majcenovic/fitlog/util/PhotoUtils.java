package com.majcenovic.fitlog.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.widget.ImageView;

import com.majcenovic.fitlog.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by vedran on 29/04/2017.
 */

public class PhotoUtils {

    public static void loadPhoto(Context context, String url, ImageView imageView) {
        if (context != null && url != null && imageView != null)
            Picasso.with(context).load(url).placeholder(R.drawable.placeholder_image)
                    .error(R.drawable.placeholder_image).into(imageView);
        else
            Picasso.with(context).load(R.drawable.placeholder_image).into(imageView);
    }

    public static void loadProfilePhoto(Context context, String url, ImageView imageView) {
        if (context != null && url != null && imageView != null)
            Picasso.with(context).load(url).placeholder(R.drawable.placeholder_user)
                    .error(R.drawable.placeholder_user).into(imageView);
        else
            Picasso.with(context).load(R.drawable.placeholder_user).into(imageView);
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    public static Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}