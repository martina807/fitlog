package com.majcenovic.fitlog.util;

import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import com.majcenovic.fitlog.storage.SharedPrefs;

/**
 * Created by martina on 08/08/2017.
 */

public class PushUtil {

    private static final String KEY_USER_CHAT = "keyChatWithUser";

    public static void setInChat(Context context, boolean isInChat, String openLocationId) {
        if (isInChat) {
            SharedPrefs.saveBoolean(context, KEY_USER_CHAT + openLocationId, isInChat);
        } else {
            SharedPrefs.remove(context, KEY_USER_CHAT + openLocationId);
        }
    }

    public static boolean isInChat(Context context, String openLocationId) {
        return SharedPrefs.readBoolean(context, KEY_USER_CHAT + openLocationId);
    }

    public static void clearPushes(Context context, String openLocation, Long openLocationId) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Log.d("MYPUSH Clear: ", openLocation + ":" + openLocationId + " - " + String.valueOf(openLocation.hashCode()
                + openLocationId.hashCode()));

        notificationManager.cancel(openLocation.hashCode() + openLocationId.hashCode());
    }

    public static void clearAllPushes(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static void clearPushWithId(Context context, int id) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);
    }
}