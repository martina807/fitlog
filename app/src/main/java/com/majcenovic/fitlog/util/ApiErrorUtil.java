package com.majcenovic.fitlog.util;

import android.content.Context;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.network.ApiManager;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by martina on 26/06/2017.
 */

public class ApiErrorUtil {

    public static String getMessage(Context context, Throwable throwable) {
        String unknownError = context.getString(R.string.unknown_failure);
        String networkError = context.getString(R.string.network_failure);
        if (throwable != null && throwable instanceof IOException) return networkError;
        return unknownError;
    }

    public static <T> String getMessage(Context context, Response<T> response) {
        String errorMessage = context.getString(R.string.unknown_failure);

        if (response != null && response.errorBody() != null) {
            Converter<ResponseBody, BaseModel> errorConverter = ApiManager
                    .getRetrofit()
                    .responseBodyConverter(BaseModel.class, new Annotation[0]);
            BaseModel error = null;
            try {
                error = errorConverter.convert(response.errorBody());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (error != null && error.getMessage() != null) {
                errorMessage = error.getMessage();
            } else if (response.message() != null) {
                errorMessage = response.code() + " - " + response.message();
            }
        }
        return errorMessage;
    }
}
