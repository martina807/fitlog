package com.majcenovic.fitlog.util;

import com.majcenovic.fitlog.data.models.dto.PostDto;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.data.models.dto.UpdateUserDto;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by martina on 27/06/2017.
 */

public class RequestUtil {

    private static RequestBody toRequestBodyText(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    private static RequestBody toRequestBodyText(Integer value) {
        return toRequestBodyText(String.valueOf(value));
    }

    private static RequestBody toRequestBodyText(Long value) {
        return toRequestBodyText(String.valueOf(value));
    }

    private static RequestBody toRequestBodyText(Double value) {
        return toRequestBodyText(String.valueOf(value));
    }

    private static RequestBody toRequestBodyImage(File file) {
        return RequestBody.create(MediaType.parse("image/*"), file);
    }

    private static String toRequestKeyImage(File file, String key) {
        return key + "\"; filename=\"" + file.getName() + ".png\"";
    }

    public static Map<String, RequestBody> mapOf(SignUpDto signUpTransferObject) {

        Map<String, RequestBody> map = new HashMap<>();
        map.put("first_name", toRequestBodyText(signUpTransferObject.getFirstName()));
        map.put("last_name", toRequestBodyText(signUpTransferObject.getLastName()));
        map.put("password", toRequestBodyText(signUpTransferObject.getPassword()));
        map.put("email", toRequestBodyText(signUpTransferObject.getEmail()));
        map.put("gender", toRequestBodyText(signUpTransferObject.getGender()));
        map.put("birthday", toRequestBodyText(signUpTransferObject.getBirthday()));
        map.put("height", toRequestBodyText(signUpTransferObject.getHeight()));
        map.put("current_weight", toRequestBodyText(signUpTransferObject.getCurrentWeight()));
        map.put("activity_level", toRequestBodyText(signUpTransferObject.getActivityLevel()));
        map.put("weekly_goal", toRequestBodyText(signUpTransferObject.getWeeklyGoalWeight()));
        map.put("goal_weight", toRequestBodyText(signUpTransferObject.getGoalWeight()));
        if (signUpTransferObject.getImage() != null) {
            map.put(toRequestKeyImage(signUpTransferObject.getImage(), "profile_picture"),
                    toRequestBodyImage(signUpTransferObject.getImage()));
        }

        return map;
    }

    public static Map<String, RequestBody> mapOf(PostDto postDto) {

        Map<String, RequestBody> map = new HashMap<>();
        switch (postDto.getType()) {
            case "progress":
                map.put("progress_weight_start_id", toRequestBodyText(postDto.getProgressStartWeightId()));
                map.put("progress_weight_end_id", toRequestBodyText(postDto.getProgressEndWeightId()));
                map.put("progress_period_type", toRequestBodyText(postDto.getProgressPeriodType()));
                break;
            case "activity":
                map.put("activity_id", toRequestBodyText(postDto.getActivityId()));
                break;
            case "photo":
                for (int i = 0; i < postDto.getPhotos().size(); i++) {
                    map.put(toRequestKeyImage(postDto.getPhotos().get(i), "photos[" + i + "]"),
                            toRequestBodyImage(postDto.getPhotos().get(i)));
                }
                break;
        }

        if (postDto.getMessage() != null && !postDto.getMessage().trim().isEmpty()) {
            map.put("message", toRequestBodyText(postDto.getMessage().trim()));
        }

        if (postDto.getTagIds() != null && !postDto.getTagIds().isEmpty()) {
            for (int i = 0; i < postDto.getTagIds().size(); i++) {
                map.put("tags[" + i + "]", toRequestBodyText(postDto.getTagIds().get(i)));
            }
        }

        if (postDto.getType() != null) {
            map.put("type", toRequestBodyText(postDto.getType()));
        }

        return map;
    }

    public static Map<String, RequestBody> mapOf(UpdateUserDto updateUserDto) {

        Map<String, RequestBody> map = new HashMap<>();

        if (updateUserDto.getFirstName() != null && !updateUserDto.getFirstName().trim().isEmpty()) {
            map.put("first_name", toRequestBodyText(updateUserDto.getFirstName()));
        }

        if (updateUserDto.getLastName() != null && !updateUserDto.getLastName().trim().isEmpty()) {
            map.put("last_name", toRequestBodyText(updateUserDto.getLastName()));
        }

        if (updateUserDto.getHeight() != null) {
            map.put("height", toRequestBodyText(updateUserDto.getHeight()));
        }

        if (updateUserDto.getDescription() != null) {
            map.put("description", toRequestBodyText(updateUserDto.getDescription()));
        }

        if (updateUserDto.getGender() != null) {
            map.put("gender", toRequestBodyText(updateUserDto.getGender()));
        }

        if (updateUserDto.getBirthday() != null && !updateUserDto.getBirthday().trim().isEmpty()) {
            map.put("birthday", toRequestBodyText(updateUserDto.getBirthday()));
        }

        if (updateUserDto.getProfilePicture() != null) {
            map.put(toRequestKeyImage(updateUserDto.getProfilePicture(), "profile_picture"),
                    toRequestBodyImage(updateUserDto.getProfilePicture()));
        }

        return map;
    }
}