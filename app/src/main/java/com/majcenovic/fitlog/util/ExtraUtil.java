package com.majcenovic.fitlog.util;

/**
 * Created by martina on 27/06/2017.
 */

public class ExtraUtil {

    public static final String MEAL_TYPE = "MEAL_TYPE";
    public static final String PROGRESS_VIEW_TYPE = "PROGRESS_VIEW_TYPE";
    public static final String PROGRESS_TYPE_MONTH = "month";
    public static final String PROGRESS_TYPE_YEAR = "year";
    public static final String PHOTO_POSTS = "PHOTO_POSTS";
    public static final String PHOTO = "PHOTO";
    public static final String DAY_OFFSET = "DAY_OFFSET";
    public static final String POST = "POST";
    public static final String GOAL = "GOAL";
    public static final String WEIGHT_TYPE = "WEIGHT_TYPE";
    public static String SIGN_UP_DTO = "SIGN_UP_DTO";
    public static String ADD_FOOD_PER_MEAL_DTO = "SIGN_UP_SECOND_DTO";
    public static String ADD_FOOD_MEAL = "ADD_FOOD_MEAL";
    public static String ACTIVITY_TYPE = "ACTIVITY_TYPE";
    public static final String OBJECT = "extraObject";
    public static final String USER = "USER";
    public static final String SHOW_FOOD_PER_MEAl = "SHOW_FOOD_PER_MEAl";
}
