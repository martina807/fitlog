package com.majcenovic.fitlog.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Progress;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martina on 08/08/2017.
 */

public class ChartUtil {

    public static void setChartProgress(LineChart chartProgress, final List<Progress> progresses, Context context) {

        if(progresses == null || progresses.isEmpty() || progresses.size() < 2)  return;

        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return DateUtil.toNiceMediumDateNoYearFormat(progresses.get((int) value).getCreatedAt());
            }
        };

        List<Entry> entries = new ArrayList<>();

        for (int i = 0; i < progresses.size(); i++) {
            Progress data = progresses.get(i);
            entries.add(new Entry(i, data.getWeight().floatValue()));
        }

        float axisWidht = 1.5f;
        float lineWidth = 3.0f;
        float circleRadius = 5.0f;

        XAxis xAxis = chartProgress.getXAxis();
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisLineColor(ContextCompat.getColor(context, R.color.accent));
        xAxis.setAxisLineWidth(axisWidht);
        LineDataSet dataSet = new LineDataSet(entries, null);
        LineData data = new LineData(dataSet);
        dataSet.setCircleColor(ContextCompat.getColor(context, R.color.accent));
        dataSet.setCircleColorHole(ContextCompat.getColor(context, R.color.accent));
        dataSet.setColor(ContextCompat.getColor(context, R.color.accent));
        dataSet.setLineWidth(lineWidth);
        dataSet.setCircleHoleRadius(circleRadius);
        dataSet.setCircleRadius(circleRadius);
        dataSet.setValueTextSize(12.0f);
        chartProgress.setData(data);
        chartProgress.getLegend().setEnabled(false);
        chartProgress.setDrawGridBackground(false);
        chartProgress.getAxisLeft().setDrawGridLines(false);
        chartProgress.getAxisRight().setDrawGridLines(false);
        chartProgress.getXAxis().setDrawGridLines(false);
        chartProgress.getAxisRight().setEnabled(false);
        chartProgress.getDescription().setEnabled(false);
        chartProgress.getAxisLeft().setAxisLineColor(ContextCompat.getColor(context, R.color.accent));
        chartProgress.getAxisLeft().setAxisLineWidth(axisWidht);
        chartProgress.getAxisLeft().setTextSize(12.0f);
        chartProgress.getXAxis().setTextSize(12.0f);
        chartProgress.setExtraBottomOffset(5.0f);
        chartProgress.invalidate();
    }
}
