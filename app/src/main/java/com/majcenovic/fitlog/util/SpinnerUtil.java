package com.majcenovic.fitlog.util;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.majcenovic.fitlog.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by martina on 27/06/2017.
 */

public class SpinnerUtil {

    public static List<Double> weeklyGoalWeights;
    public static List<String> activityLevels;

    public static void populateWeeklyGoal(Context context, Spinner spSignUpWeeklyGoal) {
        weeklyGoalWeights = new ArrayList<>();
        weeklyGoalWeights.add(0.25);
        weeklyGoalWeights.add(0.5);
        weeklyGoalWeights.add(0.75);
        weeklyGoalWeights.add(1.0);

        List<String> list = new ArrayList<>();

        DecimalFormat format = new DecimalFormat();
        format.setMinimumFractionDigits(0);
        format.setMaximumFractionDigits(2);

        for (Double num : weeklyGoalWeights) {
            list.add(String.format(context.getString(R.string.weight_kg), format.format(num)));
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context, R.layout.custom_spinner_item, list);
        dataAdapter.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);
        spSignUpWeeklyGoal.setAdapter(dataAdapter);
    }

    public static void populateActivityLevelsSp(Context context, Spinner spSignUpActivityLevel) {
        activityLevels = new ArrayList<>();
        activityLevels.add(context.getString(R.string.not_active));
        activityLevels.add(context.getString(R.string.slightly_active));
        activityLevels.add(context.getString(R.string.active));
        activityLevels.add(context.getString(R.string.very_active));
        activityLevels.add(context.getString(R.string.extremely_active));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context, R.layout.custom_spinner_item, activityLevels);
        dataAdapter.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);
        spSignUpActivityLevel.setAdapter(dataAdapter);
    }

    public static void populateActivityLevelsSpDefault(Context context, Spinner spSignUpActivityLevel) {
        activityLevels = new ArrayList<>();
        activityLevels.add(context.getString(R.string.not_active));
        activityLevels.add(context.getString(R.string.slightly_active));
        activityLevels.add(context.getString(R.string.active));
        activityLevels.add(context.getString(R.string.very_active));
        activityLevels.add(context.getString(R.string.extremely_active));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, activityLevels);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSignUpActivityLevel.setAdapter(dataAdapter);
    }
}
