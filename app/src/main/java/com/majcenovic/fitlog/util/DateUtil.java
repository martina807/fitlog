package com.majcenovic.fitlog.util;

import android.content.Context;

import com.majcenovic.fitlog.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by martina on 30/01/2017.
 */

public class DateUtil {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String YEAR_FORMAT = "MM";

    public static DateTime convertToJodaDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);
        return formatter.parseDateTime(dateString);
    }

    public static DateTime convertToJodaDateTime(String dateTimeString) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_TIME_FORMAT).withZoneUTC();
        return formatter.parseDateTime(dateTimeString);
    }

    public static String getDefaultDateTimeFormatted(String dateTimeString) {
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT);
        return df.format(convertToJodaDateTime(dateTimeString).toDate());
    }

    public static String getCurrentDateTime() {
        return DateTime.now().toString(DATE_TIME_FORMAT);
    }

    public static String getRelativeDateTimeFormatted(Context context, String dateTimeString) {
        return android.text.format.DateUtils.getRelativeDateTimeString(
                context,
                convertToJodaDateTime(dateTimeString).getMillis(),
                android.text.format.DateUtils.MINUTE_IN_MILLIS,
                android.text.format.DateUtils.WEEK_IN_MILLIS,
                android.text.format.DateUtils.FORMAT_ABBREV_RELATIVE).toString();
    }

    public static String getTimeBeforeFormatted(Context context, String date) {
        int time;
        if (date == null || date.trim().length() == 0) return "";

        DateTime dt = getDateTimeFromUTCString(date);
        DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT);

        int seconds = (int) Math.abs(DateTime.now().getMillis() / 1000 - dt.getMillis() / 1000);

        if (seconds == -1) return toNiceLongDateAndShortTimeFormat(date);
        if (seconds < 60) return context.getString(R.string.date_time_just_now);
        else if (seconds < 60 * 60) {
            time = seconds / 60;
            return context.getResources().getQuantityString(R.plurals.date_time_minutes_ago, time, time);
        } else if (seconds < 60 * 60 * 24) {
            time = seconds / (60 * 60);
            return context.getResources().getQuantityString(R.plurals.date_time_hours_ago, time, time);
        } else if (seconds < 2 * 60 * 60 * 24)
            return String.format(context.getString(R.string.date_time_yesterday_at), df.format(dt.toDate()));
        else return toNiceLongDateAndShortTimeFormat(date);
    }

    public static String toNiceLongDateAndShortTimeFormat(String createdAt) {
        Date rawDate = getDateTimeFromUTCString(createdAt).toDate();
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
        return df.format(rawDate);
    }

    public static DateTime getDateTimeFromUTCString(String dateTimeString) {
        if (dateTimeString == null) return null;
        DateTimeFormatter ft = DateTimeFormat.forPattern(DATE_TIME_FORMAT).withZoneUTC();
        return ft.parseDateTime(dateTimeString).withZone(DateTimeZone.getDefault());
    }

    public static String toNiceDateAndTimeFormat(String dateTimeString) {
        DateTime dateTmp = getDateTimeFromUTCString(dateTimeString);
        Date date = dateTmp.toDate();
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
        return df.format(date);
    }

    public static String toNiceMediumDateNoYearFormat(String dateTimeString) {
        DateTime dateTime = convertToJodaDateTime(dateTimeString);

        SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.MEDIUM);
        try {
            sdf.applyPattern(sdf.toPattern().replaceAll("[^\\p{Alpha}]*y+[^\\p{Alpha}]*", ""));
        } catch (Throwable ex) {
            ex.printStackTrace();
        }

        return sdf.format(dateTime.toDate());
    }
}