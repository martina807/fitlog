package com.majcenovic.fitlog.util;

import android.content.Context;
import android.util.DisplayMetrics;

import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.R;

import org.joda.time.DateTime;

import java.util.Locale;

/**
 * Created by martina on 05/08/2017.
 */

public class ConverterUtil {

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static String fromSecToStandard(Integer sec) {
        String standard = "";

        Integer hr = sec / 3600;
        Integer min = sec / 60 - hr * 60;
        Integer s = sec - min * 60 - hr * 3600;

        if (hr > 0) {
            standard = String.format(BaseApplication.appContext.getString(R.string.hr_min), hr, min);
        } else if (hr == 0 && min > 0) {
            standard = String.format(BaseApplication.appContext.getString(R.string.min), min);
        } else if (hr == 0 && min == 0) {
            standard = String.format(BaseApplication.appContext.getString(R.string.sec), s);
        }

        return standard;
    }

    public static String fromMiliSecToStandard(Long miliSec) {
        Long hr = miliSec / 3600000;
        Long min = miliSec / 60000 - hr * 60000;
        Long s = (miliSec - min * 60000 - hr * 3600000)/1000;

        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hr, min, s);
    }

    public static String fromMetersToStandard(Double meters) {
        Double km = meters / 1000.0;
        return String.format(BaseApplication.appContext.getString(R.string.km), km);
    }

    public static String fromMetersToStandard(float meters) {
        float km = meters / 1000.0f;
        return String.format(BaseApplication.appContext.getString(R.string.km), km);
    }

    public static String formatCal(Integer cal) {
        return String.format(BaseApplication.appContext.getString(R.string.no_cal), cal);
    }

    public static Long getTimeNowInSec() {
        return DateTime.now().getMillis() / 1000;
    }
}
