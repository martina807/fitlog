package com.majcenovic.fitlog.util;

import com.majcenovic.fitlog.data.models.dto.ActivityDto;

import org.joda.time.DateTime;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by martina on 26/06/2017.
 */

public class InputValidationUtil {

    private static int minPasswordLength = 6;
    private static int minNameLength = 1;
    private static int minBirthdayLength = 1;
    private static int minHeight = 100;
    private static Double minWeight = 20.0;
    private static int minGoalWeightLength = 0;

    public static boolean isEmailValid(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return !email.trim().isEmpty() && matcher.find();
    }

    public static boolean isPasswordValid(String password) {
        return password.trim().length() >= minPasswordLength;
    }

    public static boolean isNameValid(String name) {
        return name.trim().length() >= minNameLength;
    }

    public static boolean isBirthdayValid(String birthday) {
        return DateUtil.convertToJodaDate(birthday).getYear() <= DateTime.now().getYear() - 10
                && birthday.trim().length() >= minBirthdayLength;
    }

    public static boolean isGoalWeightValid(String goalWeight) {
        return goalWeight.trim().length() > minGoalWeightLength;
    }

    public static boolean isActivityLevelValid(String activityLevel) {
        return activityLevel.trim().length() > minGoalWeightLength;
    }

    public static boolean isWeeklyGoalWeightValid(String weeklyGoalWeight) {
        return weeklyGoalWeight.trim().length() > minGoalWeightLength;
    }

    public static boolean isWeightValid(Double weight) {
        return weight >= minWeight;
    }

    public static boolean isHeightValid(int height) {
        return height >= minHeight;
    }

    public static boolean isActivityInputValid(ActivityDto activityDto) {
        if(activityDto.getAvgSpeed() == null) return false;
        if(activityDto.getTopSpeed() == null) return false;
        if(activityDto.getDistance() == null) return false;
        if(activityDto.getDuration() == null) return false;

        return true;
    }
}
