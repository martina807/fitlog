package com.majcenovic.fitlog.util;

/**
 * Created by martina on 07/08/2017.
 */

public class ListRowTypeUtils {

    public static final int CONTENT = 1;
    public static final int HEADER = 2;
    public static final int EMPTY = 3;
    public static final int FOOTER = 4;

    public static final int CHAT_ME_CONTENT = 5;
    public static final int CHAT_OTHER_CONTENT = 6;

    public static final int FILTER = 7;
}
