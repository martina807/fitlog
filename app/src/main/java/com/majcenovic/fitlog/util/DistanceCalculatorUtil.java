package com.majcenovic.fitlog.util;

import android.location.Location;

import com.majcenovic.fitlog.data.models.dto.UpdateLocationDto;

import java.util.List;

/**
 * Created by martina on 10/08/2017.
 */

public class DistanceCalculatorUtil {

    public static float getTraveledDistance(List<UpdateLocationDto> locations) {
        float distance = 0f;

        if (locations.size() <= 1) {
            return 0.0f;
        }

        for (int i = 0; i < locations.size() - 1; i++) {
            Location loc1 = new Location("loc1");
            loc1.setLatitude(locations.get(i).getLatitude());
            loc1.setLongitude(locations.get(i).getLongitude());

            Location loc2 = new Location("loc2");
            loc2.setLatitude(locations.get(i + 1).getLatitude());
            loc2.setLongitude(locations.get(i + 1).getLongitude());

            distance += loc1.distanceTo(loc2);

        }

        return distance;
    }
}
