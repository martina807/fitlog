package com.majcenovic.fitlog;

/**
 * Created by martina on 26/06/2017.
 */

public interface BaseView {

    void showError(String message);
    void showProgress();
    void hideProgress();
}
