package com.majcenovic.fitlog.ui.profile.gallery;

import com.majcenovic.fitlog.data.galleries.GalleryInteractorImpl;
import com.majcenovic.fitlog.data.galleries.GalleryListener;
import com.majcenovic.fitlog.data.models.Photo;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public class GalleryPresenter implements GalleryContract.Presenter {

    private GalleryContract.View view;
    private final GalleryInteractorImpl interactor;

    public GalleryPresenter(GalleryContract.View view, GalleryInteractorImpl interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
    }

    @Override
    public void openPhoto(Photo photo) {
        view.showPhoto(photo);
    }

    @Override
    public void likeDislikePost(final Post post) {
        if (post.isLiked()) {
            interactor.dislike(post, new GalleryListener.DislikeCallback() {
                @Override
                public void onDislikePosted() {
                    post.setLiked(!post.isLiked());
                    post.decrementLikes();
                    view.refreshData();
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        } else {
            interactor.like(post, new GalleryListener.LikeCallback() {
                @Override
                public void onLikePosted() {
                    post.setLiked(!post.isLiked());
                    post.incrementLikes();
                    view.refreshData();
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        }
    }

    @Override
    public void openComments(Post post) {
        view.showComments(post);
    }

    @Override
    public void loadPhotoPosts(User user) {
        view.showProgress();
        interactor.loadPhotoPosts(user.getId(), new GalleryListener.PostCallback() {
            @Override
            public void onPostsLoaded(List<Post> posts) {
                view.hideProgress();
                view.showPhotosFromPosts(posts);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }
}