package com.majcenovic.fitlog.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.majcenovic.fitlog.BaseTabsViewPagerAdapter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.storage.SharedPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 26/06/2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView {

    protected Context context;
    protected User user;
    protected BaseTabsViewPagerAdapter tabsViewPagerAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        user = SharedPrefs.readUserData(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    protected abstract int getLayoutId();

    @Override
    public void showError(String message) {
        showPopUp(message);
    }

    @Override
    public void showProgress() {
        ((BaseActivity) context).showProgress();
    }

    @Override
    public void hideProgress() {
        ((BaseActivity) context).hideProgress();
    }

    protected void showPopUp(String message) {
        ((BaseActivity) context).showPopUp(message);
    }

    protected void showPopUp(@StringRes int resString) {
        ((BaseActivity) context).showPopUp(resString);
    }
}