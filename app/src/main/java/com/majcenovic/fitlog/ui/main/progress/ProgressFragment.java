package com.majcenovic.fitlog.ui.main.progress;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.majcenovic.fitlog.BaseTabsViewPagerAdapter;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.UpdateProgressEvent;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseFragment;
import com.majcenovic.fitlog.ui.goals.GoalTypes;
import com.majcenovic.fitlog.ui.main.progress.month_year.ProgressMonthYearFragment;
import com.majcenovic.fitlog.ui.main.progress.update_current_weight.UpdateGoalCustomDialog;
import com.majcenovic.fitlog.util.ExtraUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 28/06/2017.
 */

public class ProgressFragment extends BaseFragment implements ProgressContract.View {

    public static final String TAG = "ProgressFragment";

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.tvCurrentWeight)
    TextView tvCurrentWeight;

    private ProgressContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = Injection.provideProgressPresenter(this, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupTabs();
    }

    @Override
    public void onStart() {
        super.onStart();

        presenter.loadGoals();
    }

    private void setupTabs() {
        tabsViewPagerAdapter = new BaseTabsViewPagerAdapter(getChildFragmentManager());
        tabsViewPagerAdapter.addFragment(ProgressMonthYearFragment.newInstance(ExtraUtil.PROGRESS_TYPE_MONTH), getString(R.string.this_month));
        tabsViewPagerAdapter.addFragment(ProgressMonthYearFragment.newInstance(ExtraUtil.PROGRESS_TYPE_YEAR), getString(R.string.this_year));
        viewPager.setAdapter(tabsViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_progress;
    }

    @Override
    public void showCurrentWeight(Double currentWeight) {
        tvCurrentWeight.setText(String.format(getString(R.string.double_kg), currentWeight));
    }

    @OnClick(R.id.ibCurrentWeightUpdate)
    public void onUpdateCurrentWeightClick() {
        UpdateGoalCustomDialog.newInstance(GoalTypes.CURRENT_WEIGHT, "", new UpdateGoalCustomDialog.UpdateValueCallback() {
            @Override
            public void onCurrentWeightUpdated() {
                presenter.loadGoals();
                EventBus.getDefault().postSticky(new UpdateProgressEvent());
            }

            @Override
            public void onWeeklyGoalUpdated() {
            }

            @Override
            public void onFinalGoalUpdated() {
            }

            @Override
            public void onActivityLevelUpdated() {
            }
        }).show(getActivity().getSupportFragmentManager(), UpdateGoalCustomDialog.TAG);
    }
}
