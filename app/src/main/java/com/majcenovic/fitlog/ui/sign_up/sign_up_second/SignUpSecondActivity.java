package com.majcenovic.fitlog.ui.sign_up.sign_up_second;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;

import com.majcenovic.fitlog.BuildConfig;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.sign_up.sign_up_third.SignUpThirdActivity;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpSecondActivity extends BaseActivity implements SignUpSecondContract.View {

    @BindView(R.id.rbSignUpMale)
    AppCompatRadioButton rbSignUpMale;

    @BindView(R.id.rbSignUpFemale)
    AppCompatRadioButton rbSignUpFemale;

    @BindView(R.id.etSignUpBirthday)
    AppCompatEditText etSignUpBirthday;

    @BindView(R.id.etSignUpHeight)
    AppCompatEditText etSignUpHeight;

    @BindView(R.id.etSignUpCurrentWeight)
    AppCompatEditText etSignUpCurrentWeight;

    private String selectedDateString;

    private SignUpSecondContract.Presenter presenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_up_second;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_second);
        ButterKnife.bind(this);
        initToolbar(R.string.sign_up);

        if (BuildConfig.DEBUG) {
            etSignUpHeight.setText("175");
            etSignUpCurrentWeight.setText("60");
            DateTime dt = new DateTime(2000, 6, 13, 0, 0);
            selectedDateString = String.format(Locale.US, "%04d-%02d-%02d", 2014, 6, 13);
            etSignUpBirthday.setText(DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.US).format(dt.toDate()));
        }

        presenter = Injection.provideSignUpSecondPresenter(this);

    }

    @Override
    public void showInvalidInput() {
        showPopUp(R.string.invalid_input_older_than_18);
    }

    @Override
    public void showSignUpThird(SignUpDto signUpDto) {
        startActivity(new Intent(getApplicationContext(), SignUpThirdActivity.class)
                .putExtra(ExtraUtil.SIGN_UP_DTO, signUpDto));
    }

    @OnCheckedChanged(R.id.rbSignUpMale)
    void maleChanged(boolean isChecked) {
        rbSignUpFemale.setChecked(!isChecked);
    }

    @OnCheckedChanged(R.id.rbSignUpFemale)
    void femaleChanged(boolean isChecked) {
        rbSignUpMale.setChecked(!isChecked);
    }

    @OnClick(R.id.btnSignUpSecondNext)
    public void nextClick() {
        SignUpDto signUpDto = getIntent().getParcelableExtra(ExtraUtil.SIGN_UP_DTO);
        signUpDto.setGender(rbSignUpMale.isChecked() ? 1 : 2);
        signUpDto.setBirthday(selectedDateString);
        signUpDto.setHeight(Integer.parseInt(etSignUpHeight.getText().toString()));
        signUpDto.setCurrentWeight(Double.parseDouble(etSignUpCurrentWeight.getText().toString()));

        presenter.submitNext(signUpDto);
    }

    @OnClick(R.id.vSignUpBirthdayOverlay)
    void birthdayOverlayClick() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        DateTime dt = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
                        selectedDateString = String.format(Locale.US, "%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                        etSignUpBirthday.setText(DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.US).format(dt.toDate()));
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.showYearPickerFirst(true);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    protected void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }
}
