package com.majcenovic.fitlog.ui.comments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.ImageButton;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Comment;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 21/06/2017.
 */

public class CommentsActivity extends BaseActivity implements CommentsContract.View {

    @BindView(R.id.etComment)
    EditText etComment;

    @BindView(R.id.srlList)
    SwipeRefreshLayout srlList;

    @BindView(R.id.rvList)
    RecyclerView rvList;

    @BindView(R.id.ibSendComment)
    ImageButton ibSendComment;

    private final long SCALE_ANIM_DURATION = 150;
    private Post post;
    private CommentsContract.Presenter presenter;
    private String comment;

    private CommentsAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_comments;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initToolbar(R.string.comments);
        initTextListener();
        presenter = Injection.provideCommentsPresenter(this, context);
        rvList.setLayoutManager(new LinearLayoutManager(context));

        post = getIntent().getParcelableExtra(ExtraUtil.OBJECT);
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.loadComments(post);
    }

    @Override
    protected void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }


    @Override
    public void showComments(List<Comment> comments) {
        if (adapter == null) {
            adapter = new CommentsAdapter(context, createLikeDislikeClickListener());
        }

        adapter.updateData(comments);

        rvList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private CommentsAdapter.CommentClickListener createLikeDislikeClickListener() {
        return new CommentsAdapter.CommentClickListener() {
            @Override
            public void onLikeDislikeClick(Comment comment) {
                presenter.likeDislikeComment(comment);
            }
        };
    }

    @Override
    public void refreshCommentList() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void refreshComment(int position) {
        adapter.notifyItemChanged(position);
    }

    @Override
    public void clearInputAndHideKeyboard() {
        etComment.setText("");
        hideKeyboard(context);
    }

    @OnClick(R.id.ibSendComment)
    public void onSendCommentClick() {
        if(isInputValid()) {
            presenter.postComment(post, etComment.getText().toString().trim());
        }
    }

    private boolean isInputValid() {
        if(etComment.getText().toString().trim().length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    private void initTextListener() {
        ibSendComment.setEnabled(false);
        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etComment.getText().toString().trim().length() > 0) {
                    if (!ibSendComment.isEnabled()) {
                        ibSendComment.setEnabled(true);
                        animateSendButton(R.drawable.send_hangouts_active);
                    }
                } else {
                    if (ibSendComment.isEnabled()) {
                        ibSendComment.setEnabled(false);
                        animateSendButton(R.drawable.send_hangouts_inactive);
                    }
                }
            }

            private void animateSendButton(final int drawableToShow) {
                final ScaleAnimation scaleUp = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF, (float) 0.5);
                scaleUp.setDuration(SCALE_ANIM_DURATION);
                ScaleAnimation scaleDown = new ScaleAnimation(1, 0, 1, 0, Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF, (float) 0.5);
                scaleDown.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        ibSendComment.setImageResource(drawableToShow);
                        ibSendComment.startAnimation(scaleUp);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                scaleDown.setDuration(SCALE_ANIM_DURATION);
                ibSendComment.startAnimation(scaleDown);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
