package com.majcenovic.fitlog.ui.main.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.profile.view_holders.ImageBoardPostViewHolder;
import com.majcenovic.fitlog.ui.profile.view_holders.MessageBoardPostViewHolder;
import com.majcenovic.fitlog.ui.profile.view_holders.StatusBoardViewHolder;
import com.majcenovic.fitlog.util.ChartUtil;
import com.majcenovic.fitlog.util.DateUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int STATUS = 1;
    private final int MESSAGE = 2;
    private final int IMAGE = 3;

    private List<Post> posts = new ArrayList<>();
    private final User user;

    private final StatusBoardViewHolder.StatusClickListener statusClickListener;
    private final ProfileClickListener profileClickListener;

    private User me;
    private Bundle savedInstanceState;
    private Context context;
    private HashMap<Integer, MapView> mapViews = new HashMap<>();

    public void updatePost(Post post) {
        if (posts != null && posts.contains(post)) {
            int postPosition = posts.indexOf(post);

            // Status is on index 0, posts start at position 1
            int viewHolderPosition = postPosition + 1;
            notifyItemChanged(viewHolderPosition);
        }
    }

    public interface ProfileClickListener {
        void onUserClick(User user);

        void onLikeDislikeClick(Post post);

        void onCommentsClick(Post post);
    }

    public HomeAdapter(User user, StatusBoardViewHolder.StatusClickListener statusClickListener,
                       ProfileClickListener profileClickListener) {
        this.profileClickListener = profileClickListener;
        this.user = user;
        this.statusClickListener = statusClickListener;
        me = SharedPrefs.readUserData(BaseApplication.appContext);
    }

    public void onResume() {
        if (!mapViews.isEmpty()) {
            for (MapView mapView : mapViews.values()) {
                try {
                    mapView.onResume();
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void onPause() {
        if (!mapViews.isEmpty()) {
            for (MapView mapView : mapViews.values()) {
                try {
                    mapView.onPause();
                    mapView = null;
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void addPosts(List<Post> newPosts) {
        this.posts.clear();

        if (newPosts == null) {
            notifyDataSetChanged();
            return;
        }

        this.posts.addAll(newPosts);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        Integer layoutId = null;
        switch (viewType) {
            case STATUS:
                return new StatusBoardViewHolder(LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.row_board_status, parent, false));
            default:
                if (viewType == MESSAGE) {
                    return new MessageBoardPostViewHolder(LayoutInflater
                            .from(parent.getContext())
                            .inflate(R.layout.row_board_message_post, parent, false));
                } else {
                    return new ImageBoardPostViewHolder(LayoutInflater
                            .from(parent.getContext())
                            .inflate(R.layout.row_board_image_post, parent, false));
                }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (position) {
            case 0:
                setUpStatus((StatusBoardViewHolder) holder);
                break;
            default:
                setUpPost(holder, posts.get(position - 1), position);
        }
    }

    private void setUpStatus(StatusBoardViewHolder holder) {
        Picasso.with(context).load(me.getProfilePicture()).into(holder.getRivUserImage());
        holder.setStatusClickListener(statusClickListener);
    }

    private void setUpPost(RecyclerView.ViewHolder holder, final Post post, int position) {

        switch (post.getType()) {
            case "message":
                MessageBoardPostViewHolder messageHolder = (MessageBoardPostViewHolder) holder;
                messageHolder.getTvMessageSenderRecipient().setText(Html.fromHtml(post.getTitle()));
                messageHolder.getTvBoardDateTime().setText(DateUtil.toNiceDateAndTimeFormat(post.getCreatedAt()));
                messageHolder.getTvBoardMessageDesc().setText(post.getMessage());
                messageHolder.getTvBoardNoOfComments().setText(String.valueOf(post.getCommentCount()));
                messageHolder.getTvNoOfBoardLikes().setText(String.valueOf(post.getLikesCount()));
                messageHolder.getIbBoardComment().setImageResource(R.drawable.comment);
                if (post.isLiked()) {
                    messageHolder.getIbBoardLike().setImageResource(R.drawable.like_filled);
                } else {
                    messageHolder.getIbBoardLike().setImageResource(R.drawable.like);
                }
                messageHolder.getTvBoardMessageDesc().setVisibility(View.VISIBLE);
                Picasso.with(BaseApplication.appContext)
                        .load(post.getSender().getProfilePicture())
                        .into(messageHolder.getRivProfileImage());

                messageHolder.getRivProfileImage().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profileClickListener.onUserClick(post.getSender());
                    }
                });


                messageHolder.getIbBoardLike().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profileClickListener.onLikeDislikeClick(post);
                    }
                });

                messageHolder.getIbBoardComment().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profileClickListener.onCommentsClick(post);
                    }
                });

                break;
            case "photo":
            case "progress":
            case "activity":
                ImageBoardPostViewHolder imageHolder = (ImageBoardPostViewHolder) holder;
                imageHolder.getTvBoardSenderRecipient().setText(Html.fromHtml(post.getTitle()));
                imageHolder.getTvBoardDateTime().setText(DateUtil.toNiceDateAndTimeFormat(post.getCreatedAt()));
                imageHolder.getTvBoardDescription().setText(post.getMessage());
                imageHolder.getTvBoardNoOfComments().setText(String.valueOf(post.getCommentCount()));
                imageHolder.getTvBoardNoOfLikes().setText(String.valueOf(post.getLikesCount()));
                if (post.getType().equals("photo")) {
                    Picasso.with(BaseApplication.appContext)
                            .load(post.getPhotos().get(0).getPhotoUrl())
                            .into(imageHolder.getIvBoardImage());
                } else if (post.getType().equals("progress")) {
                    setUpChart(imageHolder, post.getProgress());
                } else {
                    setUpMap(imageHolder, post.getActivity().getLocations());
                }
                setLayout(post.getType(), imageHolder);
                imageHolder.getIbBoardComment().setImageResource(R.drawable.comment);
                if (post.isLiked()) {
                    imageHolder.getIbBoardLike().setImageResource(R.drawable.like_filled);
                } else {
                    imageHolder.getIbBoardLike().setImageResource(R.drawable.like);
                }

                imageHolder.getIbBoardLike().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profileClickListener.onLikeDislikeClick(post);
                    }
                });

                imageHolder.getIbBoardComment().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profileClickListener.onCommentsClick(post);
                    }
                });

                if (post.getMessage() != null) {
                    imageHolder.getTvBoardDescription().setVisibility(View.VISIBLE);
                } else {
                    imageHolder.getTvBoardDescription().setVisibility(View.GONE);
                }

                Picasso.with(BaseApplication.appContext)
                        .load(post.getSender().getProfilePicture())
                        .into(imageHolder.getRivPostProfileImage());

                imageHolder.getRivPostProfileImage().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profileClickListener.onUserClick(post.getSender());
                    }
                });
                break;
        }
    }

    private void setLayout(String type, ImageBoardPostViewHolder holder) {

        holder.getIvBoardImage().setVisibility(View.GONE);
        holder.getChartBoardProgress().setVisibility(View.GONE);
        holder.getMapBoardActivity().setVisibility(View.GONE);

        if (type.equals("photo")) {
            holder.getIvBoardImage().setVisibility(View.VISIBLE);
        } else if (type.equals("progress")) {
            holder.getChartBoardProgress().setVisibility(View.VISIBLE);
        } else if (type.equals("activity")) {
            holder.getMapBoardActivity().setVisibility(View.VISIBLE);
        }
    }

    private void setUpMap(final ImageBoardPostViewHolder holder, final List<GpsLocation> locations) {
        holder.getMapBoardActivity().onCreate(savedInstanceState);
        holder.getMapBoardActivity().getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                MapsInitializer.initialize(context);
                holder.getMapBoardActivity().onResume();
                mapViews.put(holder.getMapBoardActivity().hashCode(), holder.getMapBoardActivity());

                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        PolylineOptions options = new PolylineOptions().width(10).color(ContextCompat.getColor(BaseApplication.appContext, R.color.accent));
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();

                        for (GpsLocation location : locations) {
                            LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
                            options.add(point);
                            builder.include(point);
                        }

                        if (options.getPoints() != null && !options.getPoints().isEmpty()) {
                            googleMap.addPolyline(options);
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(),
                                    BaseApplication.appContext.getResources().getDimensionPixelSize(R.dimen.spacing_normal));
                            googleMap.moveCamera(cu);
                        }

                    }
                });
            }
        });
    }

    private void setUpChart(ImageBoardPostViewHolder holder, List<Progress> progressList) {
        ChartUtil.setChartProgress(holder.getChartBoardProgress(), progressList, BaseApplication.appContext);
    }

    @Override
    public int getItemCount() {
        return posts.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return STATUS;
            default:
                if (posts.get(position - 1).getType().equals("message")) {
                    return MESSAGE;
                } else {
                    return IMAGE;
                }
        }
    }

    public void setSavedInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }
}
