package com.majcenovic.fitlog.ui.profile.edit;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.UpdateUserDto;
import com.majcenovic.fitlog.data.profile.edit.EditInteractor;
import com.majcenovic.fitlog.data.profile.edit.EditListener;

/**
 * Created by martina on 09/09/2017.
 */

public class EditProfilePresenter implements EditProfileContract.Presenter {

    private final EditProfileContract.View view;
    private final EditInteractor interactor;

    public EditProfilePresenter(EditProfileContract.View view, EditInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadProfile() {
        view.showProgress();
        interactor.getMyProfile(new EditListener.GetMyProfileCallback() {
            @Override
            public void onProfileLoaded(User user) {
                view.hideProgress();
                view.showProfile(user);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void pickImageClick(boolean isStorageGranted) {
        if (isStorageGranted) {
            view.showImagePicker();
        } else {
            view.showRequestPermissions();
        }
    }

    @Override
    public void updateProfile(UpdateUserDto updateUserDto) {
        view.showProgress();
        interactor.updateProfile(updateUserDto, new EditListener.ProfileUpdateCallback() {
            @Override
            public void onProfileUpdated(User user) {
                view.hideProgress();
                view.showProfileUpdated(user);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }
}