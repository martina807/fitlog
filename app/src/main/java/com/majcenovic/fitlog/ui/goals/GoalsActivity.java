package com.majcenovic.fitlog.ui.goals;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.models.UpdateProgressEvent;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.main.progress.update_current_weight.UpdateGoalCustomDialog;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * Created by martina on 28/06/2017.
 */

public class GoalsActivity extends BaseActivity implements GoalsContract.View {

    @BindView(R.id.rvList)
    RecyclerView rvList;

    private GoalsAdapter rvGoalsAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_goals;
    }

    private GoalsContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar(getString(R.string.goals));

        presenter = Injection.provideGoalsPresenter(this, context);
        rvList.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.loadGoals();
    }

    @Override
    public void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }

    @Override
    public void showGoals(Goal goal) {
        if (rvGoalsAdapter == null) {
            rvGoalsAdapter = new GoalsAdapter(context, goal, createGoalClickListener());
        }

        if (rvList.getAdapter() == null) {
            rvList.setAdapter(rvGoalsAdapter);
        }

        rvGoalsAdapter.updateData(goal);
    }

    @Override
    public void showUpdateDialog(Goal goal, int weightTypeId) {
        UpdateGoalCustomDialog.newInstance(weightTypeId, goal.getValueForType(weightTypeId),
                new UpdateGoalCustomDialog.UpdateValueCallback() {
            @Override
            public void onCurrentWeightUpdated() {
                presenter.loadGoals();
            }

            @Override
            public void onWeeklyGoalUpdated() {
                presenter.loadGoals();
            }

            @Override
            public void onFinalGoalUpdated() {
                presenter.loadGoals();
            }

            @Override
            public void onActivityLevelUpdated() {
                presenter.loadGoals();
            }
        }).show(getSupportFragmentManager(), UpdateGoalCustomDialog.TAG);
    }

    private GoalsAdapter.GoalClickListener createGoalClickListener() {
        return new GoalsAdapter.GoalClickListener() {
            @Override
            public void onCurrentWeightClick(Goal goal) {
                presenter.openUpdateDialog(goal, GoalTypes.CURRENT_WEIGHT);
            }

            @Override
            public void onWeeklyGoalClick(Goal goal) {
                presenter.openUpdateDialog(goal, GoalTypes.WEEKLY_GOAL);
            }

            @Override
            public void onFinalGoalClick(Goal goal) {
                presenter.openUpdateDialog(goal, GoalTypes.FINAL_GOAL);
            }

            @Override
            public void onActivityLevelClick(Goal goal) {
                presenter.openUpdateDialog(goal, GoalTypes.ACTIVITY_LEVEL);
            }
        };
    }
}