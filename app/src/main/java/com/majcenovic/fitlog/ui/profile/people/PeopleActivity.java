package com.majcenovic.fitlog.ui.profile.people;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.main.home.search.SearchActivity;
import com.majcenovic.fitlog.ui.profile.ProfileActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 14/08/2017.
 */

public class PeopleActivity extends BaseActivity implements PeopleContract.View {

    @BindView(R.id.rvListPeople)
    RecyclerView rvList;

    @BindView(R.id.tvEmptyState)
    TextView tvEmptyState;

    @BindView(R.id.btnSearchForFriends)
    TextView btnSearchForFriends;

    private PeopleContract.Presenter presenter;
    private PeopleAdapter listAdapter;
    private List<User> listItems = new ArrayList<>();
    private User me;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rvList.setLayoutManager(new LinearLayoutManager(context));

        me = SharedPrefs.readUserData(context);

        user = getIntent().getParcelableExtra(ExtraUtil.USER);

        presenter = Injection.providePeoplePresenter(this, context);


        initToolbar(String.format(getString(R.string.users_friends), user.getFirstName()));
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.loadPeople(user.getId());
    }

    @Override
    protected void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_people;
    }

    @Override
    public void showPeople(List<User> people) {





        if ((people == null || people.isEmpty()) && user.getId().equals(me.getId())) {
            tvEmptyState.setVisibility(View.VISIBLE);
            btnSearchForFriends.setVisibility(View.VISIBLE);
            rvList.setVisibility(View.GONE);
        } else if ((people == null || people.isEmpty()) && !user.getId().equals(me.getId())) {
            tvEmptyState.setVisibility(View.VISIBLE);
            btnSearchForFriends.setVisibility(View.GONE);
            rvList.setVisibility(View.GONE);
        } else {
            tvEmptyState.setVisibility(View.GONE);
            btnSearchForFriends.setVisibility(View.GONE);
            rvList.setVisibility(View.VISIBLE);

            listItems.clear();
            listItems.addAll(people);

            if (listAdapter == null) {
                listAdapter = new PeopleAdapter(context, people, me, setPeopleClickListener());
            }

            if (rvList.getAdapter() == null) {
                rvList.setAdapter(listAdapter);
            }

            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showUserProfile(User user) {
        startActivity(new Intent(getApplicationContext(), ProfileActivity.class)
                .putExtra(ExtraUtil.USER, user));
    }

    @Override
    public void updateUserRow() {
        listAdapter.notifyDataSetChanged();
    }

    private PeopleAdapter.PeopleClickListener setPeopleClickListener() {
        return new PeopleAdapter.PeopleClickListener() {
            @Override
            public void onUserClick(User user) {
                presenter.startUserProfile(user);
            }

            @Override
            public void onFollowClick(User user) {
                presenter.follow(user);
            }

            @Override
            public void onUnfollowClick(User user) {
                presenter.unfollow(user);
            }
        };
    }

    @OnClick(R.id.btnSearchForFriends)
    public void onSearchClick() {
        startActivity(new Intent(getApplicationContext(), SearchActivity.class));
    }
}
