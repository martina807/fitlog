package com.majcenovic.fitlog.ui.profile.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.google.android.gms.maps.MapView;
import com.majcenovic.fitlog.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 14/08/2017.
 */

public class ImageBoardPostViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvBoardSenderRecipient)
    TextView tvBoardSenderRecipient;

    @BindView(R.id.tvBoardDateTime)
    TextView tvBoardDateTime;

    @BindView(R.id.tvBoardDescription)
    TextView tvBoardDescription;

    @BindView(R.id.ivBoardImage)
    ImageView ivBoardImage;

    @BindView(R.id.ibBoardLike)
    ImageButton ibBoardLike;

    @BindView(R.id.ibBoardComment)
    ImageButton ibBoardComment;

    @BindView(R.id.tvBoardNoOfComments)
    TextView tvBoardNoOfComments;

    @BindView(R.id.mapBoardActivity)
    MapView mapBoardActivity;

    @BindView(R.id.chartBoardProgress)
    LineChart chartBoardProgress;

    @BindView(R.id.tvBoardNoOfLikes)
    TextView tvBoardNoOfLikes;

    @BindView(R.id.rivPostProfileImage)
    RoundedImageView rivPostProfileImage;

    public ImageBoardPostViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getTvBoardSenderRecipient() {
        return tvBoardSenderRecipient;
    }

    public TextView getTvBoardDateTime() {
        return tvBoardDateTime;
    }

    public TextView getTvBoardDescription() {
        return tvBoardDescription;
    }

    public ImageView getIvBoardImage() {
        return ivBoardImage;
    }

    public ImageButton getIbBoardLike() {
        return ibBoardLike;
    }

    public ImageButton getIbBoardComment() {
        return ibBoardComment;
    }

    public TextView getTvBoardNoOfComments() {
        return tvBoardNoOfComments;
    }

    public TextView getTvBoardNoOfLikes() {
        return tvBoardNoOfLikes;
    }

    public RoundedImageView getRivPostProfileImage() {
        return rivPostProfileImage;
    }

    public MapView getMapBoardActivity() {
        return mapBoardActivity;
    }

    public LineChart getChartBoardProgress() {
        return chartBoardProgress;
    }
}
