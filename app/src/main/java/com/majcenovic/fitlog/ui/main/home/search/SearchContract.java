package com.majcenovic.fitlog.ui.main.home.search;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 09/09/2017.
 */

public interface SearchContract {

    interface View extends BaseView {
        void showPeople(List<User> users);
        void clearInput();
        void showDefaultBackBehavior();
        void showUserDetails(User user);
        void updateFollowState(User user);
    }

    interface Presenter extends BasePresenter {
        void search(String input);
        void onBackPressed(String input);
        void openUserDetails(User user);
        void followUnfollow(User user);
    }
}