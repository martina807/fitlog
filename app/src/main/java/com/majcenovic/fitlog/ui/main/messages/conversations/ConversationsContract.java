package com.majcenovic.fitlog.ui.main.messages.conversations;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Conversation;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 07/08/2017.
 */

public interface ConversationsContract {

    interface View extends BaseView {
        void showConversations(List<Conversation> conversations);
        void showEmptyState();
        void hideEmptyState();
        void showChat(User friend);
    }

    interface Presenter extends BasePresenter {
        void loadConversations();
        void openChat(Conversation item, Long myUserId);
    }
}
