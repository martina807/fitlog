package com.majcenovic.fitlog.ui.main.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseFragment;
import com.majcenovic.fitlog.ui.comments.CommentsActivity;
import com.majcenovic.fitlog.ui.main.home.search.SearchActivity;
import com.majcenovic.fitlog.ui.profile.ProfileActivity;
import com.majcenovic.fitlog.ui.profile.view_holders.StatusBoardViewHolder;
import com.majcenovic.fitlog.ui.status_editor.StatusEditorActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;

/**
 * Created by martina on 28/06/2017.
 */

public class HomeFragment extends BaseFragment implements HomeContract.View {

    public static final String TAG = "HomeFragment";

    @BindView(R.id.rvList)
    RecyclerView rvList;

    @BindView(R.id.srlList)
    SwipeRefreshLayout srlList;

    @BindView(R.id.tvCaloriesRemainingNumber)
    TextView tvCaloriesRemainingNumber;

    @BindView(R.id.tvCaloriesRemainingText)
    TextView tvCaloriesRemainingText;

    @BindView(R.id.tvHomeEmptyState)
    TextView tvHomeEmptyState;

    private HomeContract.Presenter presenter;
    private HomeAdapter adapter;
    private Bundle savedInstanceState;
    private User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        this.savedInstanceState = savedInstanceState;

        presenter = Injection.provideHomePresenter(this, context);
        user = getActivity().getIntent().getParcelableExtra(ExtraUtil.USER);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ActivityCompat.invalidateOptionsMenu(getActivity());
        rvList.setLayoutManager(new LinearLayoutManager(context));
        srlList.setColorSchemeResources(R.color.accent);
        srlList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadNews();
            }
        });
        presenter.loadCaloriesStatus();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                startActivity(new Intent(getActivity(), SearchActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (adapter != null) {
            adapter.setSavedInstanceState(savedInstanceState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.loadNews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.onResume();
        }
    }

    @Override
    public void onPause() {
        if (adapter != null) {
            adapter.onPause();
        }
        super.onPause();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if (!hidden) {
            presenter.loadNews();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void showProgress() {
        srlList.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        srlList.setRefreshing(false);
    }

    @Override
    public void showNews(List<Post> posts) {
        setUpAdapter(posts);
    }

    @Override
    public void showStatusEditor() {
        startActivity(new Intent(BaseApplication.appContext, StatusEditorActivity.class));
    }

    @Override
    public void showCaloriesStatus(CaloriesStatus caloriesStatus) {
        tvCaloriesRemainingNumber.setText(String.format(Locale.getDefault(), "%.0f", caloriesStatus.getCaloriesRemaining()));
        tvCaloriesRemainingText.setText(getString(R.string.calories_remained));
    }

    @Override
    public void showUser(User user) {
        startActivity(new Intent(getActivity(), ProfileActivity.class)
                .putExtra(ExtraUtil.USER, user));
    }

    @Override
    public void refreshData(Post post) {
        adapter.updatePost(post);
    }

    @Override
    public void showComments(Post post) {
        startActivity(new Intent(getActivity(), CommentsActivity.class)
                .putExtra(ExtraUtil.OBJECT, post));
    }

    private void setUpAdapter(List<Post> posts) {
        if (adapter == null) {
            adapter = new HomeAdapter(user, setStatusClickListener(), setProfileClickListener());
        }

        adapter.setSavedInstanceState(savedInstanceState);
        if (rvList.getAdapter() == null) {
            rvList.setAdapter(adapter);
        }
        adapter.addPosts(posts);
        adapter.notifyDataSetChanged();

        if (posts != null && !posts.isEmpty()) {
            tvHomeEmptyState.setVisibility(View.GONE);
        } else {
            tvHomeEmptyState.setVisibility(View.VISIBLE);
        }
    }

    private HomeAdapter.ProfileClickListener setProfileClickListener() {
        return new HomeAdapter.ProfileClickListener() {
            @Override
            public void onUserClick(User user) {
                presenter.openUser(user);
            }

            @Override
            public void onLikeDislikeClick(Post post) {
                presenter.likeOrDislike(post);
            }

            @Override
            public void onCommentsClick(Post post) {
                presenter.openComments(post);
            }
        };
    }

    private StatusBoardViewHolder.StatusClickListener setStatusClickListener() {
        return new StatusBoardViewHolder.StatusClickListener() {
            @Override
            public void onStatusClick() {
                presenter.openStatusEditor();
            }
        };
    }
}
