package com.majcenovic.fitlog.ui.profile.edit;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.UpdateUserDto;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.util.DateUtil;
import com.majcenovic.fitlog.util.ImageUtil;
import com.majcenovic.fitlog.util.PermissionUtil;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mvc.imagepicker.ImagePicker;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by martina on 09/09/2017.
 */

public class EditProfileActivity extends BaseActivity implements EditProfileContract.View {

    private EditProfileContract.Presenter presenter;
    private File photo;

    @BindView(R.id.etFirstName)
    EditText etFirstName;

    @BindView(R.id.etLastName)
    EditText etLastName;

    @BindView(R.id.etDescription)
    EditText etDescription;

    @BindView(R.id.etBirthday)
    EditText etBirthday;

    @BindView(R.id.etHeight)
    EditText etHeight;

    @BindView(R.id.rbMale)
    RadioButton rbMale;

    @BindView(R.id.rbFemale)
    RadioButton rbFemale;

    @BindView(R.id.rivEditPhoto)
    RoundedImageView rivEditPhoto;

    private String selectedDateString;
    private Drawable maleDrawable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar(getString(R.string.edit_profile));

        maleDrawable = ContextCompat.getDrawable(context, R.drawable.male);
        maleDrawable.setColorFilter(ContextCompat.getColor(context, R.color.primary), PorterDuff.Mode.SRC_ATOP);
        rbMale.setCompoundDrawablesWithIntrinsicBounds(maleDrawable, null, null, null);
        presenter = Injection.provideEditProfilePresenter(this, context);
        presenter.loadProfile();
    }

    @Override
    protected void onStart() {
        super.onStart();
        hideKeyboard(context);
    }

    @OnCheckedChanged(R.id.rbMale)
    void maleChanged(boolean isChecked) {
        rbFemale.setChecked(!isChecked);
    }

    @OnCheckedChanged(R.id.rbFemale)
    void femaleChanged(boolean isChecked) {
        rbMale.setChecked(!isChecked);
    }

    @Override
    public void showProfile(User user) {
        if (user.getProfilePicture() != null) {
            Picasso.with(context).load(user.getProfilePicture()).into(rivEditPhoto);
        }
        etFirstName.setText(user.getFirstName());
        etLastName.setText(user.getLastName());
        if (user.getBirthday() != null) {
            etBirthday.setText(DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.US)
                    .format(DateUtil.convertToJodaDate(user.getBirthday()).toDate()));
        }
        if (user.getHeight() != null) {
            etHeight.setText(String.valueOf(user.getHeight()));
        }
        if (user.getDescription() != null) {
            etDescription.setText(user.getDescription());
        }
        setGender(user);
    }

    private void setGender(User user) {
        String gender = user.getGender();

        if (gender.equals("male")) {
            rbMale.setChecked(true);
            rbFemale.setChecked(false);
        } else {
            rbFemale.setChecked(true);
            rbMale.setChecked(false);
        }
    }

    @OnClick(R.id.btnUpdate)
    public void onUpdateClick() {
        UpdateUserDto updateUserDto;
        if (isInputsValid()) {
            updateUserDto = new UpdateUserDto(
                    etFirstName.getText().toString().trim(),
                    etLastName.getText().toString().trim(),
                    Integer.parseInt(etHeight.getText().toString().trim()),
                    etDescription.getText().toString().trim(),
                    getGender(),
                    selectedDateString,
                    photo
            );
            presenter.updateProfile(updateUserDto);
        } else {
            showPopUp(R.string.all_inputs_required_except_description);
        }
    }

    @OnClick(R.id.vBirthdayOverlay)
    void birthdayOverlayClick() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        DateTime dt = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
                        selectedDateString = String.format(Locale.US, "%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                        etBirthday.setText(DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.US).format(dt.toDate()));
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.showYearPickerFirst(true);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    private boolean isInputsValid() {
        String birthday = etBirthday.getText().toString().trim();
        String height = etHeight.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String firstName = etFirstName.getText().toString().trim();

        if(!birthday.isEmpty() && !height.isEmpty() && !lastName.isEmpty() && !firstName.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private String getGender() {
        if (rbMale.isChecked()) {
            return "male";
        } else {
            return "female";
        }
    }

    @Override
    public void showProfileUpdated(User user) {
        finish();
    }

    @OnClick(R.id.rivEditPhoto)
    void onEditPhotoClick() {
        presenter.pickImageClick(PermissionUtil.isStorageGranted(context));
    }

    @Override
    public void showImagePicker() {
        ImagePicker.pickImage((Activity) context);
    }

    @Override
    public void showRequestPermissions() {
        PermissionUtil.requestStoragePermissions(context);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.STORAGE_PERMISSIONS_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            presenter.pickImageClick(PermissionUtil.isStorageGranted(context));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = ImagePicker.getImageFromResult(context, requestCode, resultCode, data);
            photo = ImageUtil.convertBitmapToFile(context, bitmap);
            rivEditPhoto.setImageBitmap(bitmap);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_profile;
    }
}