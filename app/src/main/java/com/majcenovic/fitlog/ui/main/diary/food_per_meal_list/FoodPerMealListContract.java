package com.majcenovic.fitlog.ui.main.diary.food_per_meal_list;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.data.models.UserFood;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface FoodPerMealListContract {

    interface View extends BaseView {
        void showFoodList(List<UserFood> userFoodList);
        void showEmptyState();
        void removeFood(UserFood userFood);
    }

    interface Presenter extends BasePresenter {
        void loadFoodList(int dayOffset, Meal meal);
        void onDeleteFoodClick(UserFood userFood);
    }
}
