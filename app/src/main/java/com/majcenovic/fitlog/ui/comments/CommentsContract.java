package com.majcenovic.fitlog.ui.comments;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Comment;
import com.majcenovic.fitlog.data.models.Post;

import java.util.List;

/**
 * Created by martina on 21/06/2017.
 */

public interface CommentsContract {

    interface View extends BaseView {
        void showComments(List<Comment> commentList);
        void refreshCommentList();
        void refreshComment(int position);
        void clearInputAndHideKeyboard();
    }

    interface Presenter extends BasePresenter {
        void loadComments(Post post);
        void postComment(Post post, String comment);
        void likeDislikeComment(Comment comment);
    }
}
