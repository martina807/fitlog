package com.majcenovic.fitlog.ui.profile.people;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public interface PeopleContract {

    interface View extends BaseView {
        void showPeople(List<User> people);
        void showUserProfile(User user);
        void updateUserRow();
    }

    interface Presenter extends BasePresenter {
        void loadPeople(Long userId);
        void startUserProfile(User user);
        void follow(User user);
        void unfollow(User user);
    }
}