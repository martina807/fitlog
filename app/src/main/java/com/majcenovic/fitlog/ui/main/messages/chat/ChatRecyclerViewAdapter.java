package com.majcenovic.fitlog.ui.main.messages.chat;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Space;
import android.widget.TextView;

import com.majcenovic.fitlog.BaseRecyclerViewAdapter;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.Message;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.util.DateUtil;
import com.majcenovic.fitlog.util.ListRowTypeUtils;
import com.majcenovic.fitlog.util.PhotoUtils;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vedran on 29/04/2017.
 */

public class ChatRecyclerViewAdapter extends BaseRecyclerViewAdapter<ChatRecyclerViewAdapter.ViewHolder> {

    private User friend;

    public ChatRecyclerViewAdapter(Context context, List itemList, User friend) {
        super(context, itemList);
        this.friend = friend;
    }

    @Override
    public int getItemViewType(int position) {
        BaseModel model = (BaseModel) itemList.get(position);

        if (model instanceof Message) {
            if (((Message) model).getSenderId().equals(friend.getId())) {
                return ListRowTypeUtils.CHAT_OTHER_CONTENT;
            } else {
                return ListRowTypeUtils.CHAT_ME_CONTENT;
            }
        }

        return model.getListType();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        super.onCreateViewHolder(parent, viewType);
        Integer layoutId = null;
        switch (viewType) {
            case ListRowTypeUtils.CHAT_ME_CONTENT:
                layoutId = R.layout.row_chat_message_right_row;
                break;
            case ListRowTypeUtils.CHAT_OTHER_CONTENT:
                layoutId = R.layout.row_chat_message_left_row;
                break;
            case ListRowTypeUtils.EMPTY:
                layoutId = R.layout.row_no_content;
                break;
        }
        return new ViewHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        BaseModel model = (BaseModel) itemList.get(position);

        switch (getItemViewType(position)) {
            case ListRowTypeUtils.CHAT_ME_CONTENT:
                setupContent(holder, position, model);
                break;
            case ListRowTypeUtils.CHAT_OTHER_CONTENT:
                setupContent(holder, position, model);
                break;
            case ListRowTypeUtils.EMPTY:
                setupEmpty(holder, position, model);
                break;
        }
    }

    private void setupContent(ViewHolder holder, int position, BaseModel m) {
        Message model = (Message) m;

        holder.tvMessage.setText(model.getMessage());

        String formattedDate = DateUtil.getTimeBeforeFormatted(context, model.getCreatedAt());
        holder.tvCreatedAt.setVisibility(View.VISIBLE);
        holder.emptySpace.setVisibility(View.GONE);
        holder.tvCreatedAt.setText(formattedDate);

        if (position == itemList.size() - 1) {
            model.setHeader(true);
        } else if (getItemViewType(position + 1) != getItemViewType(position)) {
            model.setHeader(true);
        } else {
            model.setHeader(false);
        }

        switch (getItemViewType(position)) {
            case ListRowTypeUtils.CHAT_ME_CONTENT:
                if (model.isHeader()) {
                    holder.flContentHolder.setBackgroundResource(R.drawable.chat_hangouts_grey_9_patch);
                } else {
                    holder.flContentHolder.setBackgroundResource(R.drawable.chat_hangouts_full_rounded_grey_9_patch);
                }
                break;
            case ListRowTypeUtils.CHAT_OTHER_CONTENT:
                if (model.isHeader()) {
                    holder.flContentHolder.setBackgroundResource(R.drawable.chat_hangouts_white_9_patch);
                    holder.rivPhoto.setVisibility(View.VISIBLE);
                } else {
                    holder.flContentHolder.setBackgroundResource(R.drawable.chat_hangouts_full_rounded_white_9_patch);
                    holder.rivPhoto.setVisibility(View.INVISIBLE);
                }
                if (holder.rivPhoto != null) {
                    PhotoUtils.loadProfilePhoto(context, friend.getProfilePicture(), holder.rivPhoto);
                }
                break;
        }
    }

    private void setupEmpty(ViewHolder holder, int position, BaseModel m) {
        holder.tvNoContent.setText(context.getString(R.string.no_content));
    }

    public void setFriend(User friend) {
        this.friend = friend;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.tvMessage)
        TextView tvMessage;

        @Nullable
        @BindView(R.id.tvCreatedAt)
        TextView tvCreatedAt;

        @Nullable
        @BindView(R.id.emptySpace)
        Space emptySpace;

        @Nullable
        @BindView(R.id.rivPhoto)
        RoundedImageView rivPhoto;

        @Nullable
        @BindView(R.id.flContentHolder)
        FrameLayout flContentHolder;

        @Nullable
        @BindView(R.id.tvNoContent)
        TextView tvNoContent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}