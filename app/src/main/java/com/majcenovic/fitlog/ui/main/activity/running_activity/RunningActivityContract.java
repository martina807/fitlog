package com.majcenovic.fitlog.ui.main.activity.running_activity;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.dto.ActivityDto;

/**
 * Created by martina on 02/08/2017.
 */

public interface RunningActivityContract {

    interface View extends BaseView {
        void showPositionOnMap();
        void showActivityCreated(Activity activity);
        void showActivityFragment();
        void removeActivityFromSharedPrefs();
        void showActivityInProgressLayout();
        void showActivityNotInProgressLayout();
    }

    interface Presenter extends BasePresenter {
        void createActivity(String activityType);
        void stopActivity(ActivityDto activity);
        void getLayoutSetup(Activity activitySharedPrefs);
    }
}
