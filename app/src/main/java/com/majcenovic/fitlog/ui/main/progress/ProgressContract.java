package com.majcenovic.fitlog.ui.main.progress;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;

/**
 * Created by martina on 09/08/2017.
 */

public class ProgressContract {

    public interface View extends BaseView {
        void showCurrentWeight(Double currentWeight);
    }

    public interface Presenter extends BasePresenter {
        void loadGoals();
    }
}
