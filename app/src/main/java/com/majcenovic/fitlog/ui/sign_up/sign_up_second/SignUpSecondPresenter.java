package com.majcenovic.fitlog.ui.sign_up.sign_up_second;

import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.util.InputValidationUtil;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpSecondPresenter implements SignUpSecondContract.Presenter {

    private SignUpSecondContract.View view;

    public SignUpSecondPresenter(SignUpSecondContract.View view) {
        this.view = view;
    }

    @Override
    public void cancel() {

    }

    @Override
    public void submitNext(SignUpDto signUpDto) {
        if (InputValidationUtil.isBirthdayValid(signUpDto.getBirthday())
                && InputValidationUtil.isHeightValid(signUpDto.getHeight())
                && InputValidationUtil.isWeightValid(signUpDto.getCurrentWeight())) {

            view.showSignUpThird(signUpDto);
        } else {
            view.showInvalidInput();
        }
    }
}