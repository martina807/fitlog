package com.majcenovic.fitlog.ui.main.activity.running_activity;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.UpdateMapActivityEvent;
import com.majcenovic.fitlog.data.models.dto.UpdateLocationDto;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.util.ApiErrorUtil;
import com.majcenovic.fitlog.util.DistanceCalculatorUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 02/08/2017.
 */

public class GpsLocationListener implements android.location.LocationListener {

    private Location lastLocation;
    private static final String TAG = "GpsListener";
    public Activity activity;
    public ActivityEndCallback callback;

    private float distance = 0.0f;
    List<Location> locations = new ArrayList<>();

    public GpsLocationListener(String provider, Activity activity, ActivityEndCallback callback) {
        this.activity = activity;
        Log.e(TAG, "GpsLocationListener " + provider);
        lastLocation = new Location(provider);
        this.callback = callback;
    }

    public void cancel() {
        activity = null;
        if (callback != null) {
            callback.onEnded();
        }
    }

    public interface ActivityEndCallback {
        void onEnded();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (activity == null) {
            callback.onEnded();
            return;
        }

        Log.e(TAG, "onLocationChanged: " + location);
        lastLocation.set(location);
        final UpdateLocationDto gpsLocation = new UpdateLocationDto(lastLocation.getLatitude(), lastLocation.getLongitude());
        locations.add(location);

        ApiManager.getApiService(BaseApplication.appContext).postLocation(activity.getId(), gpsLocation.getLatitude(), gpsLocation.getLongitude()).enqueue(new Callback<GpsLocation>() {
            @Override
            public void onResponse(Call<GpsLocation> call, Response<GpsLocation> response) {
                if (activity == null) return;

                if (response.isSuccessful()) {
                    Toast.makeText(BaseApplication.appContext, "Location saved", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(BaseApplication.appContext, ApiErrorUtil.getMessage(BaseApplication.appContext, response), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GpsLocation> call, Throwable t) {
                Toast.makeText(BaseApplication.appContext, ApiErrorUtil.getMessage(BaseApplication.appContext, t), Toast.LENGTH_LONG).show();
            }
        });

        SharedPrefs.saveActivityLocation(gpsLocation);
        List<UpdateLocationDto> updateLocationDtos = SharedPrefs.readActivityLocations();
        distance += DistanceCalculatorUtil.getTraveledDistance(updateLocationDtos);
        EventBus.getDefault().post(new UpdateMapActivityEvent(distance, gpsLocation));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.e(TAG, "onStatusChanged: " + provider);

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.e(TAG, "onProviderDisabled: " + provider);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.e(TAG, "onProviderEnabled: " + provider);
    }

}
