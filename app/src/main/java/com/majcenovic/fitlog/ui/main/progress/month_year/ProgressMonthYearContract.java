package com.majcenovic.fitlog.ui.main.progress.month_year;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Progress;

import java.util.Date;
import java.util.List;

/**
 * Created by martina on 08/08/2017.
 */

public interface ProgressMonthYearContract {

    interface View extends BaseView {
        void setupChart(List<Progress> progresses);
        void showKgLostSince(Double lostKg, String date);
        void showEmpty();
        void hideEmpty();
        void onProgressShared();
    }

    interface Presenter extends BasePresenter {
        void loadProgressForPeriod(String period);
        void shareProgress(List<Progress> progresses, String progressViewType);
    }
}
