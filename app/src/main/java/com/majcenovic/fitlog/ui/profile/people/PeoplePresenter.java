package com.majcenovic.fitlog.ui.profile.people;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.people.PeopleInteractor;
import com.majcenovic.fitlog.data.people.PeopleListener;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public class PeoplePresenter implements PeopleContract.Presenter {

    private PeopleContract.View view;
    private PeopleInteractor interactor;

    public PeoplePresenter(PeopleContract.View view, PeopleInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadPeople(Long userId) {
        view.showProgress();
        interactor.getPeople(userId, new PeopleListener() {
            @Override
            public void onPeopleLoaded(List<User> people) {
                view.hideProgress();
                view.showPeople(people);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void startUserProfile(User user) {
        view.showUserProfile(user);
    }

    @Override
    public void follow(final User user) {
        view.showProgress();
        interactor.follow(user.getId(), new PeopleListener.FollowCallback() {
            @Override
            public void onFollow() {
                view.hideProgress();
                user.setFollowing(true);
                view.updateUserRow();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void unfollow(final User user) {
        view.showProgress();
        interactor.unfollow(user.getId(), new PeopleListener.UnfollowCallback() {
            @Override
            public void onUnfollow() {
                view.hideProgress();
                user.setFollowing(false);
                view.updateUserRow();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }
}