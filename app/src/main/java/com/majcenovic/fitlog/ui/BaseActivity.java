package com.majcenovic.fitlog.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.UpdatePushTokensRequest;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.goals.GoalsActivity;
import com.majcenovic.fitlog.ui.log_in.LogInActivity;
import com.majcenovic.fitlog.ui.main.MainActivity;
import com.majcenovic.fitlog.ui.main.home.HomeFragment;
import com.majcenovic.fitlog.ui.profile.ProfileActivity;
import com.majcenovic.fitlog.util.ApiErrorUtil;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.onesignal.OneSignal;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 26/06/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    @BindView(R.id.flBaseProgressContainer)
    FrameLayout flBaseProgressContainer;

    @BindView(R.id.navigationView)
    protected NavigationView navigationView;

    @BindView(R.id.drawerLayout)
    protected DrawerLayout drawerLayout;

    protected Context context;
    protected Toolbar toolbar;
    protected User user;
    protected ActionBar supportActionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutId());

        ButterKnife.bind(this);

        user = SharedPrefs.readUserData(this);

        context = this;

        checkForPushTokens();

        initNavigationView();
    }

    protected void hideParentToolbar() {
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setVisibility(View.GONE);
        }
    }

    protected void setStatusBarWhiteColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(context, R.color.status_bar_white));
        }
    }

    private void checkForPushTokens() {

        boolean isLoggedIn = user != null;

        if (isLoggedIn) OneSignal.setSubscription(true);

        if (!SharedPrefs.isPushTokenSent(context) && isLoggedIn) {
            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                @Override
                public void idsAvailable(String onesignalId, String registrationId) {

                    String deviceId = null;
                    if (getContentResolver() != null) {
                        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    }

                    Log.d("Device ID", deviceId + "");
                    Log.d("OneSignal ID", onesignalId + "");
                    Log.d("GoogleReg ID", registrationId + "");
                    Log.d("AppUserId", registrationId + "");


                    ApiManager.getApiService(context).updatePushTokens(
                            new UpdatePushTokensRequest(
                                    onesignalId,
                                    registrationId,
                                    deviceId
                            ).toMap())
                            .enqueue(new Callback<BaseModel>() {
                                @Override
                                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                                    if (response.isSuccessful()) {
                                        SharedPrefs.setPushTokenSent(context);
                                    } else {
                                        showPopUp(ApiErrorUtil.getMessage(context, response));
                                    }
                                }

                                @Override
                                public void onFailure(Call<BaseModel> call, Throwable t) {
                                    showPopUp(ApiErrorUtil.getMessage(context, t));
                                }
                            });
                }
            });
        } else {
            Log.d("PushRegister", "Push is already registered");
        }
    }


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View rootView = getLayoutInflater().inflate(R.layout.activity_base, null);
        ViewGroup container = (ViewGroup) rootView.findViewById(R.id.flBaseContentContainer);
        getLayoutInflater().inflate(layoutResID, container, true);
        super.setContentView(rootView);
    }

    protected abstract int getLayoutId();

    @Override
    public void showError(String message) {
        showPopUp(message);
    }

    @Override
    public void showProgress() {
        flBaseProgressContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        flBaseProgressContainer.setVisibility(View.GONE);
    }

    protected void showPopUp(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    protected void showPopUp(@StringRes int resString) {
        showPopUp(getString(resString));
    }

    protected void initToolbar(int toolbarTitle) {
        initToolbar(getString(toolbarTitle));
    }

    protected void initToolbar(int toolbarTitle, @DrawableRes int resIcon) {
        initToolbar(toolbarTitle);
        supportActionBar = getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(resIcon);
        }
    }

    protected void initToolbar(Drawable resIcon) {
        initToolbar("");
        supportActionBar = getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(resIcon);
        }
    }

    protected void initToolbar(String toolbarTitle) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setVisibility(View.VISIBLE);
            supportActionBar = getSupportActionBar();

            if (supportActionBar != null) {
                supportActionBar.setTitle(toolbarTitle);
                supportActionBar.setDisplayHomeAsUpEnabled(true);
                supportActionBar.setHomeButtonEnabled(true);
            }
        }
    }

    private void initNavigationView() {
        View headerView = navigationView.getHeaderView(0);

        TextView name = (TextView) headerView.findViewById(R.id.tvDrawerName);
        TextView email = (TextView) headerView.findViewById(R.id.tvDrawerEmail);
        ImageView ivProfilePhoto = (ImageView) headerView.findViewById(R.id.ivHeaderProfileImage);

        if (user != null) {
            name.setText(user.getFullName());
            email.setText(user.getEmail());
            if (user.getProfilePicture() != null) {
                Picasso.with(context).load(user.getProfilePicture()).into(ivProfilePhoto);
            }

            ivProfilePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), ProfileActivity.class).putExtra(ExtraUtil.USER, user));
                    drawerLayout.closeDrawers();
                }
            });

            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.drawerLogout:
                            SharedPrefs.clear(context);
                            startActivity(new Intent(getApplicationContext(), LogInActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            break;
                        case R.id.drawerGoals:
                            if (context instanceof GoalsActivity) {
                                drawerLayout.closeDrawers();
                            } else {
                                startActivity(new Intent(getApplicationContext(), GoalsActivity.class));
                            }
                            break;
                        case R.id.drawerHome:
                            if(context instanceof MainActivity && getSupportFragmentManager().findFragmentByTag(HomeFragment.TAG).isVisible()) {
                                drawerLayout.closeDrawers();
                            } else {
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }
                            break;
                    }
                    drawerLayout.closeDrawers();
                    return true;
                }
            });
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    public static void hideKeyboard(Context context) {
        try {
            ((AppCompatActivity) context).getWindow().setSoftInputMode(WindowManager
                    .LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            if ((((AppCompatActivity) context).getCurrentFocus() != null)
                    && (((AppCompatActivity) context).getCurrentFocus().getWindowToken() != null)) {
                ((InputMethodManager) context
                        .getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(((AppCompatActivity) context).getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawers();
        } else {
            if (this instanceof MainActivity && drawerLayout != null) {
                drawerLayout.openDrawer(Gravity.START);
            } else {
                onBackPressed();
            }
        }
        return true;
    }

}
