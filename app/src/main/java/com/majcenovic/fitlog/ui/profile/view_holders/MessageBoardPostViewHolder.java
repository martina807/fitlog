package com.majcenovic.fitlog.ui.profile.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 14/08/2017.
 */

public class MessageBoardPostViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.rivProfileImage)
    RoundedImageView rivProfileImage;

    @BindView(R.id.tvMessageSenderRecipient)
    TextView tvMessageSenderRecipient;

    @BindView(R.id.tvBoardDateTime)
    TextView tvBoardDateTime;

    @BindView(R.id.tvBoardMessageDesc)
    TextView tvBoardMessageDesc;

    @BindView(R.id.ibBoardLike)
    ImageButton ibBoardLike;

    @BindView(R.id.ibBoardComment)
    ImageButton ibBoardComment;

    @BindView(R.id.tvNoOfBoardLikes)
    TextView tvNoOfBoardLikes;

    @BindView(R.id.tvBoardNoOfComments)
    TextView tvBoardNoOfComments;

    public MessageBoardPostViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public RoundedImageView getRivProfileImage() {
        return rivProfileImage;
    }

    public TextView getTvMessageSenderRecipient() {
        return tvMessageSenderRecipient;
    }

    public TextView getTvBoardDateTime() {
        return tvBoardDateTime;
    }

    public TextView getTvBoardMessageDesc() {
        return tvBoardMessageDesc;
    }

    public ImageButton getIbBoardLike() {
        return ibBoardLike;
    }

    public ImageButton getIbBoardComment() {
        return ibBoardComment;
    }

    public TextView getTvNoOfBoardLikes() {
        return tvNoOfBoardLikes;
    }

    public TextView getTvBoardNoOfComments() {
        return tvBoardNoOfComments;
    }
}
