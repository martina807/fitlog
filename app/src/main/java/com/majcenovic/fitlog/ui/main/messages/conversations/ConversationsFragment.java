package com.majcenovic.fitlog.ui.main.messages.conversations;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.majcenovic.fitlog.BaseRecyclerViewAdapter;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Conversation;
import com.majcenovic.fitlog.data.models.MessageEvent;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseFragment;
import com.majcenovic.fitlog.ui.main.messages.chat.ChatActivity;
import com.majcenovic.fitlog.ui.main.messages.friends.FriendsActivity;
import com.majcenovic.fitlog.ui.profile.people.PeopleActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by martina on 28/06/2017.
 */

public class ConversationsFragment extends BaseFragment implements ConversationsContract.View {

    @BindView(R.id.rvList)
    RecyclerView recyclerView;

    @BindView(R.id.srlList)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.tvConversationsEmpty)
    TextView tvConversationsEmpty;

    public static final String TAG = "ConversationsFragment";
    private ConversationsContract.Presenter presenter;
    private ConversationsRecyclerViewAdapter recyclerViewAdapter;
    private List<Conversation> listItems = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_conversations;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = Injection.provideConversationPresenter(this, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
    }

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            presenter.loadConversations();
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        presenter.loadConversations();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final MessageEvent event) {
        if (event.getOpenLocationId() != null) {
            presenter.loadConversations();
        }
    }

    @Override
    public void showError(String message) {
        showPopUp(message);
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showConversations(List<Conversation> conversations) {
        listItems.clear();
        listItems.addAll(conversations);
        if (recyclerViewAdapter == null) {
            recyclerViewAdapter = new ConversationsRecyclerViewAdapter(context, listItems, user);
            recyclerViewAdapter.setOnItemClickListener(onItemClickListener);
        } else {
            recyclerViewAdapter.notifyDataSetChanged();
        }
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    @Override
    public void showEmptyState() {
        recyclerView.setVisibility(View.GONE);
        tvConversationsEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyState() {
        recyclerView.setVisibility(View.VISIBLE);
        tvConversationsEmpty.setVisibility(View.GONE);
    }

    @Override
    public void showChat(User friend) {
        startActivity(new Intent(context, ChatActivity.class)
                .putExtra(ExtraUtil.OBJECT, friend));
    }

    private BaseRecyclerViewAdapter.OnItemClickListener<Conversation> onItemClickListener = new BaseRecyclerViewAdapter.OnItemClickListener<Conversation>() {
        @Override
        public void onItemClick(Conversation item) {
            presenter.openChat(item, user.getId());
        }
    };

    @OnClick(R.id.fabAdd)
    public void onAddConversationClick() {
        startActivity(new Intent(getActivity().getApplicationContext(), FriendsActivity.class));
    }
}