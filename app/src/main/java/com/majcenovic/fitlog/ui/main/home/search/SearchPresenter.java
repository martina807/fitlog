package com.majcenovic.fitlog.ui.main.home.search;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.search.SearchInteractor;
import com.majcenovic.fitlog.data.search.SearchListener;

import java.util.Collections;
import java.util.List;

/**
 * Created by martina on 09/09/2017.
 */

public class SearchPresenter implements SearchContract.Presenter {

    private final SearchContract.View view;
    private final SearchInteractor interactor;

    public SearchPresenter(SearchContract.View view, SearchInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void search(String input) {
        if (!input.trim().isEmpty()) {
            view.showProgress();
            interactor.searchUsers(input.trim(), new SearchListener.PeopleCallback() {
                @Override
                public void onPeopleLoaded(List<User> users) {
                    view.hideProgress();
                    view.showPeople(users);
                }

                @Override
                public void onFailure(String message) {
                    view.hideProgress();
                    view.showError(message);
                }
            });
        } else {
            view.showPeople(Collections.<User>emptyList());
        }
    }

    @Override
    public void onBackPressed(String input) {
        if (input.trim().isEmpty()) {
            view.showDefaultBackBehavior();
        } else {
            view.clearInput();
        }
    }

    @Override
    public void openUserDetails(User user) {
        view.showUserDetails(user);
    }

    @Override
    public void followUnfollow(final User user) {
        if (user.isFollowing()) {
            interactor.unfollow(user, new SearchListener.UnfollowCallback() {
                @Override
                public void onUnfollow() {
                    user.setFollowing(false);
                    view.updateFollowState(user);
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        } else {
            interactor.follow(user, new SearchListener.FollowCallback() {
                @Override
                public void onFollow() {
                    user.setFollowing(true);
                    view.updateFollowState(user);
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        }
    }
}