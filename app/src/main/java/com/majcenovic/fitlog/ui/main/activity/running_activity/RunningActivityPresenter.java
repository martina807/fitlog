package com.majcenovic.fitlog.ui.main.activity.running_activity;

import com.majcenovic.fitlog.data.activities.RunningActivityInteractor;
import com.majcenovic.fitlog.data.activities.RunningActivityListener;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.dto.ActivityDto;

/**
 * Created by martina on 02/08/2017.
 */

public class RunningActivityPresenter implements RunningActivityContract.Presenter {

    private RunningActivityContract.View view;
    private RunningActivityInteractor interactor;
    private LocationService locationService;

    public RunningActivityPresenter(RunningActivityContract.View view, RunningActivityInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void createActivity(String activityType) {
        view.showProgress();
        interactor.createActivity(activityType.toLowerCase(), new RunningActivityListener.CreateActivityListener() {
            @Override
            public void onActivityCreated(Activity activity) {
                if (activity != null) {
                    view.hideProgress();
                    view.showActivityCreated(activity);
                }
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
            }
        });
    }

    @Override
    public void stopActivity(final ActivityDto activity) {
        view.showProgress();
        interactor.postFinishedActivity(activity.getActivityId(),
                activity.getDistance(),
                activity.getDuration(),
                activity.getAvgSpeed(),
                activity.getTopSpeed(),
                new RunningActivityListener.FinishActivityListener() {
                    @Override
                    public void onFinishedActivityPosted() {
                        view.hideProgress();
                        stopLocationTracking();
                        view.removeActivityFromSharedPrefs();
                    }

                    @Override
                    public void onFailure(String message) {
                        view.hideProgress();
                        view.showError(message);
                    }
                });
    }

    @Override
    public void getLayoutSetup(Activity activitySharedPrefs) {
        if(activitySharedPrefs != null) view.showActivityInProgressLayout();
        else view.showActivityNotInProgressLayout();
    }

    private void stopLocationTracking() {
        view.showActivityFragment();
    }
}