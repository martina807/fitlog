package com.majcenovic.fitlog.ui.profile.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 14/08/2017.
 */

public class ButtonViewHolder extends RecyclerView.ViewHolder {

    private ButtonsClickListener buttonsClickListener;

    @BindView(R.id.btnPhotos)
    Button btnPhotos;

    @BindView(R.id.btnPeople)
    Button btnPeople;

    public ButtonViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        btnPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonsClickListener.onPhotosClick();
            }
        });

        btnPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonsClickListener.onPeopleClick();
            }
        });
    }

    public interface ButtonsClickListener {
        void onPhotosClick();
        void onPeopleClick();
    }

    public void setButtonsClickListener(ButtonsClickListener buttonsClickListener) {
        this.buttonsClickListener = buttonsClickListener;
    }
}
