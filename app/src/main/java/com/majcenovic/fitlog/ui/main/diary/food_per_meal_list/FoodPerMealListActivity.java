package com.majcenovic.fitlog.ui.main.diary.food_per_meal_list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.data.models.UserFood;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by martina on 07/09/2017.
 */

public class FoodPerMealListActivity extends BaseActivity implements FoodPerMealListContract.View {

    @BindView(R.id.rvFoodPerMealList)
    RecyclerView rvFoodPerMealList;

    @BindView(R.id.tvEmptyState)
    TextView tvEmptyState;

    private FoodPerMealListAdapter adapter;
    private List<UserFood> listItems = new ArrayList<>();
    private FoodPerMealListContract.Presenter presenter;
    private Integer dayOffset;
    private Meal meal;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_food_per_meal_list;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rvFoodPerMealList.setLayoutManager(new LinearLayoutManager(context));

        dayOffset = getIntent().getExtras().getInt(ExtraUtil.DAY_OFFSET);
        meal = getIntent().getParcelableExtra(ExtraUtil.SHOW_FOOD_PER_MEAl);

        initToolbar(meal.getTitle() + " ingredients");

        presenter = Injection.provideFoodPerMealListPresenter(this, context);
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.loadFoodList(dayOffset, meal);
    }

    @Override
    public void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }

    @Override
    public void showFoodList(List<UserFood> userFoodList) {
        rvFoodPerMealList.setVisibility(View.VISIBLE);
        tvEmptyState.setVisibility(View.GONE);
        listItems.clear();
        listItems.addAll(userFoodList);
        if (adapter == null) {
            adapter = new FoodPerMealListAdapter(context, listItems);
            rvFoodPerMealList.setAdapter(adapter);
            adapter.setOnDeleteFoodClickListener(onDeleteFoodClickListener);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void removeFood(UserFood userFood) {
        if (listItems == null) {
            return;
        }

        if (listItems.contains(userFood)) {
            int foodPosition = listItems.indexOf(userFood);
            listItems.remove(userFood);
            adapter.notifyItemRemoved(foodPosition);
            adapter.notifyItemRangeChanged(foodPosition, listItems.size());
        }
    }

    @Override
    public void showEmptyState() {
        rvFoodPerMealList.setVisibility(View.GONE);
        tvEmptyState.setVisibility(View.VISIBLE);
    }

    private FoodPerMealListAdapter.OnDeleteFoodClickListener onDeleteFoodClickListener = new FoodPerMealListAdapter.OnDeleteFoodClickListener() {
        @Override
        public void onClick(UserFood food, int position) {
            presenter.onDeleteFoodClick(food);
        }
    };

}
