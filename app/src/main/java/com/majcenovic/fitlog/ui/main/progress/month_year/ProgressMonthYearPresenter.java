package com.majcenovic.fitlog.ui.main.progress.month_year;

import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.models.dto.PostDto;
import com.majcenovic.fitlog.data.progress.progress_month_year.ProgressMonthYearInteractor;
import com.majcenovic.fitlog.data.progress.ProgressListener;

import java.util.List;

/**
 * Created by martina on 08/08/2017.
 */

public class ProgressMonthYearPresenter implements ProgressMonthYearContract.Presenter {

    ProgressMonthYearContract.View view;
    ProgressMonthYearInteractor interactor;

    public ProgressMonthYearPresenter(ProgressMonthYearContract.View view, ProgressMonthYearInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadProgressForPeriod(String period) {
        view.showProgress();
        interactor.getProgressForPeriod(period, new ProgressListener.ProgressMonthYearListener() {
            @Override
            public void onProgressLoaded(List<Progress> progresses) {
                view.hideProgress();
                if (progresses != null && !progresses.isEmpty()) {
                    view.setupChart(progresses);
                    view.hideEmpty();
                    getLostWeightSinceLastUpdate(progresses);
                } else {
                    view.showEmpty();
                }
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
            }
        });
    }

    @Override
    public void shareProgress(List<Progress> progresses, String progressViewType) {
        view.showProgress();
        PostDto postDto = new PostDto(progressViewType,
                progresses.get(0).getId(),
                progresses.get(progresses.size() - 1).getId());
        interactor.postProgress(postDto, new ProgressListener.PostProgress() {
            @Override
            public void onProgressPosted() {
                view.hideProgress();
                view.onProgressShared();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    public void getLostWeightSinceLastUpdate(List<Progress> progresses) {
        int size = progresses.size();

        Double currentWeight = progresses.get(size - 1).getWeight();
        Double lastCurrentWeight = progresses.get(0).getWeight();
        String lastCurrentWeightDate = progresses.get(0).getCreatedAt();

        view.showKgLostSince(currentWeight - lastCurrentWeight, lastCurrentWeightDate);
    }
}
