package com.majcenovic.fitlog.ui.main.progress.month_year;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.models.UpdateProgressEvent;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseFragment;
import com.majcenovic.fitlog.util.ChartUtil;
import com.majcenovic.fitlog.util.DateUtil;
import com.majcenovic.fitlog.util.ExtraUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 08/08/2017.
 */

@SuppressWarnings("deprecation")
public class ProgressMonthYearFragment extends BaseFragment implements ProgressMonthYearContract.View {

    @BindView(R.id.tvEmptyProgressState)
    TextView tvEmptyProgressState;

    @BindView(R.id.tvKgLostSince)
    TextView tvKgLostSince;

    @BindView(R.id.chartProgress)
    LineChart chartProgress;

    @BindView(R.id.flProgressChartContainer)
    FrameLayout flProgressChartContainer;

    @BindView(R.id.ibProgressShare)
    ImageButton ibProgressShare;

    List<Progress> progresses;

    private String progressViewType;
    private ProgressMonthYearContract.Presenter presenter;

    public static ProgressMonthYearFragment newInstance(String viewType) {
        Bundle args = new Bundle();
        args.putString(ExtraUtil.PROGRESS_VIEW_TYPE, viewType);
        ProgressMonthYearFragment fragment = new ProgressMonthYearFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.tab_view_progress;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = Injection.provideProgressMonthYearPresenter(this, context);

        progressViewType = getArguments().getString(ExtraUtil.PROGRESS_VIEW_TYPE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        presenter.loadProgressForPeriod(progressViewType);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateProgressEvent(final UpdateProgressEvent event) {
        if(progressViewType != null && presenter != null) {
            presenter.loadProgressForPeriod(progressViewType);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void setupChart(List<Progress> progresses) {
        this.progresses = progresses;
        ChartUtil.setChartProgress(chartProgress, progresses, context);
    }

    @Override
    public void showKgLostSince(Double weightDifference, String date) {
        String niceDateFormat = DateUtil.toNiceMediumDateNoYearFormat(date);
        if(weightDifference > 0) {
            //noinspection deprecation
            tvKgLostSince.setText(Html.fromHtml(String.format(getString(R.string.kg_gained_since),
                    weightDifference, niceDateFormat)));
        } else {
            Double absoluteValue = java.lang.Math.abs(weightDifference);
            tvKgLostSince.setText(Html.fromHtml(String.format(getString(R.string.kg_lost_since),
                    absoluteValue, niceDateFormat)));
        }
    }

    @Override
    public void showEmpty() {
        flProgressChartContainer.setVisibility(View.GONE);
        tvEmptyProgressState.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmpty() {
        flProgressChartContainer.setVisibility(View.VISIBLE);
        tvEmptyProgressState.setVisibility(View.GONE);
    }

    @Override
    public void onProgressShared() {
        showPopUp(R.string.progress_shared);
    }

    @OnClick(R.id.ibProgressShare)
    public void onProgressShareClick() {
        presenter.shareProgress(progresses, progressViewType);
    }
}
