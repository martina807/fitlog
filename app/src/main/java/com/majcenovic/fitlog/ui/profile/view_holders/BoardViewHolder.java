package com.majcenovic.fitlog.ui.profile.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.majcenovic.fitlog.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 14/08/2017.
 */

public class BoardViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvBoardTitle)
    TextView tvBoard;

    public BoardViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getTvBoard() {
        return tvBoard;
    }
}
