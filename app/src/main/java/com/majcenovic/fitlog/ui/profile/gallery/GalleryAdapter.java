package com.majcenovic.fitlog.ui.profile.gallery;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Photo;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 30/06/2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private Context context;
    private List<Post> list = new ArrayList<>();
    private PostClickListener postClickListener;

    public GalleryAdapter(Context context, PostClickListener postClickListener) {
        this.context = context;
        this.postClickListener = postClickListener;
    }

    public void updateData(List<Post> photoPosts) {
        this.list.clear();

        if (photoPosts == null) {
            notifyDataSetChanged();
            return;
        }

        this.list.addAll(photoPosts);
        notifyDataSetChanged();
    }

    public interface PostClickListener {

        void onPhotoClick(Photo photo);

        void onLikeDislikeClick(Post post);

        void onCommentsClick(Post post);

    }

    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_item_photo_grid, parent, false));
    }

    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder holder, int position) {
        final Photo photo = getAllPhotos(list).get(position);
        Picasso.with(context).load(photo.getPhotoUrl()).into(holder.ivGalleryImage);
        holder.tvBoardNoOfComments.setText(String.valueOf(photo.getCommentCount()));
        holder.tvBoardNoOfLikes.setText(String.valueOf(photo.getLikesCount()));

        if (photo.isLiked()) {
            holder.ibBoardLike.setImageResource(R.drawable.like_filled);
        } else {
            holder.ibBoardLike.setImageResource(R.drawable.like);
        }
    }

    @Override
    public int getItemCount() {
        return getAllPhotos(list).size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivGalleryImage)
        ImageView ivGalleryImage;

        @BindView(R.id.tvBoardNoOfComments)
        TextView tvBoardNoOfComments;

        @BindView(R.id.tvBoardNoOfLikes)
        TextView tvBoardNoOfLikes;

        @BindView(R.id.ibBoardLike)
        ImageButton ibBoardLike;

        @BindView(R.id.ibBoardComment)
        ImageButton ibBoardComment;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ivGalleryImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postClickListener.onPhotoClick(getAllPhotos(list).get(getAdapterPosition()));
                }
            });

            ibBoardComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postClickListener.onCommentsClick(getAllPhotos(list).get(getAdapterPosition()).getPost());
                }
            });

            ibBoardLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postClickListener.onLikeDislikeClick(getAllPhotos(list).get(getAdapterPosition()).getPost());
                }
            });
        }
    }

    interface OnImageClickListener {

        void onClick(Photo photo, int position);

    }

    private List<Photo> getAllPhotos(List<Post> list) {
        List<Photo> photos = new ArrayList<>();
        Post currentPost;

        for (int i = 0; i < list.size(); i++) {
            currentPost = list.get(i);
            for (int j = 0; j < currentPost.getPhotos().size(); j++) {
                photos.add(new Photo(
                        currentPost,
                        currentPost.getPhotos().get(j).getPhotoUrl(),
                        currentPost.getMessage(),
                        currentPost.getTags(),
                        currentPost.getCommentCount(),
                        currentPost.getLikesCount(),
                        currentPost.isLiked()));

            }
        }
        return photos;
    }
}
