package com.majcenovic.fitlog.ui.sign_up.sign_up_third;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.Spinner;

import com.majcenovic.fitlog.BuildConfig;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.data.sign_up.SignUpInteractor;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.main.MainActivity;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.majcenovic.fitlog.util.SpinnerUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpThirdActivity extends BaseActivity implements SignUpThirdContract.View {

    @BindView(R.id.spSignUpActivityLevel)
    Spinner spSignUpActivityLevel;

    @BindView(R.id.spSignUpWeeklyGoal)
    Spinner spSignUpWeeklyGoal;

    @BindView(R.id.etSignUpGoalWeight)
    EditText etSignUpGoalWeight;

    private SignUpThirdContract.Presenter presenter;
    private SignUpInteractor interactor;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_up_third;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            spSignUpActivityLevel.getChildAt(1);
            spSignUpWeeklyGoal.getChildAt(1);
            etSignUpGoalWeight.setText("75");
        }

        initToolbar(R.string.sign_up);

        SpinnerUtil.populateWeeklyGoal(context, spSignUpWeeklyGoal);
        SpinnerUtil.populateActivityLevelsSp(context, spSignUpActivityLevel);
        presenter = Injection.provideSignUpThirdPresenter(this, context);
    }

    @Override
    public void showInvalidInput() {
        showPopUp(getString(R.string.invalid_input));
    }

    @Override
    public void showMain() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    @OnClick(R.id.btnSignUpFinish)
    public void FinishClick() {
        SignUpDto signUpDto = getIntent().getParcelableExtra(ExtraUtil.SIGN_UP_DTO);
        signUpDto.setActivityLevel(String.valueOf(spSignUpActivityLevel.getSelectedItemPosition() + 1));
        signUpDto.setWeeklyGoalWeight(String.valueOf(spSignUpWeeklyGoal.getSelectedItemPosition() + 1));
        signUpDto.setGoalWeight(Double.parseDouble(etSignUpGoalWeight.getText().toString()));

        presenter.submitSignUp(signUpDto);
    }
}
