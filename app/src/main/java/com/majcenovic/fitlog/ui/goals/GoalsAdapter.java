package com.majcenovic.fitlog.ui.goals;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.models.Meal;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 30/06/2017.
 */

public class GoalsAdapter extends RecyclerView.Adapter<GoalsAdapter.ViewHolder> {

    final int CURRENT_WEIGHT_POSITION = 0;
    final int WEEKLY_GOAL_POSITION = 1;
    final int FINAL_GOAL_POSITION = 2;
    final int ACTIVITY_LEVEL_POSITION = 3;

    final int LIST_SIZE = 4;

    private Context context;
    private Goal goal;
    private final GoalClickListener goalClickListener;

    public GoalsAdapter(Context context, Goal goal, GoalClickListener goalClickListener) {
        this.context = context;
        this.goal = goal;
        this.goalClickListener = goalClickListener;
    }

    public void updateData(Goal goal) {
        this.goal = goal;
        notifyDataSetChanged();
    }

    public interface GoalClickListener {
        void onCurrentWeightClick(Goal goal);

        void onWeeklyGoalClick(Goal goal);

        void onFinalGoalClick(Goal goal);

        void onActivityLevelClick(Goal goal);
    }

    @Override
    public GoalsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_meals, parent, false));
    }

    @Override
    public void onBindViewHolder(GoalsAdapter.ViewHolder holder, int position) {

        holder.ibMealAdd.setImageResource(R.drawable.edit);
        holder.ivBurnIcon.setVisibility(View.GONE);

        switch (position) {
            case CURRENT_WEIGHT_POSITION:
                holder.tvMealsMealTitle.setText(context.getString(R.string.current_weight));
                if (goal.getCurrentWeight() != null) {
                    holder.tvCaloriesPerMeal.setText(String.valueOf(goal.getCurrentWeight()));
                } else {
                    holder.tvCaloriesPerMeal.setText("");
                }
                holder.ibMealAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goalClickListener.onCurrentWeightClick(goal);
                    }
                });
                break;
            case WEEKLY_GOAL_POSITION:
                holder.tvMealsMealTitle.setText(context.getString(R.string.weekly_goal));
                if (goal.getWeeklyGoal() != null) {
                    holder.tvCaloriesPerMeal.setText(String.valueOf(goal.getWeeklyGoal()));
                } else {
                    holder.tvCaloriesPerMeal.setText("");
                }
                holder.ibMealAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goalClickListener.onWeeklyGoalClick(goal);
                    }
                });
                break;
            case FINAL_GOAL_POSITION:
                holder.tvMealsMealTitle.setText(context.getString(R.string.final_goal));
                if (goal.getFinalGoal() != null) {
                    holder.tvCaloriesPerMeal.setText(String.valueOf(goal.getFinalGoal()));
                } else {
                    holder.tvCaloriesPerMeal.setText("");
                }
                holder.ibMealAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goalClickListener.onFinalGoalClick(goal);
                    }
                });
                break;
            case ACTIVITY_LEVEL_POSITION:
                holder.tvMealsMealTitle.setText(context.getString(R.string.activity_level));
                if (goal.getActivityLevel() != null) {
                    holder.tvCaloriesPerMeal.setText(getTitleForActivityType());
                } else {
                    holder.tvCaloriesPerMeal.setText("");
                }
                holder.ibMealAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goalClickListener.onActivityLevelClick(goal);
                    }
                });
                break;
        }
    }

    private String getTitleForActivityType() {
        if(goal != null && goal.getActivityLevel() != null) {
            switch (goal.getActivityLevel()) {
                case "not_active":
                    return context.getString(R.string.not_active);
                case "slightly_active":
                    return context.getString(R.string.slightly_active);
                case "active":
                    return context.getString(R.string.active);
                case "very_active":
                    return context.getString(R.string.very_active);
                case "extremely_active":
                    return context.getString(R.string.extremely_active);
            }
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return goal != null ? LIST_SIZE : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvRowTitle)
        TextView tvMealsMealTitle;

        @BindView(R.id.tvRowDescription)
        TextView tvCaloriesPerMeal;

        @BindView(R.id.ibRowButton)
        ImageButton ibMealAdd;

        @BindView(R.id.ivBurnIcon)
        ImageView ivBurnIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}