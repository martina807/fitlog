package com.majcenovic.fitlog.ui.main.progress.update_current_weight;

import com.majcenovic.fitlog.data.progress.ProgressListener;
import com.majcenovic.fitlog.data.progress.update_current_weight.UpdateGoalInteractor;

/**
 * Created by martina on 09/08/2017.
 */

public class UpdateGoalPresenter implements UpdateGoalContract.Presenter {

    private UpdateGoalContract.View view;
    private UpdateGoalInteractor interactor;

    public UpdateGoalPresenter(UpdateGoalContract.View view, UpdateGoalInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void postCurrentWeight(Double currentWeight) {
        view.showProgress();
        interactor.postCurrentWeight(currentWeight, new ProgressListener.UpdateCurrentWeightListener() {
            @Override
            public void onCurrentWeightPosted() {
                view.hideProgress();
                view.showCurrentWeightUpdated();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void postFinalGoal(Double finalGoal) {
        view.showProgress();
        interactor.postFinalGoal(finalGoal, new ProgressListener.UpdateFinalGoalListener() {
            @Override
            public void onFinalGoalPosted() {
                view.hideProgress();
                view.showFinalGoalUpdated();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void postWeeklyGoal(Double weeklyGoal) {
        view.showProgress();
        interactor.postWeeklyGoal(weeklyGoal, new ProgressListener.UpdateWeeklyGoalListener() {
            @Override
            public void onWeeklyGoalPosted() {
                view.hideProgress();
                view.showWeeklyGoalUpdated();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void postActivityLevel(String activityLevel) {
        view.showProgress();
        interactor.postActivityLevel(activityLevel, new ProgressListener.UpdateActivityLevelListener() {
            @Override
            public void onActivityLevelPosted() {
                view.hideProgress();
                view.showActivityLevelUpdated();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }
}