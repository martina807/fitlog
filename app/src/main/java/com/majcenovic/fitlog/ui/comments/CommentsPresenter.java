package com.majcenovic.fitlog.ui.comments;

import com.majcenovic.fitlog.data.comments.CommentsInteractor;
import com.majcenovic.fitlog.data.comments.CommentsListener;
import com.majcenovic.fitlog.data.models.Comment;
import com.majcenovic.fitlog.data.models.Post;

import java.util.List;

/**
 * Created by martina on 21/06/2017.
 */

public class CommentsPresenter implements CommentsContract.Presenter {

    private CommentsContract.View view;
    private CommentsInteractor interactor;
    private List<Comment> commentsData;

    public CommentsPresenter(CommentsContract.View view, CommentsInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadComments(final Post post) {
        view.showProgress();
        interactor.loadComments(post, new CommentsListener() {
            @Override
            public void onCommentsLoaded(List<Comment> commentList) {
                view.hideProgress();
                commentsData = commentList;
                view.clearInputAndHideKeyboard();
                view.showComments(commentList);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void postComment(final Post post, String comment) {
        view.showProgress();
        interactor.postComment(post, comment, new CommentsListener.PostComment() {
            @Override
            public void onCommentPosted(Comment comment) {
                view.hideProgress();
                loadComments(post);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void likeDislikeComment(final Comment comment) {
        if (comment.isLiked()) {
            interactor.dislikeComment(comment, new CommentsListener.DislikeComment() {
                @Override
                public void onCommentDisliked() {
                    comment.setLiked(!comment.isLiked());
                    comment.decrementLikes();
                    if (commentsData.contains(comment)) {
                        view.refreshComment(commentsData.indexOf(comment));
                    }
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        } else {
            interactor.likeComment(comment, new CommentsListener.LikeComment() {
                @Override
                public void onCommentLiked() {
                    comment.setLiked(!comment.isLiked());
                    comment.incrementLikes();
                    if (commentsData.contains(comment)) {
                        view.refreshComment(commentsData.indexOf(comment));
                    }
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        }
    }
}