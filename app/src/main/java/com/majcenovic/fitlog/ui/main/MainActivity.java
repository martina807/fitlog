package com.majcenovic.fitlog.ui.main;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.BaseFragment;
import com.majcenovic.fitlog.ui.main.activity.ActivityFragment;
import com.majcenovic.fitlog.ui.main.diary.meals.MealsFragment;
import com.majcenovic.fitlog.ui.main.home.HomeFragment;
import com.majcenovic.fitlog.ui.main.messages.conversations.ConversationsFragment;
import com.majcenovic.fitlog.ui.main.progress.ProgressFragment;

import butterknife.BindDimen;
import butterknife.BindView;

/**
 * Created by martina on 21/06/2017.
 */

public class MainActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @BindView(R.id.bnvMainNavigation)
    BottomNavigationView bnvMainNavigation;

    @BindDimen(R.dimen.toolbar_default_elevation)
    float toolbarDefaultElevation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar(R.string.home, R.drawable.ic_menu_24dp);

        bnvMainNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelected);

        if (savedInstanceState == null) {
            showInitialFragment();
        } else {

        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelected = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.itemMainHome:
                    switchToFragment(HomeFragment.TAG);
                    initToolbar(R.string.home, R.drawable.ic_menu_24dp);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        toolbar.setElevation(toolbarDefaultElevation);
                    }
                    break;
                case R.id.itemMainDiary:
                    switchToFragment(MealsFragment.TAG);
                    initToolbar(R.string.diary, R.drawable.ic_menu_24dp);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        toolbar.setElevation(toolbarDefaultElevation);
                    }
                    break;
                case R.id.itemMainActivity:
                    switchToFragment(ActivityFragment.TAG);
                    initToolbar(R.string.activity, R.drawable.ic_menu_24dp);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        toolbar.setElevation(toolbarDefaultElevation);
                    }
                    break;
                case R.id.itemMainProgress:
                    switchToFragment(ProgressFragment.TAG);
                    initToolbar(R.string.progress, R.drawable.ic_menu_24dp);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        toolbar.setElevation(0);
                    }
                    break;
                case R.id.itemMainInbox:
                    switchToFragment(ConversationsFragment.TAG);
                    initToolbar(R.string.inbox, R.drawable.ic_menu_24dp);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        toolbar.setElevation(toolbarDefaultElevation);
                    }
                    break;
            }
            return true;
        }
    };

    private void showInitialFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        HomeFragment fragment = new HomeFragment();
        fragmentTransaction.add(R.id.fragmentContainer, fragment, HomeFragment.TAG);
        fragmentTransaction.commit();
    }

    private void switchToFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        BaseFragment fragment = (BaseFragment) fragmentManager.findFragmentByTag(tag);

        if (fragment != null && !fragment.isHidden()) return;

        if (fragment == null) {
            switch (tag) {
                case HomeFragment.TAG:
                    fragment = new HomeFragment();
                    break;
                case MealsFragment.TAG:
                    fragment = new MealsFragment();
                    break;
                case ActivityFragment.TAG:
                    fragment = new ActivityFragment();
                    break;
                case ProgressFragment.TAG:
                    fragment = new ProgressFragment();
                    break;
                case ConversationsFragment.TAG:
                    fragment = new ConversationsFragment();
                    break;
            }
            fragmentTransaction.add(R.id.fragmentContainer, fragment, tag);
        }

        //noinspection RestrictedApi
        if (fragmentManager.getFragments() != null && fragmentManager.getFragments().size() > 0) {
            //noinspection RestrictedApi
            for (Fragment f : fragmentManager.getFragments()) {
                if (!f.isHidden()) {
                    fragmentTransaction.hide(f);
                }
            }
        }

        fragmentTransaction.show(fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
