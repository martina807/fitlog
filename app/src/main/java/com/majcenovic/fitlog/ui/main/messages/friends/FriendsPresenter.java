package com.majcenovic.fitlog.ui.main.messages.friends;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.people.PeopleInteractor;
import com.majcenovic.fitlog.data.people.PeopleListener;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public class FriendsPresenter implements FriendsContract.Presenter {

    private FriendsContract.View view;
    private PeopleInteractor interactor;

    public FriendsPresenter(FriendsContract.View view, PeopleInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadPeople(Long userId) {
        view.showProgress();
        interactor.getPeople(userId, new PeopleListener() {
            @Override
            public void onPeopleLoaded(List<User> people) {
                view.hideProgress();
                view.showPeople(people);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void startChat(User user) {
        view.showChat(user);
    }
}