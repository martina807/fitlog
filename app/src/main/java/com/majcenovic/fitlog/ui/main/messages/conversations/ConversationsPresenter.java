package com.majcenovic.fitlog.ui.main.messages.conversations;

import com.majcenovic.fitlog.data.inbox.ConversationsInteractor;
import com.majcenovic.fitlog.data.inbox.InboxListener;
import com.majcenovic.fitlog.data.models.Conversation;

import java.util.List;

/**
 * Created by martina on 07/08/2017.
 */

public class ConversationsPresenter implements ConversationsContract.Presenter {

    private ConversationsContract.View view;
    private ConversationsInteractor interactor;

    public ConversationsPresenter(ConversationsContract.View view, ConversationsInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadConversations() {
        view.showProgress();
        interactor.loadConversations(new InboxListener.ConversationsListener() {
            @Override
            public void onConversationsLoaded(List<Conversation> conversations) {
                view.hideProgress();
                if (conversations != null && !conversations.isEmpty()) {
                    view.hideEmptyState();
                    view.showConversations(conversations);
                } else {
                    view.showEmptyState();
                }
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void openChat(Conversation conversation, Long myUserId) {
        if (conversation != null && conversation.getReceiver() != null && !conversation.getReceiver().getId().equals(myUserId)) {
            view.showChat(conversation.getReceiver());
        } else {
            view.showChat(conversation.getSender());
        }
    }
}