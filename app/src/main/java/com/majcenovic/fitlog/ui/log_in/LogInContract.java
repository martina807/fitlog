package com.majcenovic.fitlog.ui.log_in;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;

/**
 * Created by martina on 21/06/2017.
 */

public interface LogInContract {

    interface View extends BaseView {
        void showMain();
        void showInvalidInput();
        void showSignUp();
    }

    interface Presenter extends BasePresenter {
        void submitLogIn(String email, String password);
        void startSignUp();
    }
}
