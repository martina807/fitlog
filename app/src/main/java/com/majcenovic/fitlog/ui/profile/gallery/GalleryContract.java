package com.majcenovic.fitlog.ui.profile.gallery;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Photo;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public interface GalleryContract {

    interface View extends BaseView {
        void showPhoto(Photo photo);
        void showComments(Post post);
        void showPhotosFromPosts(List<Post> posts);
        void refreshData();
    }

    interface Presenter extends BasePresenter {
        void openPhoto(Photo photo);
        void likeDislikePost(Post post);
        void openComments(Post post);
        void loadPhotoPosts(User user);
    }
}