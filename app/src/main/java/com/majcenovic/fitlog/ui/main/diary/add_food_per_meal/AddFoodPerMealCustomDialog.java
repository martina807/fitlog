package com.majcenovic.fitlog.ui.main.diary.add_food_per_meal;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Food;
import com.majcenovic.fitlog.data.models.dto.AddFoodPerMealDto;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.util.DateUtil;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 05/07/2017.
 */

public class AddFoodPerMealCustomDialog extends DialogFragment implements AddFoodPerMealContract.View {

    private static AddFoodPerMealCallback callback;
    private AddFoodPerMealContract.Presenter presenter;

    public static String TAG = "ADD_FOOD_PER_MEAL_CUSTOM";

    @BindView(R.id.tvAutoCompleteAddFoodIngredient)
    AutoCompleteTextView tvAutoCompleteAddFoodIngredient;

    @BindView(R.id.etAddFoodAmount)
    EditText etAddFoodAmount;


    private AlertDialog alertDialog;
    private Integer mealType;
    private ArrayList<String> allFoodNames;
    private List<Food> allFood;
    private Long selectedFoodId;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = Injection.provideAddFoodPerMealPresenter(this, getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();

        Button buttonPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        buttonPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fieldsFilled()) {
                    AddFoodPerMealDto addFoodPerMealDto = new AddFoodPerMealDto(
                            Integer.parseInt(etAddFoodAmount.getText().toString()),
                            mealType,
                            selectedFoodId,
                            DateUtil.getCurrentDateTime());

                    presenter.addFoodPerMeal(addFoodPerMealDto);
                }
            }
        });
    }

    public static AddFoodPerMealCustomDialog newInstance(Integer mealType, AddFoodPerMealCallback callback) {
        AddFoodPerMealCustomDialog.callback = callback;
        AddFoodPerMealCustomDialog fragment = new AddFoodPerMealCustomDialog();
        fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert);
        Bundle args = new Bundle();
        args.putInt(ExtraUtil.MEAL_TYPE, mealType);
        fragment.setArguments(args);
        return fragment;
    }

    public interface AddFoodPerMealCallback {
        void onFoodAdded();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View contentView = getActivity().getLayoutInflater().inflate(R.layout.custom_alert_dialog_add_food_per_meal, null);
        ButterKnife.bind(this, contentView);

        mealType = getArguments().getInt(ExtraUtil.MEAL_TYPE);

        tvAutoCompleteAddFoodIngredient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.loadAllFoodByKeyword(s.toString());
            }
        });


        alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.add_ingredient))
                .setView(contentView)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.add), null)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create();

        return alertDialog;
    }

    private boolean fieldsFilled() {

        boolean allFieldsFilled = true;

        String ingredient = tvAutoCompleteAddFoodIngredient.getText().toString();

        Integer amount = null;
        try {
            amount = Integer.parseInt(etAddFoodAmount.getText().toString());
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }

        if (ingredient.trim().length() == 0 || amount == null || selectedFoodId == null) {
            return !allFieldsFilled;
        }

        return allFieldsFilled;
    }

    @Override
    public void showFoodAdded() {
        dismiss();
        callback.onFoodAdded();
    }

    @Override
    public void showAllFoodLoadedAutoComplete(final List<Food> allFood) {
        this.allFood = allFood;

        allFoodNames = new ArrayList<>();

        if (allFood != null) {
            for (Food item : allFood) {
                allFoodNames.add(item.getTitle());
            }
        }

        //Creating the instance of ArrayAdapter containing list of fruit names
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item, allFoodNames);
        tvAutoCompleteAddFoodIngredient.setThreshold(1);//will start working from first character
        tvAutoCompleteAddFoodIngredient.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        tvAutoCompleteAddFoodIngredient.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));

        tvAutoCompleteAddFoodIngredient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > -1) {
                    if(allFood != null) {
                        selectedFoodId = allFood.get(position).getId();
                    }
                } else {
                    selectedFoodId = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedFoodId = null;
            }
        });

        tvAutoCompleteAddFoodIngredient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position > -1) {
                    if(allFood != null) {
                        selectedFoodId = allFood.get(position).getId();
                    }
                } else {
                    selectedFoodId = null;
                }
            }
        });

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}