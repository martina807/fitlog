package com.majcenovic.fitlog.ui.main.progress.update_current_weight;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;

/**
 * Created by martina on 09/08/2017.
 */

public interface UpdateGoalContract {

    interface View extends BaseView {
        void showCurrentWeightUpdated();
        void showFinalGoalUpdated();
        void showWeeklyGoalUpdated();
        void showActivityLevelUpdated();
    }

    interface Presenter extends BasePresenter {
        void postCurrentWeight(Double currentWeight);
        void postFinalGoal(Double finalGoal);
        void postWeeklyGoal(Double weeklyGoal);
        void postActivityLevel(String activityLevel);
    }
}
