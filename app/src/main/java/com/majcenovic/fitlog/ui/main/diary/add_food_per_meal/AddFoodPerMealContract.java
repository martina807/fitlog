package com.majcenovic.fitlog.ui.main.diary.add_food_per_meal;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Food;
import com.majcenovic.fitlog.data.models.dto.AddFoodPerMealDto;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface AddFoodPerMealContract {

    interface View extends BaseView {
        void showFoodAdded();
        void showAllFoodLoadedAutoComplete(List<Food> allFood);
    }

    interface Presenter extends BasePresenter {
        void addFoodPerMeal(AddFoodPerMealDto addFoodPerMealDto);

        void loadAllFoodByKeyword(String keyword);
    }
}
