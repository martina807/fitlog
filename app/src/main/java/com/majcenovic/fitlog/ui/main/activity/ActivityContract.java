package com.majcenovic.fitlog.ui.main.activity;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Activity;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface ActivityContract {

    interface View extends BaseView {
        void showActivities(List<Activity> activities);
        void showRunningActivity(String activityType);
        void showDayTitle(String s);
        void showEmptyState();
        void hideEmptyState();
        void showActivityIsInProgressState();
        void showActivityNotInProgressState();
        void showActivityPosted();
    }

    interface Presenter extends BasePresenter {
        void loadActivities(Integer dayOffset);
        void startRunningActivity(String activityType);
        void onShareClick(Activity activity);
        void setLayoutDependingOnActivityInProgress(Activity activity);
    }
}
