package com.majcenovic.fitlog.ui.profile.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 14/08/2017.
 */

public class StatusBoardViewHolder extends RecyclerView.ViewHolder {

    private StatusClickListener statusClickListener;

    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.ibSend)
    ImageButton btnSend;

    @BindView(R.id.llStatus)
    LinearLayout llStatus;
    @BindView(R.id.rivUserImage)
    RoundedImageView rivUserImage;

    public StatusBoardViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        llStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusClickListener.onStatusClick();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusClickListener.onStatusClick();
            }
        });
    }

    public StatusClickListener getStatusClickListener() {
        return statusClickListener;
    }

    public ImageButton getBtnSend() {
        return btnSend;
    }

    public TextView getTvStatus() {
        return tvStatus;
    }

    public RoundedImageView getRivUserImage() {
        return rivUserImage;
    }

    public interface StatusClickListener {
        void onStatusClick();
    }

    public void setStatusClickListener(StatusClickListener statusClickListener) {
        this.statusClickListener = statusClickListener;
    }
}