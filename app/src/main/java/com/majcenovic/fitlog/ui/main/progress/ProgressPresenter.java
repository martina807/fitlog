package com.majcenovic.fitlog.ui.main.progress;

import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.progress.ProgressInteractor;
import com.majcenovic.fitlog.data.progress.ProgressListener;

/**
 * Created by martina on 09/08/2017.
 */

public class ProgressPresenter implements ProgressContract.Presenter {

    private ProgressContract.View view;
    private ProgressInteractor interactor;

    public ProgressPresenter(ProgressContract.View view, ProgressInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadGoals() {
        view.showProgress();
        interactor.getGoals(new ProgressListener.GoalsListener() {
            @Override
            public void onGoalsLoaded(Goal goal) {
                view.showCurrentWeight(goal.getCurrentWeight());
            }

            @Override
            public void onFailure(String message) {
                view.showError(message);
            }
        });
    }
}
