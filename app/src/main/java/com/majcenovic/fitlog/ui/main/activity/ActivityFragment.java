package com.majcenovic.fitlog.ui.main.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.BaseFragment;
import com.majcenovic.fitlog.ui.main.activity.running_activity.LocationService;
import com.majcenovic.fitlog.ui.main.activity.running_activity.RunningActivityActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 28/06/2017.
 */

public class ActivityFragment extends BaseFragment implements ActivityContract.View {

    public static final String TAG = "ActivityFragment";

    @BindView(R.id.rvActivityContainer)
    RecyclerView rvActivityContainer;

    @BindView(R.id.btnNext)
    ImageButton btnNext;

    @BindView(R.id.fabMenu)
    FloatingActionsMenu fabMenu;

    @BindView(R.id.whiteOverlay)
    View whiteOverlay;

    @BindView(R.id.fabHolder)
    FrameLayout fabHolder;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvNoActivitiesFound)
    TextView tvNoActivitiesFound;

    @BindView(R.id.btnShowActivityInProgress)
    Button btnShowActivityInProgress;

    private int dayOffset;
    public static final int MAX_DAY_OFFSET = 0;
    private ActivityAdapter rvActivityAdapter;
    private List<com.majcenovic.fitlog.data.models.Activity> listItems = new ArrayList<>();

    private ActivityContract.Presenter presenter;
    private Activity activitySharedPrefs;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = Injection.provideActivityPresenter(this, context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvActivityContainer.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    public void onStart() {
        super.onStart();
        activitySharedPrefs = SharedPrefs.readActivityInProgressData(context);

        presenter.loadActivities(dayOffset);

        presenter.setLayoutDependingOnActivityInProgress(activitySharedPrefs);

        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                whiteOverlay.animate().alpha(0.94f).setDuration(200).start();
                whiteOverlay.setVisibility(View.VISIBLE);

                fabHolder.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        fabMenu.collapse();
                        return true;
                    }
                });
            }

            @Override
            public void onMenuCollapsed() {
                whiteOverlay.animate().alpha(0f).setDuration(200).start();
                whiteOverlay.setVisibility(View.GONE);
                fabHolder.setOnTouchListener(null);
            }
        });
    }

    @OnClick({R.id.fabRunning, R.id.fabCycling, R.id.fabWalking})
    public void onFabActivityClick(View view) {
        switch (view.getId()) {
            case R.id.fabRunning:
                presenter.startRunningActivity(context.getString(R.string.running));
                break;
            case R.id.fabCycling:
                presenter.startRunningActivity(context.getString(R.string.cycling));
                break;
            case R.id.fabWalking:
                presenter.startRunningActivity(context.getString(R.string.walking));
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_activity;
    }

    @Override
    public void showDayTitle(String s) {
        tvTitle.setText(s);
    }

    @Override
    public void showEmptyState() {
        rvActivityContainer.setVisibility(View.GONE);
        tvNoActivitiesFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyState() {
        tvNoActivitiesFound.setVisibility(View.GONE);
        rvActivityContainer.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.btnNext, R.id.btnPrev})
    public void onDayIndexChanged(View view) {
        if (R.id.btnPrev == view.getId()) dayOffset--;
        else dayOffset++;
        if (dayOffset >= MAX_DAY_OFFSET) dayOffset = MAX_DAY_OFFSET;
        presenter.loadActivities(dayOffset);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showActivities(List<com.majcenovic.fitlog.data.models.Activity> activities) {
        listItems.clear();
        listItems.addAll(activities);
        if (rvActivityAdapter == null) {
            rvActivityAdapter = new ActivityAdapter(context, listItems);
            rvActivityContainer.setAdapter(rvActivityAdapter);
            rvActivityAdapter.setOnShareClickListener(onShareClickListener);
        } else {
            rvActivityAdapter.notifyDataSetChanged();
        }
    }

    private ActivityAdapter.OnShareClickListener onShareClickListener = new ActivityAdapter.OnShareClickListener() {
        @Override
        public void onClick(Activity activity, int position) {
            presenter.onShareClick(activity);
        }
    };

    @OnClick(R.id.btnShowActivityInProgress)
    public void onShowActivityInProgressClick() {
        startActivity(new Intent(BaseApplication.appContext, RunningActivityActivity.class)
                .putExtra(ExtraUtil.ACTIVITY_TYPE, activitySharedPrefs.getType()));
    }

    @Override
    public void showRunningActivity(String activityType) {
        fabMenu.collapse();
        startActivity(new Intent(getActivity(), RunningActivityActivity.class).putExtra(ExtraUtil.ACTIVITY_TYPE, activityType));
    }

    @Override
    public void showActivityIsInProgressState() {
        fabHolder.setVisibility(View.GONE);
        btnShowActivityInProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void showActivityNotInProgressState() {
        fabHolder.setVisibility(View.VISIBLE);
        btnShowActivityInProgress.setVisibility(View.GONE);
    }

    @Override
    public void showActivityPosted() {
        showPopUp(R.string.activity_shared);
    }
}