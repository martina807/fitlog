package com.majcenovic.fitlog.ui.profile.gallery.photo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Photo;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.Tag;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.comments.CommentsActivity;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 07/09/2017.
 */

public class PhotoActivity extends BaseActivity implements PhotoContract.View {

    @BindView(R.id.tvPhotoDescription)
    TextView tvPhotoDescription;

    @BindView(R.id.tvBoardNoOfComments)
    TextView tvNoOfComments;

    @BindView(R.id.tvBoardNoOfLikes)
    TextView tvBoardNoOfLikes;

    @BindView(R.id.ibBoardLike)
    ImageButton ibBoardLike;

    @BindView(R.id.ibBoardComment)
    ImageButton ibBoardComment;

    @BindView(R.id.ivFullsizeImage)
    ImageView ivFullsizeImage;

    private Photo photo;
    private PhotoContract.Presenter presenter;
    private Post post;

    @Override
    protected int getLayoutId() {
        return R.layout.fullsize_item_photo_grid;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initToolbar(ContextCompat.getDrawable(context, R.drawable.ic_arrow_back_24dp));
        photo = getIntent().getParcelableExtra(ExtraUtil.PHOTO);
        post = getIntent().getParcelableExtra(ExtraUtil.POST);

        presenter = Injection.providePhotoPresenter(this, context);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setUpLayout();

        presenter.loadPost(post.getId());
    }

    private void setUpLayout() {
        Picasso.with(context).load(photo.getPhotoUrl()).into(ivFullsizeImage);

        tvPhotoDescription.setVisibility(View.VISIBLE);
        tvPhotoDescription.setText(Html.fromHtml(getDescription()));

        tvNoOfComments.setText(String.valueOf(post.getCommentCount()));
        tvBoardNoOfLikes.setText(String.valueOf(post.getLikesCount()));

        if (post.isLiked()) {
            ibBoardLike.setImageResource(R.drawable.like_filled);
        } else {
            ibBoardLike.setImageResource(R.drawable.like);
        }

        ibBoardLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.likeDislikePost(post);
            }
        });

        ibBoardComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openComments(post);
            }
        });
    }

    private String getDescription() {
        String desc = post.getMessage();
        String tagsFormatted = "";

        if (post.getTags() != null && !post.getTags().isEmpty()) {
            for (int i = 0; i < post.getTags().size(); i++) {

                tagsFormatted += "<font color='#2196F3'><b>" + post.getTags().get(i).getFullName() + "</b></font>";

                if (i < post.getTags().size() - 1) {
                    tagsFormatted += ", ";
                }
            }

            desc = desc + " " + getString(R.string.with) + " " + tagsFormatted;
        }

        return desc;
    }

    @Override
    public void showComments(Post post) {
        startActivity(new Intent(getApplicationContext(), CommentsActivity.class)
                .putExtra(ExtraUtil.OBJECT, post));
    }

    @Override
    public void refreshData() {
        setUpLayout();
    }

    @Override
    public void showPost(Post post) {
        this.post = post;
        refreshData();
    }

    @OnClick(R.id.ivFullsizeImage)
    public void onPhotoClick() {
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }
}