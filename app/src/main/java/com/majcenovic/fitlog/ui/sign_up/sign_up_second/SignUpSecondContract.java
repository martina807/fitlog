package com.majcenovic.fitlog.ui.sign_up.sign_up_second;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;

/**
 * Created by martina on 27/06/2017.
 */

public interface SignUpSecondContract {

    interface View extends BaseView {
        void showInvalidInput();
        void showSignUpThird(SignUpDto signUpDto);

    }

    interface Presenter extends BasePresenter {
        void submitNext(SignUpDto signUpDto);
    }
}
