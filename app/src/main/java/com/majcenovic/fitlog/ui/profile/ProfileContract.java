package com.majcenovic.fitlog.ui.profile;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public interface ProfileContract {

    interface View extends BaseView {
        void showBoardPostsAndUser(List<Post> postList, User user);
        void showStatusEditor();
        void showGalleryActivity(User user);
        void showPeopleActivity();
        void refreshData(Post post);
        void showCommentActivity(Post post);
        void showChat(User user);
        void showFollowChanged();
        void showEditProfile();
    }

    interface Presenter extends BasePresenter {
        void loadUserProfile(Long userId);
        void openStatusEditor();
        void startGalleryActivity(User user);
        void startPeopleActivity();
        void startCommentActivity(Post post);
        void likeDislikePost(Post post);
        void openChat(User user);
        void followUnfollow(User user);
        void openEditProfile();
    }
}
