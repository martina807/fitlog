package com.majcenovic.fitlog.ui.profile.gallery.photo;

import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.photos.PhotoInteractor;
import com.majcenovic.fitlog.data.photos.PhotoInteractorImpl;
import com.majcenovic.fitlog.data.photos.PhotoListener;

/**
 * Created by martina on 14/08/2017.
 */

public class PhotoPresenter implements PhotoContract.Presenter {

    private PhotoContract.View view;
    private final PhotoInteractor interactor;

    public PhotoPresenter(PhotoContract.View view, PhotoInteractorImpl interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
    }

    @Override
    public void likeDislikePost(final Post post) {
        if (post.isLiked()) {
            interactor.dislike(post, new PhotoListener.DislikeCallback() {
                @Override
                public void onDislikePosted() {
                    post.setLiked(!post.isLiked());
                    post.decrementLikes();
                    view.refreshData();
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        } else {
            interactor.like(post, new PhotoListener.LikeCallback() {
                @Override
                public void onLikePosted() {
                    post.setLiked(!post.isLiked());
                    post.incrementLikes();
                    view.refreshData();
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        }
    }

    @Override
    public void openComments(Post post) {
        view.showComments(post);
    }

    @Override
    public void loadPost(Long postId) {
        interactor.getPost(postId, new PhotoListener.PostCallback() {
            @Override
            public void onPostLoaded(Post post) {
                view.showPost(post);
            }

            @Override
            public void onFailure(String message) {
                view.showError(message);
            }
        });
    }
}