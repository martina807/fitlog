package com.majcenovic.fitlog.ui.log_in;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;

import com.majcenovic.fitlog.BuildConfig;
import com.majcenovic.fitlog.ui.main.MainActivity;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.sign_up.sign_up_first.SignUpFirstActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 21/06/2017.
 */

public class LogInActivity extends BaseActivity implements LogInContract.View {

    @BindView(R.id.etLogInEmail)
    EditText etLogInEmail;

    @BindView(R.id.etLogInPassword)
    EditText etLogInPassword;

    private LogInContract.Presenter presenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_log_in;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(BuildConfig.DEBUG) {
            etLogInEmail.setText("john@doe.com");
            etLogInPassword.setText("123456");
        }

        initToolbar(R.string.log_in);

        presenter = Injection.provideLogInPresenter(this, context);
    }

    @Override
    protected void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }

    @Override
    public void showMain() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void showInvalidInput() {
        showPopUp(R.string.invalid_input);
    }

    @Override
    public void showSignUp() {
        startActivity(new Intent(getApplicationContext(), SignUpFirstActivity.class));
    }

    @OnClick(R.id.btnLogInStartSignUp)
    void signUpClick() {
        presenter.startSignUp();
    }

    @OnClick(R.id.btnLogInSubmit)
    void logInClick() {
        presenter.submitLogIn(etLogInEmail.getText().toString(), etLogInPassword.getText().toString());
    }
}
