package com.majcenovic.fitlog.ui.main.home;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface HomeContract {

    interface View extends BaseView {
        void showNews(List<Post> posts);
        void showStatusEditor();
        void showCaloriesStatus(CaloriesStatus caloriesStatus);
        void showUser(User user);
        void showComments(Post post);
        void refreshData(Post post);
    }

    interface Presenter extends BasePresenter {
        void loadCaloriesStatus();
        void loadNews();
        void openStatusEditor();
        void openUser(User user);
        void likeOrDislike(Post post);
        void openComments(Post post);
    }
}