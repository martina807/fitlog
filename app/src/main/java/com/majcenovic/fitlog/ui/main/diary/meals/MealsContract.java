package com.majcenovic.fitlog.ui.main.diary.meals;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Food;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.data.models.dto.AddFoodPerMealDto;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface MealsContract {

    interface View extends BaseView {

        void showAddFood(Meal meal);
        void showFoodPerMeal(Meal meal);
        void showMealsLoaded(List<Meal> meals);
        void showDayTitle(String s);
        void showCaloriesStatus(CaloriesStatus caloriesStatus);
    }

    interface Presenter extends BasePresenter {
        void loadMeals(int dayOffset);
        void onMealAddClick(Meal meal);
        void loadCaloriesStatus();
        void onMealClick(Meal meal);
    }
}
