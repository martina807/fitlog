package com.majcenovic.fitlog.ui.status_editor;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.PostDto;
import com.majcenovic.fitlog.data.status_editor.profile.StatusEditorInteractor;
import com.majcenovic.fitlog.data.status_editor.profile.StatusListener;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public class StatusEditorPresenter implements StatusEditorContract.Presenter {

    StatusEditorContract.View view;
    StatusEditorInteractor interactor;

    public StatusEditorPresenter(StatusEditorContract.View view, StatusEditorInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }


    @Override
    public void postStatus(PostDto postDto) {
        if (postDto.isValid()) {
            view.showProgress();

            StatusListener.StatusEditorListener statusEditorListener = new StatusListener.StatusEditorListener() {
                @Override
                public void onStatusPosted() {
                    view.hideProgress();
                    view.showPreviousScreen();
                }

                @Override
                public void onFailure(String message) {
                    view.hideProgress();
                    view.showError(message);
                }
            };

            if(postDto.getFriendId() != null) {
                interactor.postPostToFriend(postDto, statusEditorListener);
            } else {
                interactor.postStatus(postDto, statusEditorListener);
            }
        }
    }

    @Override
    public void loadAllFriends(String keyword) {
        interactor.getAllUsersByKeyword(keyword, new StatusListener.AllUsersListener() {
            @Override
            public void onAllUsersLoaded(List<User> users) {
                view.showUserList(users);
            }

            @Override
            public void onFailure(String message) {
                view.showError(message);
            }
        });

    }

    @Override
    public void pickImageClick(boolean isStorageGranted) {
        if (isStorageGranted) {
            view.showImagePicker();
        } else {
            view.showRequestPermissions();
        }
    }

}
