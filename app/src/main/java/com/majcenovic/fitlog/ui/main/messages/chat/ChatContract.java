package com.majcenovic.fitlog.ui.main.messages.chat;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Message;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface ChatContract {

    interface View extends BaseView {
        void showTitle(String title);
        void showFriendUpdate(User friend);
        void showClearedInput();
        void showNewMessageUpdate(int messagesSize);
        void showNewMessageUpdateOnTop(int messagesSize);
        void showRefreshDateTime();
        void initializeAdapter(List<Message> messages);
    }

    interface Presenter extends BasePresenter {
        void loadFriend();
        void postMessage(String message);
        void loadOlderMessages();
        void loadNewerMessages();
        void setUpTitle();
        void scrollUpStop(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount);
        void start();
        void stop();
    }
}
