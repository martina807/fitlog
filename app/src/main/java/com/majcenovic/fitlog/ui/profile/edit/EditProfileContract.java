package com.majcenovic.fitlog.ui.profile.edit;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.UpdateUserDto;

/**
 * Created by martina on 09/09/2017.
 */

public interface EditProfileContract {

    interface View extends BaseView {
        void showProfile(User user);
        void showProfileUpdated(User user);
        void showImagePicker();
        void showRequestPermissions();
    }

    interface Presenter extends BasePresenter {
        void loadProfile();
        void pickImageClick(boolean isStorageGranted);
        void updateProfile(UpdateUserDto updateUserDto);
    }
}