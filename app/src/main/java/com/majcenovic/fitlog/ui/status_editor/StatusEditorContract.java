package com.majcenovic.fitlog.ui.status_editor;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.PostDto;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public interface StatusEditorContract {

    interface View extends BaseView {
        void showPreviousScreen();
        void showImagePicker();
        void showRequestPermissions();
        void showUserList(List<User> users);
    }

    interface Presenter extends BasePresenter {
        void postStatus(PostDto postDto);
        void loadAllFriends(String keyword);
        void pickImageClick(boolean isStorageGranted);
    }
}
