package com.majcenovic.fitlog.ui.sign_up.sign_up_third;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;

/**
 * Created by martina on 27/06/2017.
 */

public interface SignUpThirdContract {

    interface View extends BaseView {
        void showInvalidInput();
        void showMain();

    }

    interface Presenter extends BasePresenter {
        void submitSignUp(SignUpDto signUpDto);
    }
}
