package com.majcenovic.fitlog.ui.main.diary.meals;

import com.majcenovic.fitlog.data.home.HomeListener;
import com.majcenovic.fitlog.data.meals.MealsInteractor;
import com.majcenovic.fitlog.data.meals.MealsListener;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Meal;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public class MealsPresenter implements MealsContract.Presenter {

    MealsContract.View view;
    MealsInteractor interactor;

    public MealsPresenter(MealsContract.View view, MealsInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadMeals(final int dayOffset) {
        view.showProgress();
        interactor.getMeals(dayOffset, new MealsListener.GetMealsCallback() {
            @Override
            public void onMealsLoaded(List<Meal> meals) {
                view.showMealsLoaded(meals);
                view.hideProgress();
                view.showDayTitle(getDayTitle(dayOffset));
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
            }
        });
    }

    @Override
    public void onMealAddClick(Meal meal) {
        view.showAddFood(meal);
    }

    @Override
    public void loadCaloriesStatus() {
        view.showProgress();
        interactor.getCaloriesStatus(new HomeListener.CalorieStatusCallback() {
            @Override
            public void onCaloriesStatusLoaded(CaloriesStatus calorieStatus) {
                view.showCaloriesStatus(calorieStatus);
                view.hideProgress();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void onMealClick(Meal meal) {
        view.showFoodPerMeal(meal);
    }


    public String getDayTitle(int dayOffset) {
        switch (dayOffset) {
            case 0:
                return "Today";
            case -1:
                return "Yesterday";
            default:
                return DateFormat
                        .getDateInstance(DateFormat.DEFAULT)
                        .format(DateTime.now()
                                .plusDays(dayOffset)
                                .toDate());
        }
    }
}