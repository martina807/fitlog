package com.majcenovic.fitlog.ui.goals;

import com.majcenovic.fitlog.data.goals.GoalsInteractor;
import com.majcenovic.fitlog.data.goals.GoalsListener;
import com.majcenovic.fitlog.data.models.Goal;

/**
 * Created by martina on 28/06/2017.
 */

public class GoalsPresenter implements GoalsContract.Presenter {

    GoalsContract.View view;
    GoalsInteractor interactor;

    public GoalsPresenter(GoalsContract.View view, GoalsInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadGoals() {
        view.showProgress();
        interactor.getGoals(new GoalsListener.GetGoalsCallback() {
            @Override
            public void onGoalsLoaded(Goal goal) {
                view.hideProgress();
                view.showGoals(goal);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();

            }
        });
    }

    @Override
    public void openUpdateDialog(Goal goal, int weightTypeId) {
        view.showUpdateDialog(goal, weightTypeId);
    }
}