package com.majcenovic.fitlog.ui.profile;

import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.profile.ProfileInteractor;
import com.majcenovic.fitlog.data.profile.ProfileListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public class ProfilePresenter implements ProfileContract.Presenter {

    private ProfileContract.View view;
    private ProfileInteractor interactor;

    public ProfilePresenter(ProfileContract.View view, ProfileInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadUserProfile(final Long userId) {
        view.showProgress();

        interactor.getUserDetails(userId, new ProfileListener.ProfileCallback() {
            @Override
            public void onProfileLoaded(final User user) {
                interactor.getUserBoardPosts(userId, new ProfileListener() {
                    @Override
                    public void onPostsLoaded(List<Post> posts) {
                        view.hideProgress();
                        view.showBoardPostsAndUser(posts, user);
                    }

                    @Override
                    public void onFailure(String message) {
                        view.hideProgress();
                        view.showError(message);
                    }
                });
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void openStatusEditor() {
        view.showStatusEditor();
    }

    @Override
    public void startGalleryActivity(User user) {
        view.showGalleryActivity(user);
    }

    @Override
    public void startPeopleActivity() {
        view.showPeopleActivity();
    }

    @Override
    public void startCommentActivity(Post post) {
        view.showCommentActivity(post);
    }

    @Override
    public void likeDislikePost(final Post post) {
        if (post.isLiked()) {
            interactor.dislike(post, new ProfileListener.DislikeCallback() {
                @Override
                public void onDislikePosted() {
                    post.setLiked(!post.isLiked());
                    post.decrementLikes();
                    view.refreshData(post);
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        } else {
            interactor.like(post, new ProfileListener.LikeCallback() {
                @Override
                public void onLikePosted() {
                    post.setLiked(!post.isLiked());
                    post.incrementLikes();
                    view.refreshData(post);
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        }
    }

    @Override
    public void openChat(User user) {
        view.showChat(user);
    }

    @Override
    public void followUnfollow(final User user) {
        if (user.isFollowing()) {
            interactor.unfollow(user, new ProfileListener.UnfollowCallback() {
                @Override
                public void onUnfollow() {
                    user.setFollowing(false);
                    view.showFollowChanged();
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        } else {
            interactor.follow(user, new ProfileListener.FollowCallback() {
                @Override
                public void onFollow() {
                    user.setFollowing(true);
                    view.showFollowChanged();
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        }
    }

    @Override
    public void openEditProfile() {
        view.showEditProfile();
    }
}
