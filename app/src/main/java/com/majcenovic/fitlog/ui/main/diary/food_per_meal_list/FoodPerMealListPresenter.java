package com.majcenovic.fitlog.ui.main.diary.food_per_meal_list;

import com.majcenovic.fitlog.data.home.HomeListener;
import com.majcenovic.fitlog.data.meals.MealsInteractor;
import com.majcenovic.fitlog.data.meals.MealsListener;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.data.models.UserFood;
import com.majcenovic.fitlog.ui.main.diary.meals.MealsContract;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public class FoodPerMealListPresenter implements FoodPerMealListContract.Presenter {

    private FoodPerMealListContract.View view;
    private MealsInteractor interactor;

    public FoodPerMealListPresenter(FoodPerMealListContract.View view, MealsInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadFoodList(int dayOffset, Meal meal) {
        view.showProgress();
        interactor.getSingleMealFoods(dayOffset, meal.getMealType(), new MealsListener.GetSingleMealFoodCallback() {
            @Override
            public void onSingleMealFoodLoaded(List<UserFood> userFoods) {
                view.hideProgress();
                if(userFoods != null && !userFoods.isEmpty()) {
                    view.showFoodList(userFoods);
                } else {
                    view.showEmptyState();
                }
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void onDeleteFoodClick(final UserFood userFood) {
        view.showProgress();
        interactor.deleteUserFood(userFood.getId(), new MealsListener.DeleteSingleFoodStatusCallback() {
            @Override
            public void onSingleFoodDeleted() {
                view.hideProgress();
                view.removeFood(userFood);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

}