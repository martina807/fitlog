package com.majcenovic.fitlog.ui.main.messages.conversations;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.majcenovic.fitlog.BaseRecyclerViewAdapter;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.BaseModel;
import com.majcenovic.fitlog.data.models.Conversation;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.util.DateUtil;
import com.majcenovic.fitlog.util.ListRowTypeUtils;
import com.majcenovic.fitlog.util.PhotoUtils;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by vedran on 29/04/2017.
 */

public class ConversationsRecyclerViewAdapter extends BaseRecyclerViewAdapter<ConversationsRecyclerViewAdapter.ViewHolder> {

    private User user;
    private OnItemClickListener<Conversation> onItemClickListener;

    public ConversationsRecyclerViewAdapter(Context context, List itemList, User user) {
        super(context, itemList);
        this.user = user;
    }

    @Override
    public int getItemViewType(int position) {
        BaseModel model = (BaseModel) itemList.get(position);
        return model.getListType();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        super.onCreateViewHolder(parent, viewType);
        Integer layoutId = null;
        switch (viewType) {
            case ListRowTypeUtils.CONTENT:
                layoutId = R.layout.row_message;
                break;
            case ListRowTypeUtils.EMPTY:
                layoutId = R.layout.row_no_content;
                break;
        }
        return new ViewHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        BaseModel model = (BaseModel) itemList.get(position);

        switch (getItemViewType(position)) {
            case ListRowTypeUtils.CONTENT:
                setupContent(holder, position, model);
                setAnimation(holder.itemView, position);
                break;
            case ListRowTypeUtils.EMPTY:
                setupEmpty(holder, position, model);
                break;
        }
    }

    private void setupContent(ViewHolder holder, final int position, BaseModel m) {
        final Conversation model = (Conversation) m;

        if(model.getSender().getId().equals(user.getId())) {
            holder.tvContent.setText(String.format(context.getString(R.string.you_message), model.getMessage()));
            holder.tvName.setText(model.getReceiver().getFullName());
            PhotoUtils.loadProfilePhoto(context, model.getReceiver().getProfilePicture(), holder.rivPhoto);
        } else {
            holder.tvContent.setText(model.getMessage());
            holder.tvName.setText(model.getSender().getFullName());
            PhotoUtils.loadProfilePhoto(context, model.getSender().getProfilePicture(), holder.rivPhoto);
        }

        holder.tvCreatedAt.setText(DateUtil.toNiceDateAndTimeFormat(model.getCreatedAt()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) onItemClickListener.onItemClick(model);
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void setupEmpty(ViewHolder holder, int position, BaseModel m) {
        holder.tvNoContent.setText(context.getString(R.string.no_content));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.rivPhoto)
        RoundedImageView rivPhoto;

        @Nullable
        @BindView(R.id.tvName)
        TextView tvName;

        @Nullable
        @BindView(R.id.tvContent)
        TextView tvContent;

        @Nullable
        @BindView(R.id.tvCreatedAt)
        TextView tvCreatedAt;

        @Nullable
        @BindView(R.id.tvNoContent)
        TextView tvNoContent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}