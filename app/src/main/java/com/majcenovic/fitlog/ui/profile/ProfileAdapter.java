package com.majcenovic.fitlog.ui.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.Progress;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.profile.view_holders.BoardViewHolder;
import com.majcenovic.fitlog.ui.profile.view_holders.ButtonViewHolder;
import com.majcenovic.fitlog.ui.profile.view_holders.ImageBoardPostViewHolder;
import com.majcenovic.fitlog.ui.profile.view_holders.MessageBoardPostViewHolder;
import com.majcenovic.fitlog.ui.profile.view_holders.StatusBoardViewHolder;
import com.majcenovic.fitlog.ui.profile.view_holders.UserDataViewHolder;
import com.majcenovic.fitlog.util.ChartUtil;
import com.majcenovic.fitlog.util.DateUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public class ProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int BOARD = 6;
    private final int USER_DATA = 1;
    private final int STATUS = 2;
    private final int MESSAGE = 3;
    private final int IMAGE = 4;
    private final int BUTTON = 5;
    private final UserClickListener userClickListener;

    private List<Post> posts = new ArrayList<>();
    private User user;
    private final ButtonViewHolder.ButtonsClickListener buttonsClickListener;
    private final PostClickListener postClickListener;
    private final StatusBoardViewHolder.StatusClickListener statusClickListener;
    private User me;
    private Bundle savedInstanceState;
    private Context context;
    private HashMap<Integer, MapView> mapViews = new HashMap<>();

    public interface PostClickListener {
        void onLikeDislikeClick(Post post);

        void onCommentClick(Post post);
    }

    public interface UserClickListener {

        void onMessageClick(User user);

        void onFollowUnfollowClick(User user);

        void onProfileEditClick();
    }

    public ProfileAdapter(User user, ButtonViewHolder.ButtonsClickListener buttonsClickListener,
                          StatusBoardViewHolder.StatusClickListener statusClickListener,
                          PostClickListener postClickListener,
                          UserClickListener userClickListener) {
        this.user = user;
        this.buttonsClickListener = buttonsClickListener;
        this.postClickListener = postClickListener;
        this.statusClickListener = statusClickListener;
        this.userClickListener = userClickListener;
        me = SharedPrefs.readUserData(BaseApplication.appContext);
    }

    public void updatePost(Post post) {
        if (posts != null && posts.contains(post)) {
            int postPosition = posts.indexOf(post);

            // Posts start at position 4
            int viewHolderPosition = postPosition + 4;
            notifyItemChanged(viewHolderPosition);
        }
    }

    public void onResume() {
        if (!mapViews.isEmpty()) {
            for (MapView mapView : mapViews.values()) {
                try {
                    mapView.onResume();
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void onPause() {
        if (!mapViews.isEmpty()) {
            for (MapView mapView : mapViews.values()) {
                try {
                    mapView.onPause();
                    mapView = null;
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void updateData(List<Post> posts, User user, User loggedInUser) {
        this.me = loggedInUser;
        this.user = user;
        if (this.posts == null) {
            this.posts = new ArrayList<>();
        }
        this.posts.clear();

        if (posts != null) {
            this.posts.addAll(posts);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        Integer layoutId = null;
        switch (viewType) {
            case USER_DATA:
                return new UserDataViewHolder(LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.row_profile_user_data, parent, false));
            case BUTTON:
                return new ButtonViewHolder(LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.row_profile_buttons, parent, false));
            case STATUS:
                return new StatusBoardViewHolder(LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.row_board_status, parent, false));
            case BOARD:
                return new BoardViewHolder(LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.row_profile_board_title, parent, false));
            default:
                if (viewType == MESSAGE) {
                    return new MessageBoardPostViewHolder(LayoutInflater
                            .from(parent.getContext())
                            .inflate(R.layout.row_board_message_post, parent, false));
                } else {
                    return new ImageBoardPostViewHolder(LayoutInflater
                            .from(parent.getContext())
                            .inflate(R.layout.row_board_image_post, parent, false));
                }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (position) {
            case 0:
                setUpUserData(holder, user);
                break;
            case 1:
                setUpButtons((ButtonViewHolder) holder);
                break;
            case 2:
                setUpStatus((StatusBoardViewHolder) holder);
                break;
            case 3:
                setUpBoard((BoardViewHolder) holder);
                break;
            default:
                setUpPost(holder, posts.get(position - 4), position);
        }
    }

    private void setUpBoard(BoardViewHolder holder) {
        holder.getTvBoard().setText(BaseApplication.appContext.getString(R.string.board));
    }

    private void setUpStatus(StatusBoardViewHolder holder) {
        Picasso.with(context).load(me.getProfilePicture()).into(holder.getRivUserImage());
        if (user.getId().equals(me.getId())) {
            holder.getTvStatus().setHint(context.getString(R.string.share_your_thoughts));
        } else {
            holder.getTvStatus().setHint(String.format(context.getString(R.string.write_on_friend_board), user.getFirstName()));
        }
        holder.setStatusClickListener(statusClickListener);
    }

    private void setUpButtons(ButtonViewHolder holder) {
        holder.setButtonsClickListener(buttonsClickListener);
    }

    private void setUpUserData(RecyclerView.ViewHolder holder, final User user) {
        UserDataViewHolder userHolder = (UserDataViewHolder) holder;
        userHolder.getTvProfileName().setText(user.getFullName());
        userHolder.getTvProfileDesc().setText(user.getDescription());
        setUpImage(userHolder, user);
        setUpLayout(userHolder, user);

        userHolder.getBtnProfileFollow().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userClickListener.onFollowUnfollowClick(user);
            }
        });
        userHolder.getBtnProfileFollowed().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userClickListener.onFollowUnfollowClick(user);
            }
        });
        userHolder.getBtnProfileMessage().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userClickListener.onMessageClick(user);
            }
        });
        userHolder.getBtnProfileEdit().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userClickListener.onProfileEditClick();
            }
        });
    }

    private void setUpLayout(UserDataViewHolder userHolder, User user) {
        if (user.getId().equals(me.getId())) {
            userHolder.getBtnProfileEdit().setVisibility(View.VISIBLE);
            userHolder.getBtnProfileFollow().setVisibility(View.GONE);
            userHolder.getBtnProfileFollowed().setVisibility(View.GONE);
            userHolder.getBtnProfileMessage().setVisibility(View.GONE);
        } else {
            userHolder.getBtnProfileEdit().setVisibility(View.GONE);
            if (user.isFollowing()) {
                userHolder.getBtnProfileFollow().setVisibility(View.GONE);
                userHolder.getBtnProfileFollowed().setVisibility(View.VISIBLE);
            } else {
                userHolder.getBtnProfileFollow().setVisibility(View.VISIBLE);
                userHolder.getBtnProfileFollowed().setVisibility(View.GONE);
            }
            userHolder.getBtnProfileMessage().setVisibility(View.VISIBLE);
        }
    }

    private void setUpImage(UserDataViewHolder userHolder, User user) {
        if (user.getProfilePicture() != null) {
            Picasso.with(BaseApplication.appContext)
                    .load(user.getProfilePicture())
                    .into(userHolder.getRivProfileImage());
            Picasso.with(BaseApplication.appContext)
                    .load(user.getProfilePicture())
                    .error(R.drawable.placeholder_user)
                    .into(userHolder.getRivProfileImage());
        } else {
            userHolder.getRivProfileImage().setImageResource(R.drawable.placeholder_user);
        }
    }

    private void setUpPost(RecyclerView.ViewHolder holder, final Post post, int position) {

        switch (post.getType()) {
            case "message":
                MessageBoardPostViewHolder messageHolder = (MessageBoardPostViewHolder) holder;
                messageHolder.getTvMessageSenderRecipient().setText(Html.fromHtml(post.getTitle()));
                messageHolder.getTvBoardDateTime().setText(DateUtil.toNiceDateAndTimeFormat(post.getCreatedAt()));
                messageHolder.getTvBoardMessageDesc().setText(post.getMessage());
                messageHolder.getTvBoardNoOfComments().setText(String.valueOf(post.getCommentCount()));
                messageHolder.getTvNoOfBoardLikes().setText(String.valueOf(post.getLikesCount()));
                messageHolder.getIbBoardComment().setImageResource(R.drawable.comment);
                if (post.isLiked()) {
                    messageHolder.getIbBoardLike().setImageResource(R.drawable.like_filled);
                } else {
                    messageHolder.getIbBoardLike().setImageResource(R.drawable.like);
                }
                messageHolder.getTvBoardMessageDesc().setVisibility(View.VISIBLE);
                Picasso.with(BaseApplication.appContext)
                        .load(post.getSender().getProfilePicture())
                        .into(messageHolder.getRivProfileImage());

                messageHolder.getIbBoardLike().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        postClickListener.onLikeDislikeClick(post);
                    }
                });

                messageHolder.getIbBoardComment().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        postClickListener.onCommentClick(post);
                    }
                });

                break;
            case "photo":
            case "progress":
            case "activity":
                ImageBoardPostViewHolder imageHolder = (ImageBoardPostViewHolder) holder;
                imageHolder.getTvBoardSenderRecipient().setText(Html.fromHtml(post.getTitle()));
                imageHolder.getTvBoardDateTime().setText(DateUtil
                        .toNiceDateAndTimeFormat(post.getCreatedAt()));
                imageHolder.getTvBoardDescription().setText(post.getMessage());
                imageHolder.getTvBoardNoOfComments().setText(String.valueOf(post.getCommentCount()));
                imageHolder.getTvBoardNoOfLikes().setText(String.valueOf(post.getLikesCount()));
                if (post.getType().equals("photo")) {
                    Picasso.with(BaseApplication.appContext)
                            .load(post.getPhotos().get(0).getPhotoUrl())
                            .into(imageHolder.getIvBoardImage());
                } else if (post.getType().equals("progress")) {
                    setUpChart(imageHolder, post.getProgress());
                } else {
                    setUpMap(imageHolder, post.getActivity().getLocations());
                }
                setLayout(post.getType(), imageHolder);
                imageHolder.getIbBoardComment().setImageResource(R.drawable.comment);
                if (post.isLiked()) {
                    imageHolder.getIbBoardLike().setImageResource(R.drawable.like_filled);
                } else {
                    imageHolder.getIbBoardLike().setImageResource(R.drawable.like);
                }

                if (post.getMessage() != null) {
                    imageHolder.getTvBoardDescription().setVisibility(View.VISIBLE);
                } else {
                    imageHolder.getTvBoardDescription().setVisibility(View.GONE);
                }
                Picasso.with(BaseApplication.appContext)
                        .load(post.getSender().getProfilePicture())
                        .into(imageHolder.getRivPostProfileImage());

                imageHolder.getIbBoardLike().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        postClickListener.onLikeDislikeClick(post);
                    }
                });

                imageHolder.getIbBoardComment().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        postClickListener.onCommentClick(post);
                    }
                });
                break;
        }
    }

    private void setLayout(String type, ImageBoardPostViewHolder holder) {

        holder.getIvBoardImage().setVisibility(View.GONE);
        holder.getChartBoardProgress().setVisibility(View.GONE);
        holder.getMapBoardActivity().setVisibility(View.GONE);

        if (type.equals("photo")) {
            holder.getIvBoardImage().setVisibility(View.VISIBLE);
        } else if (type.equals("progress")) {
            holder.getChartBoardProgress().setVisibility(View.VISIBLE);
        } else if (type.equals("activity")) {
            holder.getMapBoardActivity().setVisibility(View.VISIBLE);
        }
    }

    private void setUpMap(final ImageBoardPostViewHolder holder, final List<GpsLocation> locations) {
        holder.getMapBoardActivity().onCreate(savedInstanceState);
        holder.getMapBoardActivity().getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                MapsInitializer.initialize(context);
                holder.getMapBoardActivity().onResume();
                mapViews.put(holder.getMapBoardActivity().hashCode(), holder.getMapBoardActivity());

                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        PolylineOptions options = new PolylineOptions()
                                .width(10)
                                .color(ContextCompat.getColor(BaseApplication.appContext, R.color.accent));
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();

                        for (GpsLocation location : locations) {
                            LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
                            options.add(point);
                            builder.include(point);
                        }

                        if (options.getPoints() != null && !options.getPoints().isEmpty()) {
                            googleMap.addPolyline(options);
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(),
                                    BaseApplication.appContext.getResources().getDimensionPixelSize(R.dimen.spacing_normal));
                            googleMap.moveCamera(cu);
                        }

                    }
                });
            }
        });
    }

    private void setUpChart(ImageBoardPostViewHolder holder, List<Progress> progressList) {
        ChartUtil.setChartProgress(holder.getChartBoardProgress(), progressList, BaseApplication.appContext);
    }

    @Override
    public int getItemCount() {
        return posts.size() + 4;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return USER_DATA;
            case 1:
                return BUTTON;
            case 2:
                return STATUS;
            case 3:
                return BOARD;
            default:
                if (posts.get(position - 4).getType().equals("message")) {
                    return MESSAGE;
                } else {
                    return IMAGE;
                }
        }
    }

    public void setSavedInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }
}
