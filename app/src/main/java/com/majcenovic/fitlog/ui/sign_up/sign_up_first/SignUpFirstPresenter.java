package com.majcenovic.fitlog.ui.sign_up.sign_up_first;

import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.util.InputValidationUtil;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpFirstPresenter implements SignUpFirstContract.Presenter {

    private SignUpFirstContract.View view;

    public SignUpFirstPresenter(SignUpFirstContract.View view) {
        this.view = view;
    }

    @Override
    public void cancel() {
    }

    @Override
    public void submitNext(final SignUpDto signUpDto) {
        if(InputValidationUtil.isNameValid(signUpDto.getFirstName())
                && InputValidationUtil.isNameValid(signUpDto.getLastName())
                && InputValidationUtil.isEmailValid(signUpDto.getEmail())
                && InputValidationUtil.isPasswordValid(signUpDto.getPassword())) {

            view.showSignUpSecond(signUpDto);
        } else {
            view.showInvalidInput();
        }
    }

    @Override
    public void pickImageClick(boolean isStorageGranted) {
        if(isStorageGranted) {
            view.showImagePicker();
        } else {
            view.showRequestPermissions();
        }
    }
}
