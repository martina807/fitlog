package com.majcenovic.fitlog.ui.profile.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Photo;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.comments.CommentsActivity;
import com.majcenovic.fitlog.ui.profile.gallery.photo.PhotoActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.List;

import butterknife.BindView;

/**
 * Created by martina on 14/08/2017.
 */

public class GalleryActivity extends BaseActivity implements GalleryContract.View {

    @BindView(R.id.rvList)
    RecyclerView rvList;

    @BindView(R.id.tvEmptyState)
    TextView tvEmptyState;

    private GalleryAdapter adapter;
    private GalleryContract.Presenter presenter;
    private List<Post> photos;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gallery;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initToolbar(R.string.gallery, R.drawable.ic_arrow_back_24dp);

        user = getIntent().getParcelableExtra(ExtraUtil.USER);

        rvList.setLayoutManager(new GridLayoutManager(context, 2));

        presenter = Injection.provideGalleryPresenter(this, context);
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.loadPhotoPosts(user);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void showPhotosFromPosts(List<Post> photos) {
        if (photos == null || photos.isEmpty()) {
            rvList.setVisibility(View.GONE);
            tvEmptyState.setVisibility(View.VISIBLE);
        } else {
            tvEmptyState.setVisibility(View.GONE);
            rvList.setVisibility(View.VISIBLE);

            if (adapter == null) {
                adapter = new GalleryAdapter(context, createPostClickListener());
            }
            if (rvList.getAdapter() == null) {
                rvList.setAdapter(adapter);
            }

            adapter.updateData(photos);
        }
    }

    private GalleryAdapter.PostClickListener createPostClickListener() {
        return new GalleryAdapter.PostClickListener() {
            @Override
            public void onPhotoClick(Photo photo) {
                presenter.openPhoto(photo);
            }

            @Override
            public void onLikeDislikeClick(Post post) {
                presenter.likeDislikePost(post);
            }

            @Override
            public void onCommentsClick(Post post) {
                presenter.openComments(post);
            }
        };
    }

    @Override
    public void showPhoto(Photo photo) {
        startActivity(new Intent(getApplicationContext(), PhotoActivity.class)
                .putExtra(ExtraUtil.PHOTO, photo)
                .putExtra(ExtraUtil.POST, photo.getPost()));
    }

    @Override
    public void showComments(Post post) {
        startActivity(new Intent(getApplicationContext(), CommentsActivity.class)
                .putExtra(ExtraUtil.OBJECT, post));
    }


    @Override
    public void refreshData() {
        adapter.notifyDataSetChanged();
    }
}