package com.majcenovic.fitlog.ui.main.activity;

import com.majcenovic.fitlog.data.activities.ActivityInteractor;
import com.majcenovic.fitlog.data.activities.ActivityListener;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.dto.PostDto;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public class ActivityPresenter implements ActivityContract.Presenter {

    ActivityContract.View view;
    ActivityInteractor interactor;

    public ActivityPresenter(ActivityContract.View view, ActivityInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadActivities(final Integer dayOffset) {
        view.showProgress();
        interactor.loadActivities(dayOffset, new ActivityListener.GetActivity() {
            @Override
            public void onActivitesLoaded(List<Activity> activities) {
                view.hideProgress();
                if(activities != null && !activities.isEmpty()) {
                    view.showActivities(activities);
                    view.showDayTitle(getDayTitle(dayOffset));
                    view.hideEmptyState();
                } else {
                    view.showDayTitle(getDayTitle(dayOffset));
                    view.showEmptyState();
                }
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void startRunningActivity(String activityType) {
        view.showRunningActivity(activityType.toLowerCase());
    }

    @Override
    public void onShareClick(Activity activity) {
        view.showProgress();
        PostDto postDto = new PostDto(activity.getId());
        interactor.postActivity(postDto, new ActivityListener.PostActivity() {
            @Override
            public void onActivityPosted() {
                view.hideProgress();
                view.showActivityPosted();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });

    }

    @Override
    public void setLayoutDependingOnActivityInProgress(Activity activity) {
        if(activity == null) view.showActivityNotInProgressState();
        else view.showActivityIsInProgressState();
    }

    public String getDayTitle(int dayOffset) {
        switch (dayOffset) {
            case 0:
                return "Today";
            case -1:
                return "Yesterday";
            default:
                return DateFormat
                        .getDateInstance(DateFormat.DEFAULT)
                        .format(DateTime.now()
                                .plusDays(dayOffset)
                                .toDate());
        }
    }
}
