package com.majcenovic.fitlog.ui.main.activity.running_activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.UpdateMapActivityEvent;
import com.majcenovic.fitlog.data.models.dto.ActivityDto;
import com.majcenovic.fitlog.data.models.dto.UpdateLocationDto;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.util.ConverterUtil;
import com.majcenovic.fitlog.util.DistanceCalculatorUtil;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.majcenovic.fitlog.util.InputValidationUtil;
import com.majcenovic.fitlog.util.PermissionUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

import static com.majcenovic.fitlog.ui.main.activity.running_activity.LocationService.scheduleJob;
import static com.majcenovic.fitlog.util.PermissionUtil.REQUEST_FINE_LOCATION_PERMISSION;

/**
 * Created by martina on 02/08/2017.
 */

public class RunningActivityActivity extends BaseActivity implements RunningActivityContract.View {

    private String activityType;

    @BindView(R.id.tvActivityRunningType)
    TextView tvActivityRunningType;

    @BindView(R.id.ivActivityRunningTypeImage)
    ImageView ivActivityRunningTypeImage;

    @BindView(R.id.mapStartActivity)
    MapView mapStartActivity;

    @BindView(R.id.tvActivityRunningStart)
    TextView start;

    @BindView(R.id.runningActivityContainer)
    LinearLayout runningActivityContainer;

    @BindView(R.id.ibRunningActivityArrowUp)
    ImageButton ibRunningActivityArrowUp;

    @BindView(R.id.ibRunningActivityArrowDown)
    ImageButton ibRunningActivityArrowDown;

    @BindView(R.id.tvActivityRunningDistance)
    TextView tvActivityRunningDistance;

    @BindView(R.id.chronometerStopwatch)
    Chronometer chronometerStopwatch;

    @BindView(R.id.tvActivityRunningAvgSpeed)
    TextView tvActivityRunningAvgSpeed;

    private RunningActivityContract.Presenter presenter;
    private Activity activitySharedPrefs;
    private Bundle saveInstanceState;
    private GoogleMap map;
    private UpdateLocationDto lastLocation;
    private float zoomLevel = 19f;
    private Long timerStartedAt;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_running_activity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.saveInstanceState = savedInstanceState;

        mapStartActivity.onCreate(saveInstanceState);


        presenter = Injection.provideRunningActivityPresenter(this, context);

        activityType = getIntent().getStringExtra(ExtraUtil.ACTIVITY_TYPE);
        setActivityTitle(activityType);

        activitySharedPrefs = SharedPrefs.readActivityInProgressData(context);

        initializeMap();
        initToolbar(getToolbarTitle());
    }

    private String getToolbarTitle() {
        switch (activityType) {
            case "walking":
                return getString(R.string.walking);
            case "running":
                return getString(R.string.running);
            default:
                return getString(R.string.cycling);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

//        timerStartedAt = SharedPrefs.readTimerStartedAt(context);


        presenter.getLayoutSetup(activitySharedPrefs);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateMapActivityEvent(final UpdateMapActivityEvent event) {
        drawOnMap(event.getGpsLocation());
        tvActivityRunningDistance.setText(String.format(Locale.getDefault(), "%.2f km", calculateDistance() / 1000));
        tvActivityRunningAvgSpeed.setText(String.format(Locale.getDefault(), "%.1f kmh", calculateAvgSpeed()));
    }

    private void drawOnMap(UpdateLocationDto gpsLocation) {
        if (map != null) {

            LatLng latLng = new LatLng(gpsLocation.getLatitude(), gpsLocation.getLongitude());

            if (lastLocation != null) {
                PolylineOptions polylineOptions = new PolylineOptions().width(10).color(ContextCompat.getColor(context, R.color.accent));
                polylineOptions.add(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
                polylineOptions.add(latLng);
                map.addPolyline(polylineOptions);
            }
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
        }
        lastLocation = gpsLocation;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onStartClick();
                }
            }
        }
    }

    private void setActivityTitle(String activityType) {
        if (activityType.equals(context.getString(R.string.walking).toLowerCase())) {
            tvActivityRunningType.setText(R.string.walking);
            ivActivityRunningTypeImage.setImageResource(R.drawable.walking_white);

        } else if (activityType.equals(context.getString(R.string.running).toLowerCase())) {
            tvActivityRunningType.setText(R.string.running);
            ivActivityRunningTypeImage.setImageResource(R.drawable.running_white);

        } else {
            tvActivityRunningType.setText(R.string.cycling);
            ivActivityRunningTypeImage.setImageResource(R.drawable.bike_white);

        }
    }

    @OnClick(R.id.tvActivityRunningStart)
    public void onStartClick() {
        if (PermissionUtil.checkFineLocationPermission(this)) {
            presenter.createActivity(activityType);
        }
    }

    private void initializeMap() {
        mapStartActivity.onCreate(saveInstanceState);
        mapStartActivity.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {


                MapsInitializer.initialize(context);

                mapStartActivity.onResume();

                map = googleMap;

                List<UpdateLocationDto> gpsLocations = SharedPrefs.readActivityLocations();

                if (gpsLocations != null && !gpsLocations.isEmpty()) {
                    drawExistingLines(gpsLocations);

                    if(timerStartedAt != null) {
                        tvActivityRunningDistance.setText(String.format(Locale.getDefault(), "%.2f km", calculateDistance() / 1000));
                        tvActivityRunningAvgSpeed.setText(String.format(Locale.getDefault(), "%.1f kmh", calculateAvgSpeed()));
                    }
                }



                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);
                }
                map.getUiSettings().setMyLocationButtonEnabled(true);
                map.getUiSettings().setAllGesturesEnabled(true);
                map.setOnCameraMoveCanceledListener(new GoogleMap.OnCameraMoveCanceledListener() {
                    @Override
                    public void onCameraMoveCanceled() {

                    }
                });
                map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {

                    }
                });
                map.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                    @Override
                    public void onCameraMove() {
                        zoomLevel = map.getCameraPosition().zoom;
                    }
                });
                map.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                    @Override
                    public void onCameraMoveStarted(int i) {

                    }
                });

                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        Log.d("MYTAG", "map loaded");
                    }
                });
            }
        });
    }

    private void drawExistingLines(List<UpdateLocationDto> gpsLocations) {

        PolylineOptions polylineOptions = new PolylineOptions().width(10).color(ContextCompat.getColor(context, R.color.accent));

        for (UpdateLocationDto gpsLocation : gpsLocations) {
            LatLng latLng = new LatLng(gpsLocation.getLatitude(), gpsLocation.getLongitude());
            polylineOptions.add(latLng);
        }

        if (map != null) {
            map.addPolyline(polylineOptions);

            UpdateLocationDto lastGpsLocation = gpsLocations.get(gpsLocations.size() - 1);
            LatLng lastLatLng = new LatLng(lastGpsLocation.getLatitude(), lastGpsLocation.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLatLng, 19));
            lastLocation = lastGpsLocation;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapStartActivity.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapStartActivity.onPause();
    }

    public void showTrackingLocationLayout() {
        start.setVisibility(View.GONE);
        ibRunningActivityArrowUp.setVisibility(View.GONE);
        runningActivityContainer.setVisibility(View.VISIBLE);
        ibRunningActivityArrowDown.setVisibility(View.VISIBLE);
    }

    private void hideTrackingLocationLayout() {
        start.setVisibility(View.GONE);
        runningActivityContainer.setVisibility(View.GONE);
        ibRunningActivityArrowDown.setVisibility(View.GONE);
        ibRunningActivityArrowUp.setVisibility(View.VISIBLE);
    }

    private void showStartLayout() {
        start.setVisibility(View.VISIBLE);
        runningActivityContainer.setVisibility(View.GONE);
        ibRunningActivityArrowDown.setVisibility(View.GONE);
        ibRunningActivityArrowUp.setVisibility(View.GONE);
    }

    @Override
    public void showPositionOnMap() {

    }

    @Override
    public void showActivityCreated(Activity activityCreated) {
        SharedPrefs.saveActivityInProgressData(context, activityCreated);
        activitySharedPrefs = activityCreated;

        SharedPrefs.saveTimerStartedAt(context, SystemClock.elapsedRealtime());
        startChronometerStopwatch();

        startLocationTracking(activitySharedPrefs);
        showTrackingLocationLayout();
    }

    private void startLocationTracking(Activity activityCreated) {
        scheduleJob(getApplicationContext(), activityCreated);
    }

    private void startChronometerStopwatch() {
        timerStartedAt = SharedPrefs.readTimerStartedAt(context);
        if (timerStartedAt != null) {
            chronometerStopwatch.setBase(timerStartedAt);
        }
        chronometerStopwatch.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long timeInMiliSec = SystemClock.elapsedRealtime() - cArg.getBase();
                cArg.setText(ConverterUtil.fromMiliSecToStandard(timeInMiliSec));
            }
        });
        chronometerStopwatch.start();
    }

    @OnClick({R.id.ibRunningActivityArrowDown, R.id.ibRunningActivityArrowUp})
    public void onArrowClick(View view) {
        if (view.getId() == R.id.ibRunningActivityArrowDown) {
            hideTrackingLocationLayout();
        } else if (view.getId() == R.id.ibRunningActivityArrowUp) {
            showTrackingLocationLayout();
        }
    }

    @Override
    public void showActivityFragment() {
        LocationService.cancelJob(this);
        finish();
    }

    @Override
    public void removeActivityFromSharedPrefs() {
        SharedPrefs.removeActivityInProgressData(context);
        SharedPrefs.removeActivityLocations(context);
        SharedPrefs.removeTimerStartedAt(context);
    }

    private void stopChronometerStopwatch() {
        chronometerStopwatch.stop();
        timerStartedAt = SharedPrefs.readTimerStartedAt(context);
    }

    @Override
    public void showActivityInProgressLayout() {
        showTrackingLocationLayout();
        resumeChronometerStopwatch();
//        startLocationTracking(activitySharedPrefs);
    }

    private void resumeChronometerStopwatch() {
        startChronometerStopwatch();
    }

    @Override
    public void showActivityNotInProgressLayout() {
        showStartLayout();
    }


    @OnClick(R.id.ibActivityRunningStop)
    public void onStopClick() {
        stopChronometerStopwatch();

        if (InputValidationUtil.isActivityInputValid(getActivityDtoData())) {
            presenter.stopActivity(getActivityDtoData());
        }
    }

    private ActivityDto getActivityDtoData() {
        long duration = (SystemClock.elapsedRealtime() - timerStartedAt) / 1000;
        int distanceInM = (int) calculateDistance();
        return new ActivityDto(activitySharedPrefs.getId(), distanceInM, (int) duration, 100.5, calculateAvgSpeed());
    }

    private float calculateDistance() {
        float distance = 0f;
        List<UpdateLocationDto> updateLocationDtos = SharedPrefs.readActivityLocations();
        if (updateLocationDtos != null) {
            distance = DistanceCalculatorUtil.getTraveledDistance(updateLocationDtos);
        }
        return distance;
    }

    private double calculateAvgSpeed() {
        long duration = (SystemClock.elapsedRealtime() - timerStartedAt) / 1000;
        return calculateDistance() / duration * 3.6;
    }
}
