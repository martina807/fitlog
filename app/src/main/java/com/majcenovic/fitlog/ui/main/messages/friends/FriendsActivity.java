package com.majcenovic.fitlog.ui.main.messages.friends;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.main.home.search.SearchActivity;
import com.majcenovic.fitlog.ui.main.messages.chat.ChatActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 14/08/2017.
 */

public class FriendsActivity extends BaseActivity implements FriendsContract.View {

    @BindView(R.id.rvListPeople)
    RecyclerView rvList;

    @BindView(R.id.tvEmptyState)
    TextView tvEmptyState;

    @BindView(R.id.btnSearchForFriends)
    Button btnSearchForFriends;

    private FriendsContract.Presenter presenter;
    private FriendsAdapter listAdapter;
    private List<User> listItems = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rvList.setLayoutManager(new LinearLayoutManager(context));

        user = SharedPrefs.readUserData(context);

        presenter = Injection.provideFriendsPresenter(this, context);

        initToolbar(R.string.people, R.drawable.ic_arrow_back_24dp);
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.loadPeople(user.getId());
    }

    @Override
    protected void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_friends;
    }

    @Override
    public void showPeople(List<User> people) {

        if(people == null || people.isEmpty()) {
            tvEmptyState.setVisibility(View.VISIBLE);
            rvList.setVisibility(View.GONE);
            btnSearchForFriends.setVisibility(View.VISIBLE);
        } else {
            tvEmptyState.setVisibility(View.GONE);
            rvList.setVisibility(View.VISIBLE);
            btnSearchForFriends.setVisibility(View.GONE);

            listItems.clear();
            listItems.addAll(people);

            if (listAdapter == null) {
                listAdapter = new FriendsAdapter(context, people, setPeopleClickListener());
            }

            if (rvList.getAdapter() == null) {
                rvList.setAdapter(listAdapter);
            }

            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showChat(User user) {
        startActivity(new Intent(getApplicationContext(), ChatActivity.class)
                .putExtra(ExtraUtil.OBJECT, user));
        finish();
    }

    private FriendsAdapter.PeopleClickListener setPeopleClickListener() {
        return new FriendsAdapter.PeopleClickListener() {
            @Override
            public void onMessageClick(User user) {
                presenter.startChat(user);
            }
        };
    }

    @OnClick(R.id.btnSearchForFriends)
    public void onSearchClick() {
        startActivity(new Intent(getApplicationContext(), SearchActivity.class));
    }
}
