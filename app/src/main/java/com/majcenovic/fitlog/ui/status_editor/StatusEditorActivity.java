package com.majcenovic.fitlog.ui.status_editor;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.PostDto;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.majcenovic.fitlog.util.ImageUtil;
import com.majcenovic.fitlog.util.PermissionUtil;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mvc.imagepicker.ImagePicker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 14/08/2017.
 */

public class StatusEditorActivity extends BaseActivity implements StatusEditorContract.View {

    @BindView(R.id.etStatus)
    EditText etStatus;

    @BindView(R.id.actvTag)
    AutoCompleteTextView actvTag;

    @BindView(R.id.rivUserImage)
    RoundedImageView rivUserImage;

    @BindView(R.id.fboxPhotos)
    FlexboxLayout fboxPhotos;

    @BindView(R.id.fboxTags)
    FlexboxLayout fboxTags;

    private TextView textView;

    private StatusEditorContract.Presenter presenter;
    private List<File> photos;
    private List<Long> tagIds = new ArrayList<>();
    private User me;
    private List<User> users;
    private List<String> allUserNames;
    private Long friendUserId;
    private List<User> selectedUsers = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        me = SharedPrefs.readUserData(context);

        user = getIntent().getParcelableExtra(ExtraUtil.USER);

        if (user != null) {
            // Ignore user object is logged user is trying to write to himself
            // By setting user to null, user is posting to his board
            if (user.getId().equals(me.getId())) {
                user = null;
            } else {
                friendUserId = user.getId();
            }
        }

        presenter = Injection.provideStatusEditorPresenter(this, context);

        if (user != null) {
            initToolbar(String.format(getString(R.string.post_to_user), user.getFirstName()));
        } else {
            initToolbar(R.string.post_to_fitlog);
        }

        actvTag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.loadAllFriends(s.toString().trim());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Picasso.with(context).load(me.getProfilePicture()).into(rivUserImage);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_status_editor;
    }

    @Override
    public void showPreviousScreen() {
        onBackPressed();
    }

    @OnClick(R.id.btnPost)
    public void onPostClicked() {
        PostDto postDto = createPostDto();
        presenter.postStatus(postDto);
    }

    private PostDto createPostDto() {
        return new PostDto(
                friendUserId,
                etStatus.getText().toString().trim(),
                photos,
                tagIds
        );
    }

    @OnClick(R.id.ibPostPhoto)
    public void onPostPhotoClick() {
        presenter.pickImageClick(PermissionUtil.isStorageGranted(context));
    }

    @Override
    public void showImagePicker() {
        ImagePicker.pickImage((Activity) context);
    }

    @Override
    public void showRequestPermissions() {
        PermissionUtil.requestStoragePermissions(context);
    }

    @Override
    public void showUserList(final List<User> users) {
        this.users = users;

        allUserNames = new ArrayList<>();

        if (users != null) {
            for (User item : users) {
                allUserNames.add(item.getFullName());
            }
        }

        //Creating the instance of ArrayAdapter containing list of fruit names
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, allUserNames);
        actvTag.setThreshold(1);//will start working from first character
        actvTag.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        actvTag.setTextColor(ContextCompat.getColor(this, R.color.secondary_text));

        actvTag.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                actvTag.setText("");
                if (position > -1) {
                    if (users != null) {
                        if (!selectedUsers.contains(users.get(position))) {
                            selectedUsers.add(users.get(position));
                            updateTagsList();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        actvTag.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                actvTag.setText("");
                if (position > -1) {
                    if (users != null) {
                        if (!selectedUsers.contains(users.get(position))) {
                            selectedUsers.add(users.get(position));
                            updateTagsList();
                        }
                    }
                }
            }
        });
    }

    private void updateTagsList() {
        fboxTags.setVisibility(View.VISIBLE);
        fboxTags.removeAllViews();
        tagIds.clear();
        if (!selectedUsers.isEmpty()) {
            for (final User user : selectedUsers) {
                textView = (TextView) getLayoutInflater().inflate(R.layout.item_tag, null);
                textView.setText(user.getFullName());
                fboxTags.addView(textView);
                tagIds.add(user.getId());
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectedUsers.remove(user);
                        updateTagsList();
                    }
                });
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.STORAGE_PERMISSIONS_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            presenter.pickImageClick(PermissionUtil.isStorageGranted(context));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = ImagePicker.getImageFromResult(context, requestCode, resultCode, data);
            if (photos == null) photos = new ArrayList<>();
            photos.add(ImageUtil.convertBitmapToFile(context, bitmap));

            View view = getLayoutInflater().inflate(R.layout.item_image, null);
            ImageView imageView = view.findViewById(R.id.item_image);
            imageView.setImageBitmap(bitmap);

            fboxPhotos.setVisibility(View.VISIBLE);
            fboxPhotos.addView(view);
        }
    }

    @OnClick(R.id.ibTag)
    public void onTagClick() {
        if (actvTag.getVisibility() == View.GONE) {
            actvTag.setVisibility(View.VISIBLE);
            actvTag.requestFocus();
        } else {
            actvTag.setVisibility(View.GONE);
        }
    }
}
