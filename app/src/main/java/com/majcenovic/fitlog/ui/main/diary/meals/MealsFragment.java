package com.majcenovic.fitlog.ui.main.diary.meals;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseFragment;
import com.majcenovic.fitlog.ui.main.diary.add_food_per_meal.AddFoodPerMealCustomDialog;
import com.majcenovic.fitlog.ui.main.diary.food_per_meal_list.FoodPerMealListActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 28/06/2017.
 */

public class MealsFragment extends BaseFragment implements MealsContract.View {

    public static final String TAG = "GoalsActivity";

    @BindView(R.id.tvMealsTitle)
    TextView tvMealsTitle;

    @BindView(R.id.tvMealsRemainingCaloriesNumber)
    TextView tvMealsRemainingCaloriesNumber;

    @BindView(R.id.tvMealsCaloriesEatenNumber)
    TextView tvMealsCaloriesEatenNumber;

    @BindView(R.id.rvMealsListOfMeals)
    RecyclerView rvMealsListOfMeals;

    @BindView(R.id.tvMealsRemainingCaloriesText)
    TextView tvMealsRemainingCaloriesText;

    @BindView(R.id.tvMealsCaloriesEatenText)
    TextView tvMealsCaloriesEatenText;

    private int dayOffset;
    public static final int MAX_DAY_OFFSET = 0;
    private MealsAdapter rvMealsAdapter;
    private List<Meal> listItems = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_meals;
    }

    MealsContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = Injection.provideMealsPresenter(this, context);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvMealsListOfMeals.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.loadMeals(dayOffset);
        presenter.loadCaloriesStatus();
    }

    @OnClick({R.id.btnMealsNext, R.id.btnMealsPrev})
    public void onDayIndexChanged(View view) {
        if (R.id.btnMealsPrev == view.getId()) dayOffset--;
        else dayOffset++;
        if (dayOffset >= MAX_DAY_OFFSET) dayOffset = MAX_DAY_OFFSET;
        presenter.loadMeals(dayOffset);
    }

    @Override
    public void showError(String message) {
        showPopUp(message);
    }

    @Override
    public void showAddFood(Meal meal) {
        AddFoodPerMealCustomDialog.newInstance(meal.getMealType(), new AddFoodPerMealCustomDialog.AddFoodPerMealCallback() {
            @Override
            public void onFoodAdded() {
                presenter.loadMeals(dayOffset);
                presenter.loadCaloriesStatus();
            }
        }).show(getActivity().getSupportFragmentManager(), AddFoodPerMealCustomDialog.TAG);
    }

    @Override
    public void showFoodPerMeal(Meal meal) {
        startActivity(new Intent(getActivity(), FoodPerMealListActivity.class)
                .putExtra(ExtraUtil.SHOW_FOOD_PER_MEAl, meal)
                .putExtra(ExtraUtil.DAY_OFFSET, dayOffset));
    }

    @Override
    public void showMealsLoaded(List<Meal> meals) {
        listItems.clear();
        listItems.addAll(meals);
        if (rvMealsAdapter == null) {
            rvMealsAdapter = new MealsAdapter(context, listItems);
            rvMealsListOfMeals.setAdapter(rvMealsAdapter);
            rvMealsAdapter.setOnAddMealClickListener(onAddMealClickListener);
            rvMealsAdapter.setOnMealClickListener(onMealClickListener);
        } else {
            rvMealsAdapter.notifyDataSetChanged();
        }
    }

    private MealsAdapter.OnAddMealClickListener onAddMealClickListener = new MealsAdapter.OnAddMealClickListener() {
        @Override
        public void onClick(Meal meal, int position) {
            presenter.onMealAddClick(meal);
        }
    };

    private MealsAdapter.OnMealClickListener onMealClickListener = new MealsAdapter.OnMealClickListener() {
        @Override
        public void onClick(Meal meal, int position) {
            presenter.onMealClick(meal);
        }
    };

    @Override
    public void showDayTitle(String s) {
        tvMealsTitle.setText(s);
    }

    @Override
    public void showCaloriesStatus(CaloriesStatus caloriesStatus) {
        tvMealsCaloriesEatenNumber.setText(String.format(Locale.getDefault(), "%.0f", caloriesStatus.getCaloriesEaten()));
        tvMealsRemainingCaloriesText.setText(getString(R.string.calories_remained));
        tvMealsRemainingCaloriesNumber.setText(String.format(Locale.getDefault(), "%.0f", caloriesStatus.getCaloriesRemaining()));
        tvMealsCaloriesEatenText.setText(getString(R.string.calories_eaten));
    }

    @Override
    public void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }
}