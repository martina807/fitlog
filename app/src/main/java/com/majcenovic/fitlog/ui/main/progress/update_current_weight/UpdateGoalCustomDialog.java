package com.majcenovic.fitlog.ui.main.progress.update_current_weight;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.goals.GoalTypes;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.majcenovic.fitlog.util.SpinnerUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 05/07/2017.
 */

public class UpdateGoalCustomDialog extends DialogFragment implements UpdateGoalContract.View {

    private static UpdateValueCallback callback;

    private UpdateGoalContract.Presenter presenter;

    public static UpdateGoalCustomDialog newInstance(int weightType, String value,
                                                     UpdateGoalCustomDialog.UpdateValueCallback callback) {
        UpdateGoalCustomDialog.callback = callback;
        UpdateGoalCustomDialog fragment = new UpdateGoalCustomDialog();
        Bundle args = new Bundle();
        args.putInt(ExtraUtil.WEIGHT_TYPE, weightType);
        args.putString(ExtraUtil.OBJECT, value);
        fragment.setArguments(args);
        fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert);
        return fragment;
    }

    @Override
    public void showCurrentWeightUpdated() {
        dismiss();
        callback.onCurrentWeightUpdated();
    }

    @Override
    public void showFinalGoalUpdated() {
        dismiss();
        callback.onFinalGoalUpdated();
    }

    @Override
    public void showWeeklyGoalUpdated() {
        dismiss();
        callback.onWeeklyGoalUpdated();
    }

    @Override
    public void showActivityLevelUpdated() {
        dismiss();
        callback.onActivityLevelUpdated();
    }

    public interface UpdateValueCallback {
        void onCurrentWeightUpdated();

        void onWeeklyGoalUpdated();

        void onFinalGoalUpdated();

        void onActivityLevelUpdated();
    }

    public static String TAG = "UPDATE_CURRENT_WEIGHT";

    @BindView(R.id.etUpdateCurrentWeight)
    EditText etInput;

    @BindView(R.id.tvUnit)
    TextView tvUnit;

    @BindView(R.id.spActivityLevel)
    Spinner spActivityLevel;

    private AlertDialog alertDialog;
    private int weightType;
    private String value;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = Injection.provideUpdateCurrentWeightPresenter(this, getActivity());

        weightType = getArguments().getInt(ExtraUtil.WEIGHT_TYPE);
        value = getArguments().getString(ExtraUtil.OBJECT);
    }

    @Override
    public void onStart() {
        super.onStart();

        Button buttonPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        buttonPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (weightType == GoalTypes.ACTIVITY_LEVEL) {

                    presenter.postActivityLevel(getActivityLevelType());

                } else {

                    if (fieldsFilled()) {

                        Double weight = Double.parseDouble(etInput.getText().toString());

                        switch (weightType) {
                            case GoalTypes.CURRENT_WEIGHT:
                                presenter.postCurrentWeight(weight);
                                break;
                            case GoalTypes.FINAL_GOAL:
                                presenter.postFinalGoal(weight);
                                break;
                            case GoalTypes.WEEKLY_GOAL:
                                presenter.postWeeklyGoal(weight);
                                break;
                        }

                    }
                }
            }
        });
    }

    private String getActivityLevelType() {
        switch (spActivityLevel.getSelectedItemPosition()) {
            case 0:
                return "not_active";
            case 1:
                return "slightly_active";
            case 2:
                return "active";
            case 3:
                return "very_active";
            case 4:
                return "extremely_active";
        }
        return null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View contentView = getActivity()
                .getLayoutInflater()
                .inflate(R.layout.custom_alert_dialog_update_current_weight, null);
        ButterKnife.bind(this, contentView);


        if (weightType == GoalTypes.ACTIVITY_LEVEL) {

            tvUnit.setVisibility(View.GONE);
            etInput.setVisibility(View.GONE);
            spActivityLevel.setVisibility(View.VISIBLE);
            SpinnerUtil.populateActivityLevelsSpDefault(getContext(), spActivityLevel);

            if (value != null) {
                spActivityLevel.setSelection(getPositionForActivityType());
            }
        } else {
            if (value != null) {
                etInput.setText(value);
            }
            etInput.setHint(getHintForType());
            tvUnit.setVisibility(View.VISIBLE);
            etInput.setVisibility(View.VISIBLE);
            spActivityLevel.setVisibility(View.GONE);
        }

        alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(getTitleForType())
                .setView(contentView)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.submit), null)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create();

        return alertDialog;
    }

    private int getPositionForActivityType() {
        switch (value) {
            case "not_active":
                return 0;
            case "slightly_active":
                return 1;
            case "active":
                return 2;
            case "very_active":
                return 3;
            case "extremely_active":
                return 4;
        }
        return 0;
    }

    private String getTitleForType() {
        switch (weightType) {
            case GoalTypes.CURRENT_WEIGHT:
                return getString(R.string.update_current_weight);
            case GoalTypes.FINAL_GOAL:
                return getString(R.string.update_final_goal);
            case GoalTypes.WEEKLY_GOAL:
                return getString(R.string.update_weekly_goal);
            case GoalTypes.ACTIVITY_LEVEL:
                return getString(R.string.update_activity_level);
        }
        return "";
    }

    private String getHintForType() {
        switch (weightType) {
            case GoalTypes.CURRENT_WEIGHT:
                return getString(R.string.current_weight);
            case GoalTypes.FINAL_GOAL:
                return getString(R.string.final_goal);
            case GoalTypes.WEEKLY_GOAL:
                return getString(R.string.weekly_goal);
        }
        return "";
    }

    private boolean fieldsFilled() {
        if (etInput.getText().toString().trim().length() > 0) return true;
        return false;
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}