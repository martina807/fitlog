package com.majcenovic.fitlog.ui.goals;

/**
 * Created by martina on 09/09/2017.
 */

public class GoalTypes {

    public static final int CURRENT_WEIGHT = 1;
    public static final int WEEKLY_GOAL = 2;
    public static final int FINAL_GOAL = 3;
    public static final int ACTIVITY_LEVEL = 4;
}
