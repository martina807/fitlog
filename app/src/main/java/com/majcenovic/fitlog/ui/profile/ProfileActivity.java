package com.majcenovic.fitlog.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.comments.CommentsActivity;
import com.majcenovic.fitlog.ui.main.messages.chat.ChatActivity;
import com.majcenovic.fitlog.ui.profile.edit.EditProfileActivity;
import com.majcenovic.fitlog.ui.profile.gallery.GalleryActivity;
import com.majcenovic.fitlog.ui.profile.people.PeopleActivity;
import com.majcenovic.fitlog.ui.profile.view_holders.ButtonViewHolder;
import com.majcenovic.fitlog.ui.profile.view_holders.StatusBoardViewHolder;
import com.majcenovic.fitlog.ui.status_editor.StatusEditorActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.List;

import butterknife.BindView;

/**
 * Created by martina on 14/08/2017.
 */

public class ProfileActivity extends BaseActivity implements ProfileContract.View {

    @BindView(R.id.rvList)
    RecyclerView rvList;

    @BindView(R.id.tvEmptyState)
    TextView tvEmptyState;

    private User loggedInUser;
    private User user;
    private ProfileContract.Presenter presenter;
    private ProfileAdapter rvProfileAdapter;

    private Bundle savedInstanceState;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.savedInstanceState = savedInstanceState;

        presenter = Injection.provideProfilePresenter(this, context);

        user = getIntent().getParcelableExtra(ExtraUtil.USER);

        rvList.setLayoutManager(new LinearLayoutManager(context));

        initToolbar(user.getFullName());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (rvProfileAdapter != null) {
            rvProfileAdapter.setSavedInstanceState(savedInstanceState);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        loggedInUser = SharedPrefs.readUserData(this);

        presenter.loadUserProfile(user.getId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (rvProfileAdapter != null) {
            rvProfileAdapter.onResume();
        }
    }

    @Override
    protected void onPause() {
        if (rvProfileAdapter != null) {
            rvProfileAdapter.onPause();
        }
        super.onPause();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_profile_board;
    }

    @Override
    public void showBoardPostsAndUser(List<Post> postList, User user) {
        setUpAdapter(postList, user);
        if(postList == null || postList.isEmpty()) {
            tvEmptyState.setVisibility(View.VISIBLE);
        } else {
            tvEmptyState.setVisibility(View.GONE);
        }
    }

    @Override
    public void showStatusEditor() {
        startActivity(new Intent(BaseApplication.appContext, StatusEditorActivity.class)
                .putExtra(ExtraUtil.USER, user));
    }

    @Override
    public void showGalleryActivity(User user) {
        startActivity(new Intent(getApplicationContext(), GalleryActivity.class)
                .putExtra(ExtraUtil.USER, user));
    }

    @Override
    public void showPeopleActivity() {
        startActivity(new Intent(getApplicationContext(), PeopleActivity.class)
                .putExtra(ExtraUtil.USER, user));
    }

    @Override
    public void refreshData(Post post) {
        rvProfileAdapter.updatePost(post);
    }

    @Override
    public void showCommentActivity(Post post) {
        startActivity(new Intent(getApplicationContext(), CommentsActivity.class)
                .putExtra(ExtraUtil.OBJECT, post));
    }

    @Override
    public void showChat(User user) {
        startActivity(new Intent(context, ChatActivity.class)
                .putExtra(ExtraUtil.OBJECT, user));
    }

    @Override
    public void showFollowChanged() {
        rvProfileAdapter.notifyDataSetChanged();
    }

    @Override
    public void showEditProfile() {
        startActivity(new Intent(getApplicationContext(), EditProfileActivity.class));
    }

    private void setUpAdapter(List<Post> posts, User user) {
        if (rvProfileAdapter == null) {
            rvProfileAdapter = new ProfileAdapter(user,
                    setButtonsClickListener(),
                    setStatusClickListener(),
                    setLikeCommentClickListener(),
                    setProfileClickListener());
            setButtonsClickListener();
            setStatusClickListener();
        }
        rvProfileAdapter.setSavedInstanceState(savedInstanceState);
        if (rvList.getAdapter() == null) {
            rvList.setAdapter(rvProfileAdapter);
        }

        initToolbar(user.getFullName());

        rvProfileAdapter.updateData(posts, user, loggedInUser);
    }

    private ProfileAdapter.UserClickListener setProfileClickListener() {
        return new ProfileAdapter.UserClickListener() {
            @Override
            public void onMessageClick(User user) {
                presenter.openChat(user);
            }

            @Override
            public void onFollowUnfollowClick(User user) {
                presenter.followUnfollow(user);
            }

            @Override
            public void onProfileEditClick() {
                presenter.openEditProfile();
            }
        };
    }

    private StatusBoardViewHolder.StatusClickListener setStatusClickListener() {
        return new StatusBoardViewHolder.StatusClickListener() {
            @Override
            public void onStatusClick() {
                presenter.openStatusEditor();
            }
        };
    }

    private ProfileAdapter.PostClickListener setLikeCommentClickListener() {
        return new ProfileAdapter.PostClickListener() {
            @Override
            public void onLikeDislikeClick(Post post) {
                presenter.likeDislikePost(post);
            }

            @Override
            public void onCommentClick(Post post) {
                presenter.startCommentActivity(post);
            }
        };
    }

    private ButtonViewHolder.ButtonsClickListener setButtonsClickListener() {
        return new ButtonViewHolder.ButtonsClickListener() {
            @Override
            public void onPhotosClick() {
                presenter.startGalleryActivity(user);
            }

            @Override
            public void onPeopleClick() {
                presenter.startPeopleActivity();
            }
        };
    }
}