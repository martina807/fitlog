package com.majcenovic.fitlog.ui.main.home.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 09/09/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private Context context;
    private final User loggedUser;
    private final SearchClickListener searchClickListener;
    private List<User> list = new ArrayList<>();

    public SearchAdapter(Context context, User loggedUser, SearchClickListener searchClickListener) {
        this.context = context;
        this.loggedUser = loggedUser;
        this.searchClickListener = searchClickListener;
    }

    public interface SearchClickListener {

        void onUserClick(User user);

        void onFollowUnfollowClick(User user);
    }

    public void updateData(List<User> users) {
        list.clear();

        if (users != null) {
            list.addAll(users);
        }

        notifyDataSetChanged();
    }

    public void updateUserState(User user) {
        if (list != null && list.contains(user)) {
            notifyItemChanged(list.indexOf(user));
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_people, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final User user = list.get(position);

        if (loggedUser.getId().equals(user.getId())) {

            holder.btnUnfollow.setVisibility(View.GONE);
            holder.btnFollow.setVisibility(View.GONE);

        } else {

            if (user.isFollowing()) {
                holder.btnUnfollow.setVisibility(View.VISIBLE);
                holder.btnFollow.setVisibility(View.GONE);
            } else {
                holder.btnUnfollow.setVisibility(View.GONE);
                holder.btnFollow.setVisibility(View.VISIBLE);
            }

        }

        holder.tvFullName.setText(user.getFullName());
        holder.btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchClickListener.onFollowUnfollowClick(user);
            }
        });

        holder.btnUnfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchClickListener.onFollowUnfollowClick(user);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchClickListener.onUserClick(user);
            }
        });

        Picasso.with(context).load(user.getProfilePicture()).into(holder.rivProfileImage);
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvFullName)
        TextView tvFullName;

        @BindView(R.id.btnFollow)
        Button btnFollow;

        @BindView(R.id.btnUnfollow)
        Button btnUnfollow;

        @BindView(R.id.rivProfileImage)
        RoundedImageView rivProfileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}