package com.majcenovic.fitlog.ui.main.diary.add_food_per_meal;

import com.majcenovic.fitlog.data.meals.MealsInteractor;
import com.majcenovic.fitlog.data.meals.MealsListener;
import com.majcenovic.fitlog.data.models.Food;
import com.majcenovic.fitlog.data.models.dto.AddFoodPerMealDto;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public class AddFoodPerMealPresenter implements AddFoodPerMealContract.Presenter {

    AddFoodPerMealContract.View view;
    MealsInteractor interactor;

    public AddFoodPerMealPresenter(AddFoodPerMealContract.View view, MealsInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void addFoodPerMeal(AddFoodPerMealDto addFoodPerMealDto) {
        view.showProgress();
        interactor.postSingleFoodPerMeal(addFoodPerMealDto, new MealsListener.PostSingleFoodPerMealCallback() {
            @Override
            public void onSingleFoodPerMealAdded() {
                view.hideProgress();
                view.showFoodAdded();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void loadAllFoodByKeyword(String keyword) {
        view.showProgress();

        interactor.getAllFood(keyword, new MealsListener.GetAllFoodCallback() {
            @Override
            public void onAllFoodLoaded(List<Food> allFood) {
                view.showAllFoodLoadedAutoComplete(allFood);
                view.hideProgress();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }
}