package com.majcenovic.fitlog.ui.profile.people;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 07/09/2017.
 */

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> {

    private Context context;
    private List<User> list;
    private final User loggedUser;
    private PeopleClickListener peopleClickListener;

    public interface PeopleClickListener {
        void onUserClick(User user);

        void onFollowClick(User user);

        void onUnfollowClick(User user);
    }

    public PeopleAdapter(Context context, List<User> list, User loggedUser, PeopleClickListener peopleClickListener) {
        this.loggedUser = loggedUser;
        this.peopleClickListener = peopleClickListener;
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_people, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final User user = list.get(position);

        if(loggedUser.getId().equals(user.getId())) {
            holder.btnFollow.setVisibility(View.GONE);
            holder.btnUnfollow.setVisibility(View.GONE);
        } else {
            if (user.isFollowing()) {
                holder.btnUnfollow.setVisibility(View.VISIBLE);
                holder.btnFollow.setVisibility(View.GONE);
            } else {
                holder.btnUnfollow.setVisibility(View.GONE);
                holder.btnFollow.setVisibility(View.VISIBLE);
            }
        }



        holder.tvFullName.setText(user.getFullName());
        holder.btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                peopleClickListener.onFollowClick(user);
            }
        });

        holder.btnUnfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                peopleClickListener.onUnfollowClick(user);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                peopleClickListener.onUserClick(user);
            }
        });

        Picasso.with(context).load(user.getProfilePicture()).into(holder.rivProfileImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvFullName)
        TextView tvFullName;

        @BindView(R.id.btnFollow)
        Button btnFollow;

        @BindView(R.id.btnUnfollow)
        Button btnUnfollow;

        @BindView(R.id.rivProfileImage)
        RoundedImageView rivProfileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
