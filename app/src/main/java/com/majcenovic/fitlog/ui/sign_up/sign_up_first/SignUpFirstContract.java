package com.majcenovic.fitlog.ui.sign_up.sign_up_first;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;

import java.io.File;

/**
 * Created by martina on 27/06/2017.
 */

public interface SignUpFirstContract {

    interface View extends BaseView {
        void showInvalidInput();
        void showSignUpSecond(SignUpDto signUpDto);
        void showImagePicker();
        void showRequestPermissions();
    }

    interface Presenter extends BasePresenter {
        void submitNext(SignUpDto signUpDto);
        void pickImageClick(boolean isStorageGranted);
    }
}
