package com.majcenovic.fitlog.ui.main.messages.friends;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 07/09/2017.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private Context context;
    private List<User> list;
    private PeopleClickListener peopleClickListener;

    public interface PeopleClickListener {
        void onMessageClick(User user);
    }

    public FriendsAdapter(Context context, List<User> list, PeopleClickListener peopleClickListener) {
        this.peopleClickListener = peopleClickListener;
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_friends, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final User user = list.get(position);

        holder.btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                peopleClickListener.onMessageClick(user);
            }
        });

        holder.tvFullName.setText(user.getFullName());

        Picasso.with(context).load(user.getProfilePicture()).into(holder.rivProfileImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvFullName)
        TextView tvFullName;

        @BindView(R.id.btnMessage)
        Button btnMessage;

        @BindView(R.id.rivProfileImage)
        RoundedImageView rivProfileImage;

        @BindView(R.id.llFriendContainer)
        LinearLayout llFriendContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
