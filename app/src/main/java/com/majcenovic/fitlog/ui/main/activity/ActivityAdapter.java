package com.majcenovic.fitlog.ui.main.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.Meal;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.main.diary.meals.MealsAdapter;
import com.majcenovic.fitlog.util.ConverterUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by martina on 30/06/2017.
 */

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ViewHolder> {

    private Context context;
    private List<Activity> list;
    private OnShareClickListener onShareClickListener;
    private Integer mapWidth;
    private Integer mapHeight;

    public ActivityAdapter(Context context, List<Activity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ActivityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_activity, parent, false));
    }

    @Override
    public void onBindViewHolder(final ActivityAdapter.ViewHolder holder, int position) {
        final Activity activity = list.get(position);

        holder.tvActivityTitle.setText(getType(activity));
        holder.tvActivityBurnedCalories.setText(ConverterUtil.formatCal(activity.getCaloriesBurned()));
        holder.tvActivityDistance.setText(ConverterUtil.fromMetersToStandard((double)activity.getDistance()));
        holder.tvActivityDuration.setText(ConverterUtil.fromSecToStandard(activity.getDuration()));

        holder.mapActivity.onCreate(null);
        holder.mapActivity.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                MapsInitializer.initialize(context);
                holder.mapActivity.onResume();

                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        PolylineOptions options = new PolylineOptions().width(10).color(ContextCompat.getColor(context, R.color.accent));
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();

                        for (GpsLocation location : activity.getLocations()) {
                            LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
                            options.add(point);
                            builder.include(point);
                        }

                        if (options.getPoints() != null && !options.getPoints().isEmpty()) {
                            googleMap.addPolyline(options);
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(), context.getResources().getDimensionPixelSize(R.dimen.spacing_normal));
                            googleMap.moveCamera(cu);
                        }

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvActivityTitle)
        TextView tvActivityTitle;

        @BindView(R.id.tvActivityBurnedCalories)
        TextView tvActivityBurnedCalories;

        @BindView(R.id.tvActivityDistance)
        TextView tvActivityDistance;

        @BindView(R.id.tvActivityDuration)
        TextView tvActivityDuration;

        @BindView(R.id.mapActivity)
        MapView mapActivity;

        @BindView(R.id.ibActivityShare)
        ImageButton ibActivityShare;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ibActivityShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onShareClickListener.onClick(list.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }

    public interface OnShareClickListener {

        void onClick(Activity activity, int position);

    }

    private String getType(Activity activity) {
        if (activity.getType().equals("walking")) return context.getString(R.string.walking);
        else if (activity.getType().equals("running")) return context.getString(R.string.running);
        else return context.getString(R.string.cycling);

    }

    public void setOnShareClickListener(ActivityAdapter.OnShareClickListener onShareClickListener) {
        this.onShareClickListener = onShareClickListener;
    }


}
