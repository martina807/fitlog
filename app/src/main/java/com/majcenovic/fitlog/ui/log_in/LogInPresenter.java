package com.majcenovic.fitlog.ui.log_in;

import com.majcenovic.fitlog.data.log_in.LogInInteractor;
import com.majcenovic.fitlog.data.log_in.LogInListener;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.util.InputValidationUtil;

/**
 * Created by martina on 21/06/2017.
 */

public class LogInPresenter implements LogInContract.Presenter {

    private LogInContract.View view;
    private LogInInteractor interactor;

    private final int minPasswordLength = 6;

    public LogInPresenter(LogInContract.View view, LogInInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void submitLogIn(String email, String password) {
        if(InputValidationUtil.isEmailValid(email) && InputValidationUtil.isPasswordValid(password)) {
            view.showProgress();

            interactor.loginUser(email, password, new LogInListener() {
                @Override
                public void onLoggedIn(User user) {
                    view.hideProgress();
                    view.showMain();
                }

                @Override
                public void onFailure(String message) {
                    view.hideProgress();
                    view.showError(message);
                }
            });
        } else {
            view.showInvalidInput();
        }
    }

    @Override
    public void startSignUp() {
        view.showSignUp();
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }
}
