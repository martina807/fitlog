package com.majcenovic.fitlog.ui.comments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Comment;
import com.majcenovic.fitlog.data.models.UserFood;
import com.majcenovic.fitlog.util.DateUtil;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 30/06/2017.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private Context context;
    private List<Comment> list = new ArrayList<>();
    private CommentClickListener commentClickListener;

    public CommentsAdapter(Context context, CommentClickListener commentClickListener) {
        this.context = context;
        this.commentClickListener = commentClickListener;
    }

    public void updateData(List<Comment> comments) {
        this.list.clear();
        if (comments != null) {
            this.list.addAll(comments);
        }
        notifyDataSetChanged();
    }

    public interface CommentClickListener {
        void onLikeDislikeClick(Comment comment);
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder holder, int position) {
        final Comment comment = list.get(position);

        if (comment.isLiked()) {
            holder.btnLike.setImageResource(R.drawable.like_filled);
        } else {
            holder.btnLike.setImageResource(R.drawable.like);
        }

        Picasso.with(context).load(comment.getUser().getProfilePicture()).into(holder.rivUserImage);
        holder.tvUserNameComment.setText(Html.fromHtml(getCommentString(comment)));
        holder.tvLikeCount.setText(String.valueOf(comment.getLikesCount()));
        holder.tvCreatedAt.setText(DateUtil.toNiceDateAndTimeFormat(comment.getCreatedAt()));
    }

    private String getCommentString(Comment comment) {
        return "<font color='#2196F3'><b>" + comment.getUser().getFullName() + "</b></font>" + " " + comment.getMessage();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rivUserImage)
        RoundedImageView rivUserImage;

        @BindView(R.id.tvUserNameComment)
        TextView tvUserNameComment;

        @BindView(R.id.btnLike)
        ImageButton btnLike;

        @BindView(R.id.tvLikeCount)
        TextView tvLikeCount;

        @BindView(R.id.tvCreatedAt)
        TextView tvCreatedAt;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btnLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    commentClickListener.onLikeDislikeClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}