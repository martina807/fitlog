package com.majcenovic.fitlog.ui.goals;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Goal;
import com.majcenovic.fitlog.data.models.Meal;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public interface GoalsContract {

    interface View extends BaseView {
        void showGoals(Goal goal);
        void showUpdateDialog(Goal goal, int weightTypeId);
    }

    interface Presenter extends BasePresenter {
        void loadGoals();
        void openUpdateDialog(Goal goal, int weightType);
    }
}