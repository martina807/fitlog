package com.majcenovic.fitlog.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.log_in.LogInActivity;
import com.majcenovic.fitlog.ui.main.MainActivity;

/**
 * Created by martina on 26/06/2017.
 */

public class StartingPointActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        if (SharedPrefs.readUserData(this) != null) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        } else {
            startActivity(new Intent(getApplicationContext(), LogInActivity.class));
        }

        finish();

        super.onCreate(savedInstanceState);

    }
}