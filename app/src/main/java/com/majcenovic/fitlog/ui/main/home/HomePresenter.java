package com.majcenovic.fitlog.ui.main.home;

import com.majcenovic.fitlog.data.home.HomeInteractor;
import com.majcenovic.fitlog.data.home.HomeListener;
import com.majcenovic.fitlog.data.models.CaloriesStatus;
import com.majcenovic.fitlog.data.models.Post;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View view;
    private HomeInteractor interactor;

    public HomePresenter(HomeContract.View view, HomeInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadCaloriesStatus() {
        view.showProgress();
        interactor.getCaloriesStatus(new HomeListener.CalorieStatusCallback() {
            @Override
            public void onCaloriesStatusLoaded(CaloriesStatus caloriesStatus) {
                view.hideProgress();
                view.showCaloriesStatus(caloriesStatus);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void loadNews() {
        view.showProgress();
        interactor.getNews(new HomeListener.PostsListener() {
            @Override
            public void onNewsLoaded(List<Post> posts) {
                view.hideProgress();
                view.showNews(posts);
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void openStatusEditor() {
        view.showStatusEditor();
    }

    @Override
    public void openUser(User user) {
        view.showUser(user);
    }

    @Override
    public void likeOrDislike(final Post post) {
        if(post.isLiked()) {
            interactor.dislike(post, new HomeListener.DislikeCallback() {
                @Override
                public void onDislikePosted() {
                    post.setLiked(!post.isLiked());
                    post.decrementLikes();
                    view.refreshData(post);
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        } else {
            interactor.like(post, new HomeListener.LikeCallback() {
                @Override
                public void onLikePosted() {
                    post.setLiked(!post.isLiked());
                    post.incrementLikes();
                    view.refreshData(post);
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        }
    }

    @Override
    public void openComments(Post post) {
        view.showComments(post);
    }
}
