package com.majcenovic.fitlog.ui.main.messages.friends;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.User;

import java.util.List;

/**
 * Created by martina on 14/08/2017.
 */

public interface FriendsContract {

    interface View extends BaseView {
        void showPeople(List<User> people);
        void showChat(User user);
    }

    interface Presenter extends BasePresenter {
        void loadPeople(Long userId);
        void startChat(User user);
    }
}