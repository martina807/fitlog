package com.majcenovic.fitlog.ui.main.activity.running_activity;


import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.GpsLocation;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by martina on 02/08/2017.
 */

public class LocationService extends JobService {

    private static final String TAG = LocationService.class.getSimpleName();
    public static Activity activity;

    private static LocationManager locationManager;
    private static final int LOCATION_INTERVAL = 2;
    private static final float LOCATION_DISTANCE = 5;
    private static GpsLocationListener listener;
    private static JobParameters currentJob;

    @Override
    public boolean onStartJob(JobParameters job) {

        this.currentJob = job;

        initializeLocationManager();
        collectLocations();

        return true;
    }

    public static void cancelJob(Context context) {

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        //Cancel all the jobs for this package
        dispatcher.cancelAll();
        // Cancel the job for this tag
        dispatcher.cancel("CollectingLocationJob");

        activity = null;
        if (listener != null) {
            listener.cancel();
        }
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

    private void collectLocations() {
        try {
            listener = new GpsLocationListener(LocationManager.GPS_PROVIDER, activity, new GpsLocationListener.ActivityEndCallback() {
                @Override
                public void onEnded() {
                    jobFinished(currentJob, false);
                    if (listener != null && locationManager != null) {
                        locationManager.removeUpdates(listener);
                        locationManager = null;
                        listener = null;
                    }
                    currentJob = null;
                }
            });
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, listener);
        } catch (java.lang.SecurityException ex) {
            jobFinished(currentJob, false);
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            jobFinished(currentJob, false);
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    public static void scheduleJob(Context context, Activity activity) {
        LocationService.activity = activity;

        //creating new firebase job dispatcher
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        //creating new job and adding it with dispatcher
        Job job = createJob(dispatcher);
        dispatcher.mustSchedule(job);
    }

    public static Job createJob(FirebaseJobDispatcher dispatcher) {

        Job job = dispatcher.newJobBuilder()
                //persist the task across boots
                .setLifetime(Lifetime.FOREVER)
                //.setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                //call this service when the criteria are met.
                .setService(LocationService.class)
                //unique id of the task
                .setTag("CollectingLocationJob")
                //don't overwrite an existing job with the same tag
                .setReplaceCurrent(false)
                // We are mentioning that the job is periodic.
                .setRecurring(true)
                // Run between 1 - 10 seconds from now.
                .setTrigger(Trigger.executionWindow(0, 0))
                // retry with exponential backoff
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                //.setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                //Run this job only when the network is available.
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
        return job;
    }
}

