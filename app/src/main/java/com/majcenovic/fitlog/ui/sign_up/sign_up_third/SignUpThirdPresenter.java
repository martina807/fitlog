package com.majcenovic.fitlog.ui.sign_up.sign_up_third;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.data.sign_up.SignUpInteractor;
import com.majcenovic.fitlog.data.sign_up.SignUpListener;
import com.majcenovic.fitlog.util.InputValidationUtil;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpThirdPresenter implements SignUpThirdContract.Presenter {

    private SignUpThirdContract.View view;
    private SignUpInteractor interactor;

    public SignUpThirdPresenter(SignUpThirdContract.View view, SignUpInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void cancel() {
    }

    @Override
    public void submitSignUp(SignUpDto signUpDto) {
        if (InputValidationUtil.isActivityLevelValid(signUpDto.getActivityLevel())
                && InputValidationUtil.isWeeklyGoalWeightValid(signUpDto.getWeeklyGoalWeight())
                && InputValidationUtil.isGoalWeightValid(String.valueOf(signUpDto.getGoalWeight()))) {

            view.showProgress();
            interactor.signUpUser(signUpDto, new SignUpListener() {
                @Override
                public void onSignedUp(User user) {
                    view.hideProgress();
                    view.showMain();
                }

                @Override
                public void onFailure(String message) {
                    view.hideProgress();
                    view.showError(message);
                }
            });
        } else {
            view.showInvalidInput();
        }
    }
}
