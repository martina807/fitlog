package com.majcenovic.fitlog.ui.profile.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 14/08/2017.
 */

public class UserDataViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvProfileName)
    TextView tvProfileName;

    @BindView(R.id.btnProfileMessage)
    Button btnProfileMessage;

    @BindView(R.id.tvProfileDesc)
    TextView tvProfileDesc;

    @BindView(R.id.btnProfileEdit)
    ImageButton btnProfileEdit;

    @BindView(R.id.btnProfileFollow)
    ImageButton btnProfileFollow;

    @BindView(R.id.btnProfileFollowed)
    ImageButton btnProfileFollowed;

    @BindView(R.id.rivProfileImage)
    RoundedImageView rivProfileImage;

    public UserDataViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getTvProfileName() {
        return tvProfileName;
    }

    public Button getBtnProfileMessage() {
        return btnProfileMessage;
    }

    public TextView getTvProfileDesc() {
        return tvProfileDesc;
    }

    public ImageButton getBtnProfileEdit() {
        return btnProfileEdit;
    }

    public ImageButton getBtnProfileFollow() {
        return btnProfileFollow;
    }

    public ImageButton getBtnProfileFollowed() {
        return btnProfileFollowed;
    }

    public RoundedImageView getRivProfileImage() {
        return rivProfileImage;
    }
}
