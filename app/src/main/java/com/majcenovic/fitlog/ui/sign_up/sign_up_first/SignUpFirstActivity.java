package com.majcenovic.fitlog.ui.sign_up.sign_up_first;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.ImageView;

import com.majcenovic.fitlog.BuildConfig;
import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.sign_up.sign_up_second.SignUpSecondActivity;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.majcenovic.fitlog.util.ImageUtil;
import com.majcenovic.fitlog.util.PermissionUtil;
import com.mvc.imagepicker.ImagePicker;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 26/06/2017.
 */

public class SignUpFirstActivity extends BaseActivity implements SignUpFirstContract.View {

    @BindView(R.id.etSingUpFirstName)
    EditText etSignUpFirstName;

    @BindView(R.id.etSingUpLastName)
    EditText etSignUpLastName;

    @BindView(R.id.etSingUpEmail)
    EditText etSignUpEmail;

    @BindView(R.id.etSingUpPassword)
    EditText etSignUpPassword;

    @BindView(R.id.ivSingUpProfileImage)
    ImageView ivSingUpProfileImage;

    private SignUpFirstContract.Presenter presenter;

    private File profileImageFile;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_up_first;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            etSignUpFirstName.setText("James");
            etSignUpLastName.setText("Wick");
            etSignUpEmail.setText("james@wick.com");
            etSignUpPassword.setText("123456");
        }

        initToolbar(R.string.sign_up);

        presenter = Injection.provideSignUpFirstPresenter(this);
    }

    @Override
    public void showInvalidInput() {
        showPopUp(R.string.invalid_input);
    }

    @Override
    public void showSignUpSecond(SignUpDto signUpDto) {
        startActivity(new Intent(getApplicationContext(), SignUpSecondActivity.class)
                .putExtra(ExtraUtil.SIGN_UP_DTO, signUpDto));
    }

    @Override
    public void showImagePicker() {
        ImagePicker.pickImage((Activity) context);
    }

    @Override
    public void showRequestPermissions() {
        PermissionUtil.requestStoragePermissions(context);
    }

    @OnClick(R.id.btnSignUpNext)
    public void nextClick() {
        SignUpDto signUpDto = new SignUpDto(
                etSignUpFirstName.getText().toString(),
                etSignUpLastName.getText().toString(),
                etSignUpPassword.getText().toString(),
                etSignUpEmail.getText().toString(),
                profileImageFile
        );

        presenter.submitNext(signUpDto);
    }

    @OnClick(R.id.ivSingUpProfileImage)
    void profilePicClick() {
        presenter.pickImageClick(PermissionUtil.isStorageGranted(context));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.STORAGE_PERMISSIONS_REQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            presenter.pickImageClick(PermissionUtil.isStorageGranted(context));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = ImagePicker.getImageFromResult(context, requestCode, resultCode, data);
            ivSingUpProfileImage.setImageBitmap(bitmap);
            profileImageFile = ImageUtil.convertBitmapToFile(context, bitmap);
        }
    }

    @Override
    protected void onDestroy() {
        presenter.cancel();
        super.onDestroy();
    }
}
