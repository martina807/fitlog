package com.majcenovic.fitlog.ui.profile.gallery.photo;

import com.majcenovic.fitlog.BasePresenter;
import com.majcenovic.fitlog.BaseView;
import com.majcenovic.fitlog.data.models.Photo;
import com.majcenovic.fitlog.data.models.Post;

/**
 * Created by martina on 14/08/2017.
 */

public interface PhotoContract {

    interface View extends BaseView {
        void showComments(Post post);
        void refreshData();
        void showPost(Post post);
    }

    interface Presenter extends BasePresenter {
        void likeDislikePost(Post post);
        void openComments(Post post);
        void loadPost(Long postId);
    }
}