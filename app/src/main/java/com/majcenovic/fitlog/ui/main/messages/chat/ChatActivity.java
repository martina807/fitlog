package com.majcenovic.fitlog.ui.main.messages.chat;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.ImageButton;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Message;
import com.majcenovic.fitlog.data.models.MessageEvent;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.util.ExtraUtil;
import com.majcenovic.fitlog.util.NotificationTypeUtil;
import com.majcenovic.fitlog.util.PushUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 07/08/2017.
 */

public class ChatActivity extends BaseActivity implements ChatContract.View {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.etMessage)
    EditText etMessage;

    @BindView(R.id.ibSend)
    ImageButton ibSend;

    private final long SCALE_ANIM_DURATION = 150;
    private LinearLayoutManager layoutManager;
    private ChatRecyclerViewAdapter recyclerViewAdapter;
    private User friend;
    private ChatContract.Presenter presenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        friend = getIntent().getParcelableExtra(ExtraUtil.OBJECT);
        presenter = Injection.provideChatPresenter(this, context, friend);
        presenter.loadFriend();
        presenter.setUpTitle();
    }

    private void initViews() {
        initToolbar(R.string.chat);
        initList();
        initTextListener();
    }

    private void initList() {
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
    }

    private void initTextListener() {
        ibSend.setEnabled(false);
        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etMessage.getText().toString().trim().length() > 0) {
                    if (!ibSend.isEnabled()) {
                        ibSend.setEnabled(true);
                        animateSendButton(R.drawable.send_hangouts_active);
                    }
                } else {
                    if (ibSend.isEnabled()) {
                        ibSend.setEnabled(false);
                        animateSendButton(R.drawable.send_hangouts_inactive);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        presenter.start();
        presenter.loadNewerMessages();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        presenter.stop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getOpenLocationId() != null && event.getOpenLocationId().equals(friend.getId())) {
            presenter.loadNewerMessages();
        }
    }

    private void animateSendButton(final int drawableToShow) {
        final ScaleAnimation scaleUp = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF, (float) 0.5);
        scaleUp.setDuration(SCALE_ANIM_DURATION);
        ScaleAnimation scaleDown = new ScaleAnimation(1, 0, 1, 0, Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF, (float) 0.5);
        scaleDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ibSend.setImageResource(drawableToShow);
                ibSend.startAnimation(scaleUp);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        scaleDown.setDuration(SCALE_ANIM_DURATION);
        ibSend.startAnimation(scaleDown);
    }

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        int visibleItemCount = layoutManager.getChildCount();
                        int totalItemCount = layoutManager.getItemCount();
                        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                        presenter.scrollUpStop(firstVisibleItemPosition, visibleItemCount, totalItemCount);
                    }
                });
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
        }
    };

    @OnClick(R.id.ibSend)
    void sendClick() {
        presenter.postMessage(etMessage.getText().toString().trim());
    }

    @Override
    public void showClearedInput() {
        etMessage.setText("");
    }

    @Override
    public void showTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void showFriendUpdate(User friend) {
        if (recyclerViewAdapter != null) {
            recyclerViewAdapter.setFriend(friend);
            recyclerViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showNewMessageUpdate(int messagesSize) {
        PushUtil.clearPushes(getApplicationContext(), NotificationTypeUtil.CHAT, friend.getId());
        recyclerViewAdapter.notifyItemInserted(0);
        recyclerViewAdapter.notifyItemRangeChanged(0, messagesSize);
        if (layoutManager.findFirstVisibleItemPosition() < 5) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void showNewMessageUpdateOnTop(int messagesSize) {
        recyclerViewAdapter.notifyItemInserted(messagesSize);
    }

    @Override
    public void showRefreshDateTime() {
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted() && !isFinishing() && context != null) {
                        Thread.sleep(1000 * 60);
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (recyclerViewAdapter != null) {
                                        recyclerViewAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                        } catch (Throwable ex) {
                            ex.printStackTrace();
                        }
                    }
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            }
        };
        t.start();
    }

    @Override
    public void initializeAdapter(List<Message> messages) {
        if (recyclerViewAdapter == null) {
            recyclerViewAdapter = new ChatRecyclerViewAdapter(context, messages, friend);
            recyclerView.addOnScrollListener(onScrollListener);
            recyclerView.setAdapter(recyclerViewAdapter);
        }
    }
}