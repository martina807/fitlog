package com.majcenovic.fitlog.ui.main.messages.chat;

import com.majcenovic.fitlog.data.inbox.ChatInteractor;
import com.majcenovic.fitlog.data.inbox.InboxListener;
import com.majcenovic.fitlog.data.models.Message;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.util.MessagesParameterTypeUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by martina on 28/06/2017.
 */

public class ChatPresenter implements ChatContract.Presenter {

    private ChatContract.View view;
    private ChatInteractor interactor;
    private User friend;

    private List<Message> listItems;
    private boolean isLoading = false;
    private boolean autoRefreshStarted = false;

    public ChatPresenter(ChatContract.View view, ChatInteractor interactor, User friend) {
        this.view = view;
        this.interactor = interactor;
        this.friend = friend;
    }

    @Override
    public void cancel() {
        view.hideProgress();
        interactor.cancel();
    }

    @Override
    public void loadFriend() {
        if (friend.getProfilePicture() == null) {
            interactor.getUserById(friend.getId(), new InboxListener.UserListener() {
                @Override
                public void onUserLoaded(User friend) {
                    view.showFriendUpdate(friend);
                }

                @Override
                public void onFailure(String message) {
                    view.showError(message);
                }
            });
        }
    }

    @Override
    public void postMessage(String message) {
        view.showClearedInput();
        interactor.postMessage(friend.getId(), message, new InboxListener.CreateChatMessageListener() {
            @Override
            public void onMessagePosted(Message message) {
                if (!isMessageInList(message)) {
                    if (listItems == null) {
                        listItems = new ArrayList<>();
                        view.initializeAdapter(listItems);
                    }
                    listItems.add(0, message);
                    view.showNewMessageUpdate(listItems.size());
                }
            }

            @Override
            public void onFailure(String message) {
                view.showError(message);
            }
        });
    }

    @Override
    public void loadOlderMessages() {
        if (listItems == null || listItems.size() == 0 || isLoading) return;
        isLoading = true;
        Message m = listItems.get(listItems.size() - 1);
        interactor.getMessagesWithUser(friend.getId(), m.getId(), MessagesParameterTypeUtil.OLDER, new InboxListener.GetChatMessagesListener() {
            @Override
            public void onMessagesReceived(List<Message> messages) {
                isLoading = false;
                view.hideProgress();
                if (messages != null && !messages.isEmpty()) {
                    for (Message m : messages) {
                        if (listItems == null) {
                            listItems = new ArrayList<>();
                            view.initializeAdapter(listItems);
                            if (!autoRefreshStarted) {
                                autoRefreshStarted = true;
                                view.showRefreshDateTime();
                            }
                        }
                        listItems.add(m);
                        view.showNewMessageUpdateOnTop(listItems.size());
                    }
                }
            }

            @Override
            public void onFailure(String message) {
                isLoading = false;
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void loadNewerMessages() {
        if (listItems == null) {
            view.showProgress();
        }
        isLoading = true;
        interactor.getMessagesWithUser(friend.getId(), getLowestMessageIdFromList(), MessagesParameterTypeUtil.NEWER, new InboxListener.GetChatMessagesListener() {
            @Override
            public void onMessagesReceived(List<Message> messages) {
                isLoading = false;
                view.hideProgress();
                if (messages != null && !messages.isEmpty()) {
                    Collections.reverse(messages);
                    for (Message m : messages) {
                        if (listItems == null) {
                            listItems = new ArrayList<>();
                            view.initializeAdapter(listItems);
                            if (!autoRefreshStarted) {
                                autoRefreshStarted = true;
                                view.showRefreshDateTime();
                            }
                        }
                        if (!isMessageInList(m)) {
                            listItems.add(0, m);
                            view.showNewMessageUpdate(listItems.size());
                        }
                    }
                }
                view.showFriendUpdate(friend);
            }

            @Override
            public void onFailure(String message) {
                isLoading = false;
                view.hideProgress();
                view.showError(message);
            }
        });
    }

    @Override
    public void setUpTitle() {
        if (friend.getChatTitle() != null) {
            view.showTitle(friend.getChatTitle());
        } else {
            view.showTitle(friend.getFullName());
        }
    }

    @Override
    public void scrollUpStop(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (!isLoading && listItems != null && listItems.size() > 1) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount) {
                loadOlderMessages();
            }
        }
    }

    @Override
    public void start() {
        interactor.chatStarted(friend.getId());
    }

    @Override
    public void stop() {
        interactor.chatStopped(friend.getId());
    }

    private Long getLowestMessageIdFromList() {
        if (listItems == null || listItems.size() == 0) return null;
        if (listItems.get(0).getId() != null) return listItems.get(0).getId();
        return null;
    }

    private boolean isMessageInList(Message messageModel) {
        if (listItems == null || messageModel == null || listItems.size() == 0) return false;
        for (int i = 0; i < listItems.size(); i++) {
            Message m = listItems.get(i);
            if (m.getId() != null && messageModel.getId() != null && m.getId().equals(messageModel.getId())) {
                return true;
            }
        }
        return false;
    }
}