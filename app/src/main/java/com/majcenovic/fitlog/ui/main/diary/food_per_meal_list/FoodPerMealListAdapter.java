package com.majcenovic.fitlog.ui.main.diary.food_per_meal_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.UserFood;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 30/06/2017.
 */

public class FoodPerMealListAdapter extends RecyclerView.Adapter<FoodPerMealListAdapter.ViewHolder> {

    private Context context;
    private List<UserFood> list;
    private OnDeleteFoodClickListener onDeleteFoodClickListener;

    public FoodPerMealListAdapter(Context context, List<UserFood> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public FoodPerMealListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_meals, parent, false));
    }

    @Override
    public void onBindViewHolder(FoodPerMealListAdapter.ViewHolder holder, int position) {
        final UserFood food = list.get(position);

        holder.tvMealsMealTitle.setText(food.getFood().getTitle());
        holder.tvCaloriesPerMeal.setText(String.valueOf(food.getCalories()));
        holder.ibMealDelete.setImageResource(R.drawable.ic_delete_24dp);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvRowTitle)
        TextView tvMealsMealTitle;

        @BindView(R.id.tvRowDescription)
        TextView tvCaloriesPerMeal;

        @BindView(R.id.ibRowButton)
        ImageButton ibMealDelete;

        @BindView(R.id.llMeal)
        LinearLayout llMeal;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ibMealDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onDeleteFoodClickListener != null) {
                        onDeleteFoodClickListener.onClick(list.get(getAdapterPosition()), getAdapterPosition());
                    }
                }
            });
        }
    }

    public interface OnDeleteFoodClickListener {

        void onClick(UserFood food, int position);
    }

    public void setOnDeleteFoodClickListener(OnDeleteFoodClickListener onDeleteFoodClickListener) {
        this.onDeleteFoodClickListener = onDeleteFoodClickListener;
    }

}
