package com.majcenovic.fitlog.ui.main.diary.meals;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.Meal;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by martina on 30/06/2017.
 */

public class MealsAdapter extends RecyclerView.Adapter<MealsAdapter.ViewHolder> {

    private Context context;
    private List<Meal> list;
    private OnAddMealClickListener onAddMealClickListener;
    private OnMealClickListener onMealClickListener;

    public MealsAdapter(Context context, List<Meal> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public MealsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_meals, parent, false));
    }

    @Override
    public void onBindViewHolder(MealsAdapter.ViewHolder holder, int position) {
        final Meal meal = list.get(position);

        holder.tvMealsMealTitle.setText(meal.getTitle());
        holder.tvCaloriesPerMeal.setText(String.valueOf(meal.getCalories()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvRowTitle)
        TextView tvMealsMealTitle;

        @BindView(R.id.tvRowDescription)
        TextView tvCaloriesPerMeal;

        @BindView(R.id.ibRowButton)
        ImageButton ibMealAdd;

        @BindView(R.id.llMeal)
        LinearLayout llMeal;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ibMealAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onAddMealClickListener != null) {
                        onAddMealClickListener.onClick(list.get(getAdapterPosition()), getAdapterPosition());
                    }
                }
            });

            llMeal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onMealClickListener != null) {
                        onMealClickListener.onClick(list.get(getAdapterPosition()), getAdapterPosition());
                    }
                }
            });
        }
    }

    public interface OnAddMealClickListener {

        void onClick(Meal meal, int position);
    }

    public interface OnMealClickListener {

        void onClick(Meal meal, int position);
    }

    public void setOnAddMealClickListener(OnAddMealClickListener onAddMealClickListener) {
        this.onAddMealClickListener = onAddMealClickListener;
    }

    public void setOnMealClickListener(OnMealClickListener onMealClickListener) {
        this.onMealClickListener = onMealClickListener;
    }
}
