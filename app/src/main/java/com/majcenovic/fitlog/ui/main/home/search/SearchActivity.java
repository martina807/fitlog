package com.majcenovic.fitlog.ui.main.home.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.majcenovic.fitlog.R;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.di.Injection;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.ui.BaseActivity;
import com.majcenovic.fitlog.ui.profile.ProfileActivity;
import com.majcenovic.fitlog.util.ExtraUtil;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by martina on 09/09/2017.
 */

public class SearchActivity extends BaseActivity implements SearchContract.View {

    @BindView(R.id.etSearchInput)
    EditText etSearchInput;

    @BindView(R.id.rvListSearch)
    RecyclerView rvList;

    @BindView(R.id.pbSearch)
    ProgressBar pbSearch;

    @BindView(R.id.ibSearchBack)
    ImageButton ibSearchBack;

    @BindView(R.id.ibSearchClear)
    ImageButton ibSearchClear;

    private SearchContract.Presenter presenter;
    private SearchAdapter listAdapter;
    private Timer timer;
    private User me;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideParentToolbar();
        setStatusBarWhiteColor();

        initTextWatcher();

        rvList.setLayoutManager(new LinearLayoutManager(context));

        me = SharedPrefs.readUserData(context);

        presenter = Injection.provideSearchPresenter(this, context);
    }

    private void initTextWatcher() {
        etSearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etSearchInput.length() > 0) {
                    ibSearchClear.animate().alpha(1);
                } else {
                    ibSearchClear.animate().alpha(0);
                }

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                presenter.search(etSearchInput.getText().toString().trim());
                            }
                        });
                    }
                }, 600);

            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed(etSearchInput.getText().toString());
    }

    @Override
    public void showPeople(List<User> users) {
        if (listAdapter == null) {
            listAdapter = new SearchAdapter(context, me, createSearchClickListener());
        }

        if (rvList.getAdapter() == null) {
            rvList.setAdapter(listAdapter);
        }

        listAdapter.updateData(users);
    }

    private SearchAdapter.SearchClickListener createSearchClickListener() {
        return new SearchAdapter.SearchClickListener() {
            @Override
            public void onUserClick(User user) {
                presenter.openUserDetails(user);
            }

            @Override
            public void onFollowUnfollowClick(User user) {
                presenter.followUnfollow(user);
            }
        };
    }

    @Override
    public void showProgress() {
        pbSearch.animate().alpha(1);
    }

    @Override
    public void hideProgress() {
        if (pbSearch != null && context != null && !isFinishing()) {
            pbSearch.animate().alpha(0);
        }
    }

    @Override
    public void clearInput() {
        etSearchInput.setText("");
    }

    @Override
    public void showDefaultBackBehavior() {
        super.onBackPressed();
    }

    @Override
    public void showUserDetails(User user) {
        startActivity(new Intent(getApplicationContext(), ProfileActivity.class)
                .putExtra(ExtraUtil.USER, user));
        finish();
    }

    @Override
    public void updateFollowState(User user) {
        listAdapter.updateUserState(user);
    }

    @OnClick(R.id.ibSearchClear)
    void searchClearClick() {
        clearInput();
    }

    @OnClick(R.id.ibSearchBack)
    void searchBackClick() {
        presenter.onBackPressed(etSearchInput.getText().toString());
    }
}