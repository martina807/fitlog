package com.majcenovic.fitlog.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.majcenovic.fitlog.BaseApplication;
import com.majcenovic.fitlog.data.models.Activity;
import com.majcenovic.fitlog.data.models.GpsLocation;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.UpdateLocationDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martina on 11/01/2017.
 */
public class SharedPrefs {

    private static final String FILE_NAME = "shared_preferences";
    private static final String PREFS_USER_DATA = "userData";
    private static final String PREFS_ACTIVITY_LOCATIONS = "activityLocationsData";
    private static final String TIMER_STARTED_AT = "timerStartedAt";
    private static final String IS_PUSH_TOKEN_SENT = "isPushTokenSent";
    private static final String PREFS_ACTIVITY_DATA = "prefsActivityData";

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return getSharedPreferences(context).edit();
    }

    public static <T> String toJson(T object) {
        return (new Gson()).toJson(object);
    }

    public static <T> T fromJson(String object, TypeToken<T> typeToken) {
        return (new Gson()).fromJson(object, typeToken.getType());
    }

    public static <T> void saveObject(Context context, String key, T value) {
        if (context == null) return;
        save(context, key, toJson(value));
    }

    public static <T> T readObject(Context context, String key, TypeToken<T> typeToken) {
        if (context == null) return null;
        T object = null;
        String objectRaw = read(context, key);
        if (objectRaw != null) object = fromJson(objectRaw, typeToken);
        return object;
    }

    public static void remove(Context context, String name) {
        getEditor(context).remove(name).apply();
    }

    public static void clear(Context context) {
        getEditor(context).clear().apply();
    }

    private static void save(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();
    }

    private static void save(Context context, String key, Integer value) {
        getEditor(context).putInt(key, value).apply();
    }

    private static void save(Context context, String key, Long value) {
        getEditor(context).putLong(key, value).apply();
    }

    public static void saveBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    public static boolean readBoolean(Context context, String key) {
        return getSharedPreferences(context).getBoolean(key, false);
    }

    private static String read(Context context, String key) {
        return getSharedPreferences(context).getString(key, null);
    }

    public static void saveUserData(Context context, User object) {
        saveObject(context, PREFS_USER_DATA, object);
    }

    public static User readUserData(Context context) {
        return readObject(context, PREFS_USER_DATA, new TypeToken<User>() {
        });
    }

    public static void saveActivityLocation(UpdateLocationDto object) {

        List<UpdateLocationDto> gpsLocations = readActivityLocations();

        if (gpsLocations == null) {
            gpsLocations = new ArrayList<>();
        }

        gpsLocations.add(object);

        saveObject(BaseApplication.appContext, PREFS_ACTIVITY_LOCATIONS, gpsLocations);
    }

    public static List<UpdateLocationDto> readActivityLocations() {
        return readObject(BaseApplication.appContext, PREFS_ACTIVITY_LOCATIONS, new TypeToken<List<UpdateLocationDto>>() {
        });
    }

    public static void saveTimerStartedAt(Context context, Long seconds) {
        save(context, TIMER_STARTED_AT, seconds);
    }

    public static Long readTimerStartedAt(Context context) {
        Long timer = getSharedPreferences(context).getLong(TIMER_STARTED_AT, -1L);
        if (timer == -1L) return null;
        return timer;
    }

    public static boolean isPushTokenSent(Context context) {
        return getSharedPreferences(context).getBoolean(IS_PUSH_TOKEN_SENT, false);
    }

    public static void setPushTokenSent(Context context) {
        saveBoolean(context, IS_PUSH_TOKEN_SENT, true);
    }

    public static void saveActivityInProgressData(Context context, Activity object) {
        saveObject(context, PREFS_ACTIVITY_DATA, object);
    }

    public static Activity readActivityInProgressData(Context context) {
        return readObject(context, PREFS_ACTIVITY_DATA, new TypeToken<Activity>() {
        });
    }

    public static void removeActivityInProgressData(Context context) {
        remove(context, PREFS_ACTIVITY_DATA);
    }

    public static void removeActivityLocations(Context context) {
        remove(context, PREFS_ACTIVITY_LOCATIONS);
    }

    public static void removeTimerStartedAt(Context context) {
        remove(context, TIMER_STARTED_AT);
    }
}