package com.majcenovic.fitlog.ui.log_in;

import com.majcenovic.fitlog.FakeData;
import com.majcenovic.fitlog.data.log_in.LogInInteractor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by martina on 26/06/2017.
 */

public class LogInPresenterTest {

    @Mock
    private LogInInteractor interactor;

    @Mock
    private LogInContract.View view;

    private LogInContract.Presenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        presenter = new LogInPresenter(view, new FakeLogInInteractorImpl());
    }

    @Test
    public void whenLogInSubmitInputMissing_showInvalidInput() {
        presenter.submitLogIn("", "");
        verify(view).showInvalidInput();
    }

    @Test
    public void whenLogInSubmitInputInvalid_showInvalidInput() {
        presenter.submitLogIn(FakeData.invalidEmail, FakeData.validPassword);

        verify(view, never()).showProgress();

        verify(view).showInvalidInput();
    }

    @Test
    public void whenLogInSubmitSuccess_showMain() {
        presenter.submitLogIn(FakeData.validExistingEmail, FakeData.validPassword);

        verify(view).showProgress();
        verify(view).hideProgress();

        verify(view).showMain();
    }

    @Test
    public void whenLogInSubmitEmailNotExistError_showError() {
        presenter.submitLogIn(FakeData.validNotExistingEmail, FakeData.validPassword);

        verify(view).showProgress();
        verify(view).hideProgress();

        verify(view).showError(FakeData.errorEmailNotExist);
    }

    @Test
    public void whenLogInSubmitUnknownError_showError() {
        presenter.submitLogIn(FakeData.validExistingShouldErrorEmail, FakeData.validPassword);

        verify(view).showProgress();
        verify(view).hideProgress();

        verify(view).showError(FakeData.errorUnknown);
    }

    @Test
    public void whenStartSignUp_showSignUp() {
        presenter.startSignUp();

        verify(view, never()).showProgress();

        verify(view).showSignUp();
    }
}
