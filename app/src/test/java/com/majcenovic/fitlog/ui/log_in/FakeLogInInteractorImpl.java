package com.majcenovic.fitlog.ui.log_in;

import com.majcenovic.fitlog.FakeData;
import com.majcenovic.fitlog.data.log_in.LogInInteractor;
import com.majcenovic.fitlog.data.log_in.LogInListener;

/**
 * Created by martina on 26/06/2017.
 */

public class FakeLogInInteractorImpl implements LogInInteractor {

    @Override
    public void cancel() {

    }

    @Override
    public void reset() {

    }

    @Override
    public void loginUser(String email, String password, LogInListener callback) {
        if (email.equals(FakeData.validExistingEmail) && password.equals(FakeData.validPassword)) {
            callback.onLoggedIn(FakeData.provideFakeUser());
        } else if (email.equals(FakeData.validNotExistingEmail) && password.equals(FakeData.validPassword)) {
            callback.onFailure(FakeData.errorEmailNotExist);
        } else {
            callback.onFailure(FakeData.errorUnknown);
        }
    }
}