package com.majcenovic.fitlog.sign_up;

import com.majcenovic.fitlog.FakeData;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.ui.sign_up.sign_up_third.SignUpThirdContract;
import com.majcenovic.fitlog.ui.sign_up.sign_up_third.SignUpThirdPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpThirdPresenterTest {

    @Mock
    private SignUpThirdContract.View view;

    private SignUpThirdContract.Presenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        presenter = new SignUpThirdPresenter(view, new FakeSignUpInteractorImpl());
    }

    @Test
    public void whenSignUpThirdSubmitInvalidInput_showInvalidInput() {
        presenter.submitSignUp(FakeData.provideInvalidSignUpThirdDto());
        verify(view, never()).showProgress();
        verify(view).showInvalidInput();
    }

    @Test
    public void whenSignUpThirdSubmitValidInput_showMain() {
        SignUpDto signUpDto = FakeData.provideValidSignUpThirdDto();
        presenter.submitSignUp(signUpDto);
        verify(view).showProgress();
        verify(view).showMain();
    }
}
