package com.majcenovic.fitlog.sign_up;

import com.majcenovic.fitlog.FakeData;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.ui.sign_up.sign_up_first.SignUpFirstContract;
import com.majcenovic.fitlog.ui.sign_up.sign_up_first.SignUpFirstPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpFirstPresenterTest {

    @Mock
    private SignUpFirstContract.View view;

    private SignUpFirstContract.Presenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        presenter = new SignUpFirstPresenter(view);
    }

    @Test
    public void whenSignUpFirstSubmitInputMissing_showInvalidInput() {
        presenter.submitNext(FakeData.provideInvalidSignUpFirstDto());
        verify(view, never()).showProgress();
        verify(view).showInvalidInput();
    }

    @Test
    public void whenSignUpFirstSubmitInputValid_showSignUpSecond() {
        SignUpDto signUpDto = FakeData.provideValidSignUpFirstDto();
        presenter.submitNext(signUpDto);
        verify(view, never()).showProgress();
        verify(view).showSignUpSecond(signUpDto);
    }

}
