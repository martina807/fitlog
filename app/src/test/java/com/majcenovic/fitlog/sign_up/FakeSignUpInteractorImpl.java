package com.majcenovic.fitlog.sign_up;

import android.content.Context;

import com.majcenovic.fitlog.FakeData;
import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.data.network.ApiManager;
import com.majcenovic.fitlog.data.sign_up.SignUpInteractor;
import com.majcenovic.fitlog.data.sign_up.SignUpListener;
import com.majcenovic.fitlog.storage.SharedPrefs;
import com.majcenovic.fitlog.util.ApiErrorUtil;
import com.majcenovic.fitlog.util.RequestUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by martina on 27/06/2017.
 */

public class FakeSignUpInteractorImpl implements SignUpInteractor {

    private Boolean isCanceled;

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void signUpUser(SignUpDto signUpTransferObject, final SignUpListener callback) {

        if (signUpTransferObject.equals(FakeData.provideValidSignUpThirdDto())) {
            callback.onSignedUp(FakeData.provideFakeUser());
        } else if (signUpTransferObject.equals(FakeData.provideInvalidSignUpThirdDto())) {
            callback.onFailure(FakeData.errorMissingInputs);
        } else {
            callback.onFailure(FakeData.errorUnknown);
        }
    }
}
