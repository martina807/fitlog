package com.majcenovic.fitlog.sign_up;

import com.majcenovic.fitlog.FakeData;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;
import com.majcenovic.fitlog.ui.sign_up.sign_up_second.SignUpSecondContract;
import com.majcenovic.fitlog.ui.sign_up.sign_up_second.SignUpSecondPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by martina on 27/06/2017.
 */

public class SignUpSecondPresenterTest {

    @Mock
    private SignUpSecondContract.View view;

    private SignUpSecondContract.Presenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        presenter = new SignUpSecondPresenter(view);
    }

    @Test
    public void whenSignUpSecondSubmitInvalidDateInput_showInvalidInput() {
        presenter.submitNext(FakeData.provideInvalidDateSignUpSecondDto());
        verify(view, never()).showProgress();
        verify(view).showInvalidInput();
    }

    @Test
    public void whenSignUpSecondSubmitInvalidWeightInput_showInvalidInput() {
        presenter.submitNext(FakeData.provideInvalidWeightSignUpSecondDto());
        verify(view, never()).showProgress();
        verify(view).showInvalidInput();
    }

    @Test
    public void whenSignUpSecondSubmitInvalidHeightInput_showInvalidInput() {
        presenter.submitNext(FakeData.provideInvalidHeightSignUpSecondDto());
        verify(view, never()).showProgress();
        verify(view).showInvalidInput();
    }

    @Test
    public void whenSignUpSecondSubmitInputValid_showSignUpThird() {
        SignUpDto signUpDto = FakeData.provideValidSignUpSecondDto();
        presenter.submitNext(signUpDto);
        verify(view, never()).showProgress();
        verify(view).showSignUpThird(signUpDto);
    }

}
