package com.majcenovic.fitlog;

import com.majcenovic.fitlog.data.models.User;
import com.majcenovic.fitlog.data.models.dto.SignUpDto;

import java.io.File;

/**
 * Created by martina on 26/06/2017.
 */

public class FakeData {

    public static String validNotExistingEmail = "john@doe.com";
    public static String invalidEmail = "johndoe.com";

    public static String validPassword = "123456";
    public static String invalidPassword = "123";

    public static String validExistingEmail = "art@tux.com";

    public static String validName = "John";

    public static String validDate = "1994-11-15";
    public static int validHeight = 180;
    public static double validWeight = 80.0;

    public static String invalidDate = "2016-11-15";
    public static int invalidHeight = 80;
    public static double invalidWeight = 10.0;

    public static String validExistingShouldErrorEmail = "art-error@tux.com";

    public static String errorEmailNotExist = "Email does not exist";
    public static String errorUnknown = "Unknown error";
    public static String errorMissingInputs = "Missing inputs";


    public static SignUpDto provideValidSignUpFirstDto() {
        return new SignUpDto(FakeData.validName, FakeData.validName, FakeData.validPassword, FakeData.validNotExistingEmail, new File(""));
    }

    public static SignUpDto provideInvalidSignUpFirstDto() {
        return new SignUpDto("", FakeData.validName, FakeData.validPassword, FakeData.validNotExistingEmail, new File(""));
    }

    public static SignUpDto provideValidSignUpSecondDto() {
        return new SignUpDto(1, FakeData.validDate, FakeData.validHeight, FakeData.validWeight);
    }

    public static SignUpDto provideInvalidDateSignUpSecondDto() {
        return new SignUpDto(1, FakeData.invalidDate, FakeData.validHeight, FakeData.validWeight);
    }

    public static SignUpDto provideInvalidHeightSignUpSecondDto() {
        return new SignUpDto(1, FakeData.validDate, FakeData.invalidHeight, FakeData.validWeight);
    }

    public static SignUpDto provideInvalidWeightSignUpSecondDto() {
        return new SignUpDto(1, FakeData.validDate, FakeData.validHeight, FakeData.invalidWeight);
    }

    public static SignUpDto provideInvalidSignUpThirdDto() {
        return new SignUpDto("active", "", 70.5);
    }

    public static SignUpDto provideValidSignUpThirdDto() {
        return new SignUpDto("active", "0.5", 70.5);
    }

    public static User provideFakeUser() {
        return new User();
    }
}
